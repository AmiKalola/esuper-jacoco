package com.edelivery;

import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edelivery.adapter.RedeemPointHistoryAdapter;
import com.edelivery.component.AbstractCustomDialogRedeem;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.RedeemPointHistory;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.RedeemPointHistoryResponse;
import com.edelivery.models.responsemodels.WalletHistoryResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RewardPointActivity extends AbstractBaseAppCompatActivity {

    private CustomFontTextView tvRedeem;
    private CustomFontTextView tvSubmitRedeem;
    private AbstractCustomDialogRedeem customDialogVerification;
    private int totalRedeemPoint = 0;
    private String driverRedeemPointValue;
    private int driverMinimumPointRequireForWithdrawal;
    private RecyclerView rcvRedeemPointData;
    private View ivEmpty;
    private List<RedeemPointHistory> redeemPointHistory;
    private RedeemPointHistoryAdapter redeemPointHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward_point);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_reward_point));
        initViewById();
        setViewListener();
        initRcvRedeemPointHistory();
        getRedeemPointHistory();
        setData();
    }

    private void initRcvRedeemPointHistory() {
        redeemPointHistory = new ArrayList<>();
        rcvRedeemPointData.setLayoutManager(new LinearLayoutManager(this));
        redeemPointHistoryAdapter = new RedeemPointHistoryAdapter(this, redeemPointHistory);
        rcvRedeemPointData.setAdapter(redeemPointHistoryAdapter);
    }


    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void initViewById() {
        tvSubmitRedeem = findViewById(R.id.tvSubmitRedeem);
        tvRedeem = findViewById(R.id.tvRedeem);
        rcvRedeemPointData = findViewById(R.id.rcvRedeemPointData);
        ivEmpty = findViewById(R.id.ivEmpty);
    }

    @Override
    protected void setViewListener() {
        tvSubmitRedeem.setOnClickListener(this);

    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tvSubmitRedeem) {
            openRedeemDialog();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * method used to call a webservice for get redeem point history
     */
    private void getRedeemPointHistory() {
        Utils.showCustomProgressDialog(RewardPointActivity.this, false);
        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.ID, preferenceHelper.getUserId());
        map.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
        map.put(Const.Params.TYPE, Const.Type.USER);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<RedeemPointHistoryResponse> detailResponseCall = apiInterface.getRedeemPointHistory(map);
        detailResponseCall.enqueue(new Callback<RedeemPointHistoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<RedeemPointHistoryResponse> call, @NonNull Response<RedeemPointHistoryResponse> response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    redeemPointHistory.clear();
                    if (response.body().isSuccess()) {
                        redeemPointHistory.addAll(response.body().getWalletHistory());
                        Collections.sort(redeemPointHistory, (lhs, rhs) -> compareTwoDate(lhs.getCreatedAt(), rhs.getCreatedAt()));
                        redeemPointHistoryAdapter.notifyDataSetChanged();
                    }

                    if (redeemPointHistory.isEmpty()) {
                        rcvRedeemPointData.setVisibility(View.GONE);
                        ivEmpty.setVisibility(View.VISIBLE);
                    } else {
                        rcvRedeemPointData.setVisibility(View.VISIBLE);
                        ivEmpty.setVisibility(View.GONE);
                    }

                    totalRedeemPoint = response.body().getTotalRedeemPoint();
                    driverRedeemPointValue = response.body().getUserRedeemPointValue();
                    driverMinimumPointRequireForWithdrawal = response.body().getUserMinimumPointRequireForWithdrawal();
                    setData();
                }

            }

            @Override
            public void onFailure(@NonNull Call<RedeemPointHistoryResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(Const.Tag.REDEEM_ACTIVITY, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    private void setData() {
        tvRedeem.setText(getResources().getString(R.string.text_points, totalRedeemPoint));
        if (totalRedeemPoint <= 0) {
            tvSubmitRedeem.setEnabled(false);
            tvSubmitRedeem.setAlpha(0.3f);

        } else {
            tvSubmitRedeem.setEnabled(true);
            tvSubmitRedeem.setAlpha(1f);
        }
    }

    private void openRedeemDialog() {
        if (customDialogVerification != null && customDialogVerification.isShowing()) {
            return;
        }
        customDialogVerification = new AbstractCustomDialogRedeem(this, getResources().getString(R.string.text_redeem), getResources().getString(R.string.text_submit), getResources().getString(R.string.redeem_points), InputType.TYPE_CLASS_NUMBER, preferenceHelper.getWalletCurrencyCode(), totalRedeemPoint, driverRedeemPointValue, driverMinimumPointRequireForWithdrawal) {
            @Override
            public void onClickLeftButton() {
                customDialogVerification.dismiss();
            }

            @Override
            public void onClickRightButton(CustomFontEditTextView etDialogEditTextTwo) {
                withdrawRedeemPoint(Objects.requireNonNull(etDialogEditTextTwo.getText()).toString());
                customDialogVerification.dismiss();
            }
        };
        customDialogVerification.show();
        customDialogVerification.setOnDismissListener(dialog -> getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN));
    }

    /**
     * method used to call a webservice for withdraw redeem point
     */
    private void withdrawRedeemPoint(String redeemPoint) {
        Utils.showCustomProgressDialog(RewardPointActivity.this, false);
        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.USER_ID, preferenceHelper.getUserId());
        map.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
        map.put(Const.Params.REDEEM_POINT, Integer.parseInt(redeemPoint));
        map.put(Const.Params.TYPE, Const.Type.USER);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> detailResponseCall = apiInterface.withdrawRedeemPoint(map);
        detailResponseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(@NonNull Call<IsSuccessResponse> call, @NonNull Response<IsSuccessResponse> response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        getRedeemPointHistory();
                        Utils.showMessageToast(response.body().getStatusPhrase(), RewardPointActivity.this);
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), RewardPointActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<IsSuccessResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(Const.Tag.REDEEM_ACTIVITY, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    private int compareTwoDate(String firstStrDate, String secondStrDate) {
        try {
            SimpleDateFormat webFormat = parseContent.webFormat;
            SimpleDateFormat dateFormat = parseContent.dateTimeFormat;
            String date2 = dateFormat.format(webFormat.parse(secondStrDate));
            String date1 = dateFormat.format(webFormat.parse(firstStrDate));
            return date2.compareTo(date1);
        } catch (ParseException e) {
            AppLog.handleException(WalletHistoryResponse.class.getName(), e);
        }
        return 0;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return super.equals(obj);
    }
}