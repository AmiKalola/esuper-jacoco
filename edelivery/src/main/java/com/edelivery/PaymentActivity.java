package com.edelivery;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.StateSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.edelivery.adapter.ViewPager2Adapter;
import com.edelivery.component.AbstractCustomDialogAlert;
import com.edelivery.component.AbstractCustomDialogVerification;
import com.edelivery.component.AbstractCustomSendMoneyDialog;
import com.edelivery.component.AbstractDialogVerifyPayment;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.fragments.AbstractBasePaymentFragments;
import com.edelivery.fragments.CashFragment;
import com.edelivery.fragments.FeedbackFragment;
import com.edelivery.fragments.PayPalFragment;
import com.edelivery.fragments.PayTabsFragment;
import com.edelivery.fragments.PaystackFragment;
import com.edelivery.fragments.RazorPayFragment;
import com.edelivery.fragments.StripeFragment;
import com.edelivery.interfaces.AbstractOnSingleClickListener;
import com.edelivery.models.datamodels.Card;
import com.edelivery.models.datamodels.CreateOrderDetail;
import com.edelivery.models.datamodels.PaymentGateway;
import com.edelivery.models.datamodels.UnavailableItems;
import com.edelivery.models.responsemodels.CheckAvailableItemResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.PaymentGatewayResponse;
import com.edelivery.models.responsemodels.PaymentResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppColor;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PayPalHelper;
import com.edelivery.utils.ServerConfig;
import com.edelivery.utils.SocketHelper;
import com.edelivery.utils.Utils;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.paypal.checkout.approve.ApprovalData;
import com.paypal.checkout.createorder.CurrencyCode;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentAuthConfig;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentIntent;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity extends AbstractBaseAppCompatActivity {

    public List<PaymentGateway> paymentGateways;
    public PaymentGateway gatewayItem;
    public String stripeKeyId;
    public Card selectCard;
    private SwitchCompat switchIsWalletUse;
    private static boolean isForRazorPay = false;
    private CustomFontTextView tvWalletAmount;
    private ViewPager2Adapter viewPagerAdapter;
    private TabLayout tabLayout;
    private ViewPager2 viewPager;
    private LinearLayout btnPayNow;
    private CustomFontTextView tvAddWalletAmount;
    private CustomFontTextView tvPayMessage;
    private CustomFontTextView tvPayTotal;
    private boolean isComingFromCheckout;
    private int deliveryType;
    private Stripe stripe;
    private boolean isPaymentForWallet;
    private AbstractCustomDialogVerification customDialogVerification;
    private Double addWalletAmount;
    private CustomFontTextView tvAddCard;
    private View llWallet;
    private AbstractCustomDialogAlert unavailableCartItemsDialog;
    public boolean isBringChange = false;
    public boolean isShowBringChange = false;
    public String vehicleId;
    private String deliveryManNote;

    private LinearLayout llAddPaymentMethod;

    private SocketHelper socketHelper;
    private String transactionReference;
    private String currency;
    private double remainPay;

    private AbstractCustomSendMoneyDialog customSendMoneyDialog;
    private Double walletAmount;
    private TextView tvSendMoney;
    private FrameLayout llSendMoney;
    private CustomFontTextView tvEmptyPayment;
    private boolean razorPayIsForCreateOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        restoreState(savedInstanceState);
        setContentView(R.layout.activity_payment);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_payments));
        initViewById();
        getExtraData();
        setViewListener();
        getPaymentGateWays();
        if (!preferenceHelper.getIsRegisterQRUser()) {
            setToolbarRightIcon3(R.drawable.ic_history_time, this);
        }
        if (preferenceHelper.getIsRegisterQRUser()) {
            llWallet.setVisibility(View.GONE);
        }
        socketHelper = SocketHelper.getInstance();
        socketHelper.socketConnect();
    }

    public static boolean isIsForRazorPay() {
        return isForRazorPay;
    }

    private void initStripePayment() {
        final PaymentAuthConfig.Stripe3ds2UiCustomization uiCustomization;
        uiCustomization = new PaymentAuthConfig.Stripe3ds2UiCustomization.Builder().build();
        PaymentAuthConfig.init(new PaymentAuthConfig.Builder().set3ds2Config(new PaymentAuthConfig.Stripe3ds2Config.Builder()
                // set a 5 minute timeout for challenge flow
                .setTimeout(5)
                // customize the UI of the challenge flow
                .setUiCustomization(uiCustomization).build()).build());

        PaymentConfiguration.init(this, stripeKeyId);
        stripe = new Stripe(this, PaymentConfiguration.getInstance(this).getPublishableKey());
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void initViewById() {
        llWallet = findViewById(R.id.llWallet);
        switchIsWalletUse = findViewById(R.id.switchIsWalletUse);
        tvWalletAmount = findViewById(R.id.tvWalletAmount);
        tvAddWalletAmount = findViewById(R.id.tvAddWalletAmount);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        btnPayNow = findViewById(R.id.btnPayNow);
        tvPayTotal = findViewById(R.id.tvPayTotal);
        tvPayMessage = findViewById(R.id.tvPayMessage);
        tvAddCard = findViewById(R.id.tvAddCard);
        tvAddCard.setVisibility(View.GONE);
        tvEmptyPayment = findViewById(R.id.tvEmptyPayment);
        llAddPaymentMethod = findViewById(R.id.llAddPaymentMethod);
        tvSendMoney = findViewById(R.id.tvSendMoney);
        llSendMoney = findViewById(R.id.llSendMoney);
    }

    @Override
    protected void setViewListener() {
        tvAddCard.setOnClickListener(this);
        tvAddWalletAmount.setOnClickListener(this);
        switchIsWalletUse.setOnClickListener(this);
        tvSendMoney.setOnClickListener(this);
        btnPayNow.setOnClickListener(new AbstractOnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                PaymentActivity.this.onClick(v);
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setSelectedPaymentGateway(Objects.requireNonNull(tab.getText()).toString());
                Fragment paymentFragments = viewPagerAdapter.getFragment(tab.getPosition());
                tvAddCard.setVisibility(View.VISIBLE);
                tvAddCard.setTag(tab.getPosition());

                if (paymentFragments instanceof StripeFragment) {
                    ((StripeFragment) paymentFragments).notifyCardData();
                } else if (paymentFragments instanceof PaystackFragment) {
                    ((PaystackFragment) paymentFragments).notifyCardData();
                } else if (paymentFragments instanceof PayTabsFragment) {
                    ((PayTabsFragment) paymentFragments).notifyCardData();
                } else if (paymentFragments instanceof RazorPayFragment) {
                    tvAddCard.setVisibility(View.GONE);
                } else {
                    tvAddCard.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                //nothing here
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                //nothing here
            }
        });
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tvAddWalletAmount) {
            openAddWalletDialog();
        } else if (id == R.id.switchIsWalletUse) {
            toggleWalletUse();
        } else if (id == R.id.btnPayNow) {
            if (deliveryType == Const.DeliveryType.STORE) {
                checkAvailableItem();
            } else {
                checkPaymentGatewayForPayOrder();
            }
        } else if (id == R.id.ivToolbarRightIcon3) {
            goToWalletHistoryActivity();
        } else if (id == R.id.tvAddCard) {
            if (tvAddCard.getVisibility() == View.VISIBLE && viewPagerAdapter != null) {
                AbstractBasePaymentFragments paymentFragment = (AbstractBasePaymentFragments) viewPagerAdapter.getFragment((Integer) tvAddCard.getTag());
                if (paymentFragment instanceof StripeFragment) {
                    ((StripeFragment) paymentFragment).openAddCardDialog();
                } else if (paymentFragment instanceof PaystackFragment) {
                    ((PaystackFragment) paymentFragment).gotoPayStackAddCard(currency);
                } else if (paymentFragment instanceof PayTabsFragment) {
                    ((PayTabsFragment) paymentFragment).gotoPayTabsAddCard(currency);
                }
            }
        } else if (id == R.id.tvSendMoney) {
            openCustomSendMoneyDialog();
        }
    }

    /**
     * register pay tabs result launcher
     */
    private final ActivityResultLauncher<Intent> payTabActivityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        Log.d(tag, "onActivityResult: " + result);
            if (isForRazorPay) {

                if (result.getData() != null) {
                    boolean isSuccess = result.getData().getBooleanExtra("isSuccess", false);
                    if (isSuccess) {
                        if (razorPayIsForCreateOrder) {
                            preferenceHelper.putAndroidId(Utils.generateRandomString());
                            currentBooking.clearCart();
                            currentBooking.setSchedule(null);
                            goToThankYouActivity();
                        } else {
                            getPaymentGateWays();
                        }
                    }
                }
            } else PaymentActivity.this.showLoadingForPayment();
    });

    /**
     * Check all items are available to place order
     */
    private void checkAvailableItem() {
        Utils.showCustomProgressDialog(this, false);

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(Const.Params.CART_ID, currentBooking.getCartId());
        if (currentBooking.isFutureOrder()) {
            hashMap.put(Const.Params.IS_SCHEDULE_ORDER, ApiClient.makeTextRequestBody(currentBooking.isFutureOrder()));
            if (currentBooking.getSchedule().isDeliverySlotSelect()) {
                hashMap.put(Const.Params.ORDER_START_AT, ApiClient.makeTextRequestBody(currentBooking.getSchedule().getScheduleDateAndStartTimeMilli()));
                hashMap.put(Const.Params.ORDER_START_AT2, ApiClient.makeTextRequestBody(currentBooking.getSchedule().getScheduleDateAndEndTimeMilli()));
                if (currentBooking.isTableBooking()) {
                    hashMap.put(Const.Params.TABLE_ID, ApiClient.makeTextRequestBody(currentBooking.getTableId()));
                }
            } else {
                hashMap.put(Const.Params.ORDER_START_AT, ApiClient.makeTextRequestBody(currentBooking.getSchedule().getScheduleDateAndStartTimeMilli()));
            }
        }

        Call<CheckAvailableItemResponse> call = ApiClient.getClient().create(ApiInterface.class).checkAvailableItem(hashMap);
        call.enqueue(new Callback<CheckAvailableItemResponse>() {
            @Override
            public void onResponse(@NonNull Call<CheckAvailableItemResponse> call, @NonNull Response<CheckAvailableItemResponse> response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    if (Objects.requireNonNull(response.body()).isSuccess()) {
                        if (response.body().getUnavailableItems() != null && response.body().getUnavailableItems().isEmpty()) {
                            checkPaymentGatewayForPayOrder();
                        } else {
                            showUnavailableCartItemsDialog(response.body().getUnavailableItems());
                        }
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), PaymentActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CheckAvailableItemResponse> call, @NonNull Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(tag, t);
            }
        });
    }


    /**
     * this method called a webservice for toggleWalletUse
     */
    private void toggleWalletUse() {
        Utils.showCustomProgressDialog(this, false);
        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
        map.put(Const.Params.USER_ID, preferenceHelper.getUserId());
        map.put(Const.Params.IS_USE_WALLET, switchIsWalletUse.isChecked());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.toggleWalletUse(map);
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(@NonNull Call<IsSuccessResponse> call, @NonNull Response<IsSuccessResponse> response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        switchIsWalletUse.setChecked(response.body().isWalletUse());
                        setMessageAsParPayment(switchIsWalletUse.isChecked(), preferenceHelper.getWalletAmount(), currentBooking.getTotalInvoiceAmount());
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), PaymentActivity.this);
                        switchIsWalletUse.setChecked(!switchIsWalletUse.isChecked());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<IsSuccessResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                switchIsWalletUse.setChecked(!switchIsWalletUse.isChecked());
                Utils.hideCustomProgressDialog();
            }
        });
    }

    /**
     * this method called a webservice for  add wallet amount
     */
    private void addWalletAmount(String paymentIntentId, String lastFour, String cardId) {
        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
        map.put(Const.Params.USER_ID, preferenceHelper.getUserId());
        map.put(Const.Params.TYPE, Const.Type.USER);
        map.put(Const.Params.PAYMENT_ID, gatewayItem.getId());
        map.put(Const.Params.LAST_FOUR, lastFour);
        map.put(Const.Params.WALLET, addWalletAmount);
        map.put(Const.Params.PAYMENT_INTENT_ID, paymentIntentId);
        map.put(Const.Params.CARD_ID, cardId);

        Utils.showCustomProgressDialog(this, false);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PaymentResponse> responseCall = apiInterface.getAddWalletAmount(map);
        responseCall.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaymentResponse> call, @NonNull Response<PaymentResponse> response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    if (Objects.requireNonNull(response.body()).isSuccess()) {
                        setWalletAmount(response.body().getWallet(), response.body().getWalletCurrencyCode());
                        if (preferenceHelper.isSendMoney()) {
                            llSendMoney.setVisibility(response.body().getWallet() > 0 ? View.VISIBLE : View.GONE);
                        }
                        Utils.showMessageToast(response.body().getStatusPhrase(), PaymentActivity.this);
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), PaymentActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaymentResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    /**
     * this method called a webservice to get client secret and initiate payment Intent for
     * delivery order or add wallet
     */
    public void createStripePaymentIntent() {
        this.isPaymentForWallet = true;
        Utils.showCustomProgressDialog(this, false);
        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.TYPE, Const.Type.USER);
        map.put(Const.Params.USER_ID, preferenceHelper.getUserId());
        map.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
        map.put(Const.Params.PAYMENT_ID, gatewayItem.getId());
        map.put("is_web", false);
        if (isPaymentForWallet) {
            map.put(Const.Params.AMOUNT, addWalletAmount);
            map.put(Const.Params.WALLET_CURRENCY_CODE, currency);
        } else {
            map.put(Const.Params.ORDER_PAYMENT_ID, currentBooking.getOrderPaymentId());
        }
        Call<PaymentResponse> call;
        if (isPaymentForWallet) {
            call = ApiClient.getClient().create(ApiInterface.class).getStripPaymentIntentWallet(map);
        } else {
            call = ApiClient.getClient().create(ApiInterface.class).getStripPaymentIntent(map);
        }
        call.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaymentResponse> call, @NonNull Response<PaymentResponse> response) {
                if (parseContent.isSuccessful(response)) {
                    if (Objects.requireNonNull(response.body()).isSuccess()) {

                        setIsForRazorpay(false);
                        switch (gatewayItem.getId()) {
                            case Const.Payment.RAZORPAY:

                                Utils.hideCustomProgressDialog();
                                transactionReference = "'" + response.body().getOptions().order_id + "'";
                                registerTransactionSocket(transactionReference, false);
                                Intent i = new Intent(PaymentActivity.this, RazorPayPaymentActivity.class);
                                i.putExtra(Const.Params.PAYU_HTML, response.body().getHtmlForm());
                                payTabActivityResultLauncher.launch(i);
                                break;
                            case Const.Payment.STRIPE:
                                ConfirmPaymentIntentParams paymentIntentParams = ConfirmPaymentIntentParams.createWithPaymentMethodId(response.body().getPaymentMethod(), response.body().getClientSecret());
                                stripe.confirmPayment(PaymentActivity.this, paymentIntentParams);
                                break;
                            case Const.Payment.PAYSTACK:
                                Utils.hideCustomProgressDialog();
                                setWalletAmount(response.body().getWallet(), response.body().getWalletCurrencyCode());
                                Utils.showMessageToast(response.body().getStatusPhrase(), PaymentActivity.this);
                                break;
                            case Const.Payment.PAY_TABS:
                                Utils.hideCustomProgressDialog();
                                transactionReference = "'" + response.body().getTranRef() + "'";
                                registerTransactionSocket(transactionReference, false);
                                Intent intent = new Intent(PaymentActivity.this, PayStackPaymentActivity.class);
                                intent.putExtra(Const.Params.AUTHORIZATION_URL, response.body().getAuthorizationUrl());
                                payTabActivityResultLauncher.launch(intent);
                                break;
                            default:
                                Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), PaymentActivity.this);
                                break;
                        }
                    } else {
                        Utils.hideCustomProgressDialog();
                        if (TextUtils.isEmpty(response.body().getRequiredParam())) {
                            if (!TextUtils.isEmpty(response.body().getError())) {
                                Utils.showToast(response.body().getError(), PaymentActivity.this);
                            } else {
                                Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), PaymentActivity.this);
                            }
                        } else {
                            openVerificationDialog(response.body().getRequiredParam(), response.body().getReference());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaymentResponse> call, @NonNull Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(PaymentActivity.class.getSimpleName(), t);
            }
        });
    }

    /**
     * Unavailable item dialog
     */
    private void showUnavailableCartItemsDialog(List<UnavailableItems> unavailableItemsList) {
        if (unavailableCartItemsDialog != null && unavailableCartItemsDialog.isShowing() && !isFinishing()) {
            return;
        }

        StringBuilder message = new StringBuilder();
        message.append(getString(R.string.text_following_items_are_not_available)).append("\n\n");
        for (int i = 0; i < unavailableItemsList.size(); i++) {
            if (unavailableItemsList.size() - 1 == i) {
                message.append(unavailableItemsList.get(i).getItemName());
            } else {
                message.append(unavailableItemsList.get(i).getItemName()).append(", ");
            }
        }

        unavailableCartItemsDialog = new AbstractCustomDialogAlert(this, getString(R.string.text_alert), message.toString(), getString(R.string.text_go_to_home)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
            }

            @Override
            public void onClickRightButton() {
                dismiss();
                goToHomeActivity();
            }
        };

        unavailableCartItemsDialog.show();
    }

    //    this method is called when user want to pay order payment  using razorPay
    public void createStripePaymentIntentForOrderRazorPay() {
        Utils.showCustomProgressDialog(this, false);
        HashMap<String, Object> map = new HashMap<>();

        CreateOrderDetail createOrderDetail = new CreateOrderDetail();

        if (currentBooking.isFutureOrder()) {

            if (currentBooking.getSchedule().isDeliverySlotSelect()) {
                createOrderDetail.setOrderStartAt(currentBooking.getSchedule().getScheduleDateAndStartTimeMilli());
                createOrderDetail.setOrderStartAt2(currentBooking.getSchedule().getScheduleDateAndEndTimeMilli());
            } else {
                createOrderDetail.setOrderStartAt(currentBooking.getSchedule().getScheduleDateAndStartTimeMilli());
            }
            map.put("is_schedule_order", true);
        } else map.put("is_schedule_order", false);

        createOrderDetail.setDeliveryNote(deliveryManNote);

        createOrderDetail.setDeliveryUserName("");
        createOrderDetail.setDeliveryUserPhone(preferenceHelper.getCountryPhoneCode() + preferenceHelper.getPhoneNumber());
        createOrderDetail.setAllowContactLessDelivery(currentBooking.isAllowContactLessDelivery()/*false*/);
        createOrderDetail.setDeliveryType(deliveryType);
        createOrderDetail.setBringChange(isBringChange /*false*/);

        map.put(Const.Params.ORDER_PAYMENT_ID, currentBooking.getOrderPaymentId());
        map.put(Const.Params.DELIVERY_TYPE, deliveryType);

        createOrderDetail.setVehicleId(vehicleId);

        map.put(Const.Params.ORDER_DETAILS, createOrderDetail);

        Double newData = walletAmount;
        int value = newData.intValue();

        map.put("wallet_amount", "" + value);
        map.put(Const.Params.IS_USE_WALLET, switchIsWalletUse.isChecked()/*false*/);
        map.put(Const.Params.IS_PAYMENT_MODE_CASH, gatewayItem.isPaymentModeCash()/*false*/);
        map.put(Const.Params.USER_ID, preferenceHelper.getUserId());
        map.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
        map.put(Const.Params.AMOUNT, /*remainPay*/currentBooking.getTotalInvoiceAmount());
        map.put(Const.Params.PAYMENT_ID, gatewayItem.getId());
        map.put("is_web", false);
        map.put("url", ServerConfig.baseUrl + "payments");
        map.put(Const.Params.WALLET_CURRENCY_CODE, currency);
        map.put(Const.Params.CART_ID, ApiClient.makeTextRequestBody(currentBooking.getCartId()));
        map.put(Const.Params.TYPE, Const.Type.USER);


        if (deliveryType == Const.DeliveryType.COURIER) {
            map.put(Const.Params.STORE_DELIVERY_ID, CurrentBooking.getInstance().getSelectedDeliveryId());
        }

        Call<PaymentResponse> call = ApiClient.getClient().create(ApiInterface.class).getStripPaymentIntentWallet(map);

        call.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaymentResponse> call, @NonNull Response<PaymentResponse> response) {
                if (parseContent.isSuccessful(response)) {
                    if (Objects.requireNonNull(response.body()).isSuccess()) {
                        if (Const.Payment.RAZORPAY.equals(gatewayItem.getId())) {
                            Utils.hideCustomProgressDialog();
                            transactionReference = "'" + response.body().getOptions().order_id + "'";
                            registerTransactionSocket(transactionReference, true);
                            Intent i = new Intent(PaymentActivity.this, RazorPayPaymentActivity.class);
                            i.putExtra(Const.Params.PAYU_HTML, response.body().getHtmlForm());
                            payTabActivityResultLauncher.launch(i);
                        } else {
                            Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), PaymentActivity.this);
                        }
                    } else {
                        Utils.hideCustomProgressDialog();
                        if (TextUtils.isEmpty(response.body().getRequiredParam())) {
                            if (!TextUtils.isEmpty(response.body().getError())) {
                                Utils.showToast(response.body().getError(), PaymentActivity.this);
                            } else {
                                Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), PaymentActivity.this);
                            }
                        } else {
                            openVerificationDialog(response.body().getRequiredParam(), response.body().getReference());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaymentResponse> call, @NonNull Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(PaymentActivity.class.getSimpleName(), t);
            }
        });
    }

    /**
     * register transaction socket for pay tabs
     */
    private void registerTransactionSocket(String transactionReference, boolean isForCreateOrder) {
        razorPayIsForCreateOrder = isForCreateOrder;
        if (socketHelper != null && transactionReference != null) {
            if (!socketHelper.isConnected()) {
                socketHelper.socketConnect();
            }
            Log.e(tag, "transactionReference --> " + transactionReference);
            socketHelper.getSocket().off(transactionReference);
            socketHelper.getSocket().on(transactionReference, args -> {
                if (args != null) {
                    final JSONObject jsonObject = (JSONObject) args[0];
                    AppLog.log(tag, "SocketHelper transactionReference --> " + transactionReference + " --> " + jsonObject);
                    runOnUiThread(() -> {
                        try {
                            Utils.hideCustomProgressDialog();
                            if (jsonObject.has("error")) {
                                Utils.showToast(jsonObject.getString("error"), PaymentActivity.this);
                            } else {
                                if (isForCreateOrder) {
                                    preferenceHelper.putAndroidId(Utils.generateRandomString());
                                    currentBooking.clearCart();
                                    currentBooking.setSchedule(null);
                                    goToThankYouActivity();
                                } else {
                                    getPaymentGateWays();
                                }
                            }
                        } catch (Exception e) {
                            AppLog.handleException(PaymentActivity.class.getName(),e);
                        }
                    });
                }
            });
        }
    }

    /**
     * un register transaction socket for pay tabs
     */
    public void unregisterTransactionSocket(String transactionReference) {
        if (socketHelper != null && transactionReference != null) {
            socketHelper.getSocket().off(transactionReference);
        }
    }

    @Override
    protected void onDestroy() {
        unregisterTransactionSocket(transactionReference);
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (stripe != null) {
            stripe.onPaymentResult(requestCode, data, new ApiResultCallback<PaymentIntentResult>() {
                @Override
                public void onSuccess(@NonNull PaymentIntentResult result) {
                    Utils.hideCustomProgressDialog();
                    final PaymentIntent paymentIntent = result.getIntent();
                    final PaymentIntent.Status status = paymentIntent.getStatus();
                    if (status == PaymentIntent.Status.Succeeded || status == PaymentIntent.Status.RequiresCapture) {
                        Log.d("w",String.valueOf(isPaymentForWallet));
                        if (isPaymentForWallet) {
                           updateWalletUsingSocket(paymentIntent.getId());

                        } else {
                            updateCreateOrderUsingSocket(paymentIntent.getId());
                        }
                    } else if (status == PaymentIntent.Status.Canceled) {
                        Utils.hideCustomProgressDialog();
                        Utils.showToast(getString(R.string.error_payment_cancel), PaymentActivity.this);
                    } else if (status == PaymentIntent.Status.Processing) {
                        Utils.hideCustomProgressDialog();
                        Utils.showToast(getString(R.string.error_payment_processing), PaymentActivity.this);
                    } else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
                        Utils.hideCustomProgressDialog();
                        Utils.showToast(getString(R.string.error_payment_auth), PaymentActivity.this);
                    } else if (status == PaymentIntent.Status.RequiresAction || status == PaymentIntent.Status.RequiresConfirmation) {
                        Utils.hideCustomProgressDialog();
                        Utils.showToast(getString(R.string.error_payment_action), PaymentActivity.this);
                    } else {
                        Utils.hideCustomProgressDialog();
                    }
                }

                @Override
                public void onError(@NonNull Exception e) {
                    Utils.hideCustomProgressDialog();
                    Utils.showToast(e.getMessage(), PaymentActivity.this);
                }
            });
        }
    }

    /**
     * show loading and hide it after 1 min
     */
    private void showLoadingForPayment() {
        Utils.showCustomProgressDialog(this, false);
        new Handler().postDelayed(() -> runOnUiThread(Utils::hideCustomProgressDialog), 60000);
    }

    private void updateWalletUsingSocket(String id) {
        if (socketHelper != null && socketHelper.isConnected()) {
            String paymentId = String.format("'%s'", id);
            try {
                socketHelper.getSocket().off(paymentId);
                socketHelper.getSocket().on(paymentId, args -> {
                    if (args != null) {
                        final JSONObject jsonObject = (JSONObject) args[0];
                        AppLog.log(tag, "SocketHelper updateWalletUsingSocket --> " + id + " --> " + jsonObject);
                        runOnUiThread(this::getPaymentGateWays);
                    }
                });
            } catch (Exception e) {
                AppLog.handleException(PaymentActivity.class.getName(),e);
            }
        }
    }

    private void updateCreateOrderUsingSocket(String id) {
        if (socketHelper != null && socketHelper.isConnected()) {
            Utils.showCustomProgressDialog(this, false);
            String paymentId = String.format("'%s'", id);
            try {
                socketHelper.getSocket().off(paymentId);
                socketHelper.getSocket().on(paymentId, args -> {
                    if (args != null) {
                        runOnUiThread(() -> {
                            Utils.hideCustomProgressDialog();
                            preferenceHelper.putAndroidId(Utils.generateRandomString());
                            currentBooking.clearCart();
                            currentBooking.setSchedule(null);
                            goToThankYouActivity();
                        });
                    }
                });
            } catch (Exception e) {
                AppLog.handleException(PaymentActivity.class.getName(),e);
                Utils.hideCustomProgressDialog();
            }
        }
    }

    /**
     * this method called a webservice to get All Payment Gateway witch is available from admin
     * panel
     */
    private void getPaymentGateWays() {
        Utils.showCustomProgressDialog(this, false);
        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
        map.put(Const.Params.USER_ID, preferenceHelper.getUserId());
        map.put(Const.Params.COUNTRY, currentBooking.getCountry());
        map.put(Const.Params.COUNTRY_CODE, currentBooking.getCountryCode());
        map.put(Const.Params.COUNTRY_CODE_2, currentBooking.getCountryCode2());
        map.put(Const.Params.LATITUDE, currentBooking.getPaymentLatitude());
        map.put(Const.Params.LONGITUDE, currentBooking.getPaymentLongitude());
        map.put(Const.Params.CITY1, currentBooking.getCity1());
        map.put(Const.Params.CITY2, currentBooking.getCity2());
        map.put(Const.Params.CITY3, currentBooking.getCity3());
        map.put(Const.Params.CITY_CODE, currentBooking.getCityCode());
        map.put(Const.Params.TYPE, Const.Type.USER);
        if (btnPayNow.getVisibility() == View.VISIBLE) {
            map.put(Const.Params.CITY_ID, currentBooking.getCartCityId());
        } else {
            map.put(Const.Params.CITY_ID, "");
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PaymentGatewayResponse> responseCall = apiInterface.getPaymentGateway(map);
        responseCall.enqueue(new Callback<PaymentGatewayResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaymentGatewayResponse> call, @NonNull Response<PaymentGatewayResponse> response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        paymentGateways = new ArrayList<>();
                        switchIsWalletUse.setChecked(response.body().isUseWallet());
                        currentBooking.setCurrencyCode(response.body().getWalletCurrencyCode());
                        currentBooking.setCashPaymentMode(response.body().isCashPaymentMode());
                        if (currentBooking.isCashPaymentMode()) {
                            PaymentGateway paymentGateway = new PaymentGateway();
                            paymentGateway.setName(getResources().getString(R.string.text_cash));
                            paymentGateway.setId(Const.Payment.CASH);
                            paymentGateway.setPaymentModeCash(true);
                            paymentGateway.setPaymentKey("");
                            paymentGateway.setPaymentKeyId("");
                            paymentGateways.add(paymentGateway);
                        }
                        if (currentBooking.isTableBooking() && currentBooking.getTotalInvoiceAmount() <= 0 || response.body().getPaymentGateway() != null) {
                            paymentGateways.addAll(response.body().getPaymentGateway());
                        }
                        if (isComingFromCheckout) {
                            btnPayNow.setVisibility(paymentGateways.isEmpty() ? View.GONE : View.VISIBLE);
                            tvEmptyPayment.setVisibility(paymentGateways.isEmpty() ? View.VISIBLE : View.GONE);
                            llAddPaymentMethod.setVisibility(paymentGateways.isEmpty() ? View.GONE : View.VISIBLE);
                            tvAddWalletAmount.setVisibility(response.body().getPaymentGateway().isEmpty() ? View.GONE : View.VISIBLE);
                        } else {
                            if (preferenceHelper.isSendMoney()) {
                                llSendMoney.setVisibility(response.body().getWalletAmount() > 0 ? View.VISIBLE : View.GONE);
                            }
                            tvEmptyPayment.setVisibility(response.body().getPaymentGateway().isEmpty() ? View.VISIBLE : View.GONE);
                            tvAddWalletAmount.setVisibility(response.body().getPaymentGateway().isEmpty() ? View.GONE : View.VISIBLE);
                            llAddPaymentMethod.setVisibility(response.body().getPaymentGateway().isEmpty() ? View.GONE : View.VISIBLE);
                        }
                        initTabLayout();
                        setWalletAmount(response.body().getWalletAmount(), response.body().getWalletCurrencyCode());
                        Log.d("w", String.valueOf(response.body().getWalletAmount()));
                        currency = response.body().getWalletCurrencyCode();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), PaymentActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaymentGatewayResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void initTabLayout() {
        if (!paymentGateways.isEmpty() && viewPagerAdapter == null) {
            viewPagerAdapter = new ViewPager2Adapter(getSupportFragmentManager(), getLifecycle());

            for (PaymentGateway paymentGateway : paymentGateways) {
                if (TextUtils.equals(paymentGateway.getId(), Const.Payment.CASH) && isComingFromCheckout && !currentBooking.isAllowContactLessDelivery()) {
                    viewPagerAdapter.addFragment(new CashFragment(), getResources().getString(R.string.text_cash));
                }
                if (TextUtils.equals(paymentGateway.getId(), Const.Payment.STRIPE)) {
                    viewPagerAdapter.addFragment(new StripeFragment(), paymentGateway.getName());
                    stripeKeyId = paymentGateway.getPaymentKeyId();
                    initStripePayment();
                }
                if (TextUtils.equals(paymentGateway.getId(), Const.Payment.PAYSTACK)) {
                    viewPagerAdapter.addFragment(new PaystackFragment(), paymentGateway.getName());
                }
                if (TextUtils.equals(paymentGateway.getId(), Const.Payment.PAY_TABS)) {
                    viewPagerAdapter.addFragment(new PayTabsFragment(), paymentGateway.getName());
                }
                if (TextUtils.equals(paymentGateway.getId(), Const.Payment.PAY_PAL)) {
                    viewPagerAdapter.addFragment(new PayPalFragment(), paymentGateway.getName());
                }
                if (TextUtils.equals(paymentGateway.getId(), Const.Payment.RAZORPAY)) {
                    viewPagerAdapter.addFragment(new RazorPayFragment(), paymentGateway.getName());
                }
            }


            viewPager.setOffscreenPageLimit(1);
            viewPager.setAdapter(viewPagerAdapter);
            new TabLayoutMediator(tabLayout, viewPager, (tab, position) -> tab.setText(viewPagerAdapter.getPageTitle(position))).attach();

            ViewGroup tabStrip = (ViewGroup) tabLayout.getChildAt(0);
            tabLayout.setSelectedTabIndicator(null);
            for (int i = 0; i < tabStrip.getChildCount(); i++) {
                View tabView = tabStrip.getChildAt(i);
                if (tabView != null) {
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tabView.getLayoutParams();
                    layoutParams.rightMargin = getResources().getDimensionPixelSize(R.dimen.activity_horizontal_padding);
                    int paddingStart = tabView.getPaddingStart();
                    int paddingTop = tabView.getPaddingTop();
                    int paddingEnd = tabView.getPaddingEnd();
                    int paddingBottom = tabView.getPaddingBottom();
                    Drawable drawable = AppCompatResources.getDrawable(this, R.drawable.shape_custom_button);
                    drawable.setTint(AppColor.colorTheme);
                    Drawable drawable2 = AppCompatResources.getDrawable(this, R.drawable.shape_custom_button_stroke);
                    StateListDrawable stateListDrawable = new StateListDrawable();
                    stateListDrawable.addState(new int[]{android.R.attr.state_selected}, drawable);
                    stateListDrawable.addState(StateSet.NOTHING, drawable2);
                    ViewCompat.setBackground(tabView, stateListDrawable);
                    ViewCompat.setPaddingRelative(tabView, paddingStart, paddingTop, paddingEnd, paddingBottom);
                }
            }
        }
    }

    private void getExtraData() {
        if (getIntent() != null) {
            isComingFromCheckout = getIntent().getExtras().getBoolean(Const.Tag.PAYMENT_ACTIVITY);
            if (isComingFromCheckout) {
                btnPayNow.setVisibility(View.VISIBLE);
                deliveryType = getIntent().getExtras().getInt(Const.Params.DELIVERY_TYPE, Const.DeliveryType.STORE);
                isShowBringChange = getIntent().getExtras().getBoolean(Const.Params.IS_BRING_CHANGE);
                deliveryManNote = getIntent().getExtras().getString(Const.Params.DELIVERY_NOTE);
                vehicleId = getIntent().getExtras().getString(Const.Params.VEHICLE_ID);
            } else {
                btnPayNow.setVisibility(View.GONE);
            }
        }
    }

    /**
     * this method called a webservice fro make payment for delivery order
     */
    private void payOrderPayment(boolean isCash, boolean isSendOrderDetails, String paymentIntentId) {
        isPaymentForWallet = false;
        if (gatewayItem != null) {
            Utils.showCustomProgressDialog(this, false);
            HashMap<String, Object> map = new HashMap<>();
            map.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            map.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            map.put(Const.Params.IS_PAYMENT_MODE_CASH, gatewayItem.isPaymentModeCash());
            map.put(Const.Params.PAYMENT_ID, gatewayItem.getId());
            map.put(Const.Params.ORDER_PAYMENT_ID, currentBooking.getOrderPaymentId());
            map.put(Const.Params.DELIVERY_TYPE, deliveryType);
            map.put(Const.Params.PAYMENT_INTENT_ID, paymentIntentId);
            map.put(Const.Params.IS_PAYPAL, gatewayItem.getId().equals(Const.Payment.PAY_PAL));
            if (deliveryType == Const.DeliveryType.COURIER) {
                map.put(Const.Params.STORE_DELIVERY_ID, CurrentBooking.getInstance().getSelectedDeliveryId());
            }

            if (isSendOrderDetails) {
                CreateOrderDetail createOrderDetail = new CreateOrderDetail();
                createOrderDetail.setDeliveryUserName(String.format("%s %s", preferenceHelper.getFirstName(), preferenceHelper.getLastName()));
                createOrderDetail.setDeliveryUserPhone(preferenceHelper.getCountryPhoneCode() + preferenceHelper.getPhoneNumber());
                createOrderDetail.setDeliveryNote(deliveryManNote);
                if (currentBooking.isFutureOrder()) {
                    if (currentBooking.getSchedule().isDeliverySlotSelect()) {
                        createOrderDetail.setOrderStartAt(currentBooking.getSchedule().getScheduleDateAndStartTimeMilli());
                        createOrderDetail.setOrderStartAt2(currentBooking.getSchedule().getScheduleDateAndEndTimeMilli());
                    } else {
                        createOrderDetail.setOrderStartAt(currentBooking.getSchedule().getScheduleDateAndStartTimeMilli());
                    }
                }

                if (isCash) {
                    createOrderDetail.setAllowContactLessDelivery(false);
                } else {
                    createOrderDetail.setAllowContactLessDelivery(currentBooking.isAllowContactLessDelivery());
                }
                createOrderDetail.setDeliveryType(deliveryType);
                createOrderDetail.setBringChange(isBringChange);
                createOrderDetail.setVehicleId(vehicleId);
                map.put(Const.Params.ORDER_DETAILS, createOrderDetail);
            }

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<PaymentResponse> responseCall = apiInterface.payOrderPayment(map);
            responseCall.enqueue(new Callback<PaymentResponse>() {
                @Override
                public void onResponse(@NonNull Call<PaymentResponse> call, @NonNull Response<PaymentResponse> response) {
                    if (parseContent.isSuccessful(response)) {
                        if (Objects.requireNonNull(response.body()).isSuccess()) {
                            if (response.body().isPaymentPaid()) {
                                Utils.showMessageToast(response.body().getStatusPhrase(), PaymentActivity.this);
                                createOrder(deliveryType, "", isCash);
                            } else if (!TextUtils.isEmpty(response.body().getClientSecret()) && gatewayItem.getId().equals(Const.Payment.STRIPE)) {
                                ConfirmPaymentIntentParams paymentIntentParams = ConfirmPaymentIntentParams.createWithPaymentMethodId(response.body().getPaymentMethod(), response.body().getClientSecret());
                                stripe.confirmPayment(PaymentActivity.this, paymentIntentParams);
                            } else if (!TextUtils.isEmpty(response.body().getAuthorizationUrl()) && gatewayItem.getId().equals(Const.Payment.PAY_TABS)) {
                                Utils.hideCustomProgressDialog();
                                transactionReference = "'" + response.body().getPaymentIntentId() + "'";
                                registerTransactionSocket(transactionReference, true);
                                Intent intent = new Intent(PaymentActivity.this, PayStackPaymentActivity.class);
                                intent.putExtra(Const.Params.AUTHORIZATION_URL, response.body().getAuthorizationUrl());
                                payTabActivityResultLauncher.launch(intent);
                            } else if (!TextUtils.isEmpty(response.body().getHtmlForm()) && gatewayItem.getId().equals(Const.Payment.RAZORPAY)) {
                                //nothing here
                            } else {
                                Utils.hideCustomProgressDialog();
                                Utils.showToast(getResources().getString(R.string.msg_payment_flied), PaymentActivity.this);
                            }
                        } else {
                            Utils.hideCustomProgressDialog();
                            if (TextUtils.isEmpty(response.body().getRequiredParam())) {
                                if (!TextUtils.isEmpty(response.body().getError())) {
                                    Utils.showToast(response.body().getError(), PaymentActivity.this);
                                } else {
                                    Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), PaymentActivity.this);
                                }
                            } else {
                                openVerificationDialog(response.body().getRequiredParam(), response.body().getReference());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PaymentResponse> call, @NonNull Throwable t) {
                    AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                    Utils.hideCustomProgressDialog();
                }
            });
        }
    }

    /**
     * this method called a webservice for create delivery order
     */
    private void createOrder(int deliveryType, String paymentIntentId, boolean isCash) {
        Utils.showCustomProgressDialog(this, false);
        HashMap<String, RequestBody> hashMap = new HashMap<>();
        hashMap.put(Const.Params.SERVER_TOKEN, ApiClient.makeTextRequestBody(preferenceHelper.getSessionToken()));
        hashMap.put(Const.Params.USER_ID, ApiClient.makeTextRequestBody(preferenceHelper.getUserId()));
        hashMap.put(Const.Params.CART_ID, ApiClient.makeTextRequestBody(currentBooking.getCartId()));
        hashMap.put(Const.Params.DELIVERY_TYPE, ApiClient.makeTextRequestBody(deliveryType));
        hashMap.put(Const.Params.PAYMENT_INTENT_ID, ApiClient.makeTextRequestBody(paymentIntentId));
        hashMap.put(Const.Params.ORDER_PAYMENT_ID, ApiClient.makeTextRequestBody(currentBooking.getOrderPaymentId()));
        if (isCash) {
            hashMap.put(Const.Params.IS_ALLOW_CONTACT_LESS_DELIVERY, ApiClient.makeTextRequestBody(false));
        } else {
            hashMap.put(Const.Params.IS_ALLOW_CONTACT_LESS_DELIVERY, ApiClient.makeTextRequestBody(currentBooking.isAllowContactLessDelivery()));
        }
        if (Const.DeliveryType.COURIER == deliveryType) {
            hashMap.put(Const.Params.VEHICLE_ID, ApiClient.makeTextRequestBody(currentBooking.getVehicleId()));
        }

        if (currentBooking.isFutureOrder() && deliveryType != Const.DeliveryType.COURIER) {
            hashMap.put(Const.Params.IS_SCHEDULE_ORDER, ApiClient.makeTextRequestBody(currentBooking.isFutureOrder()));
            if (currentBooking.getSchedule().isDeliverySlotSelect()) {
                hashMap.put(Const.Params.ORDER_START_AT, ApiClient.makeTextRequestBody(currentBooking.getSchedule().getScheduleDateAndStartTimeMilli()));
                hashMap.put(Const.Params.ORDER_START_AT2, ApiClient.makeTextRequestBody(currentBooking.getSchedule().getScheduleDateAndEndTimeMilli()));
                if (currentBooking.isTableBooking()) {
                    hashMap.put(Const.Params.TABLE_ID, ApiClient.makeTextRequestBody(currentBooking.getTableId()));
                }
            } else {
                hashMap.put(Const.Params.ORDER_START_AT, ApiClient.makeTextRequestBody(currentBooking.getSchedule().getScheduleDateAndStartTimeMilli()));
            }
        }

        hashMap.put(Const.Params.IS_BRING_CHANGE, ApiClient.makeTextRequestBody(isBringChange));

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall;
        if (Const.DeliveryType.COURIER == deliveryType) {
            responseCall = apiInterface.createOrder(hashMap, ApiClient.addMultipleImage(CurrentBooking.getInstance().getCourierItems()));
        } else {
            responseCall = apiInterface.createOrder(hashMap, null);
        }
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(@NonNull Call<IsSuccessResponse> call, @NonNull Response<IsSuccessResponse> response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        Utils.showMessageToast(response.body().getStatusPhrase(), PaymentActivity.this);
                        preferenceHelper.putAndroidId(Utils.generateRandomString());
                        currentBooking.clearCart();
                        currentBooking.setSchedule(null);
                        goToThankYouActivity();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), PaymentActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<IsSuccessResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    private void setSelectedPaymentGateway(String paymentName) {
        for (PaymentGateway gateway_Item : paymentGateways) {
            if (TextUtils.equals(gateway_Item.getName(), paymentName)) {
                this.gatewayItem = gateway_Item;
                return;
            }
        }
    }

    private void selectPaymentGatewayForAddWalletAmount(String amount) {
        if (gatewayItem != null) {
            if (!Utils.isDecimalAndGraterThenZero(amount)) {
                Utils.showToast(getResources().getString(R.string.msg_plz_enter_valid_amount), this);
            } else {
                addWalletAmount = Double.parseDouble(amount);
                switch (gatewayItem.getId()) {
                    case Const.Payment.RAZORPAY:
                        RazorPayFragment razFragment = (RazorPayFragment) viewPagerAdapter.getFragment(tabLayout.getSelectedTabPosition());
                        razFragment.addWalletAmount();
                        break;
                    case Const.Payment.STRIPE:
                        StripeFragment stripeFragment = (StripeFragment) viewPagerAdapter.getFragment(tabLayout.getSelectedTabPosition());
                        stripeFragment.addWalletAmount();
                        break;
                    case Const.Payment.PAYSTACK:
                        PaystackFragment paystackFragment = (PaystackFragment) viewPagerAdapter.getFragment(tabLayout.getSelectedTabPosition());
                        paystackFragment.addWalletAmount();
                        break;
                    case Const.Payment.PAY_TABS:
                        PayTabsFragment payTabsFragment = (PayTabsFragment) viewPagerAdapter.getFragment(tabLayout.getSelectedTabPosition());
                        payTabsFragment.addWalletAmount();
                        break;
                    case Const.Payment.PAY_PAL:
                        startPayPalPayment(amount, false);
                        break;
                    case Const.Payment.PAY_U_MONEY:
                        break;
                    case Const.Payment.CASH:
                        Utils.showToast(getResources().getString(R.string.msg_plz_select_other_payment_gateway), this);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void setWalletAmount(double amount, String currency) {
        preferenceHelper.putWalletAmount((float) amount);
        tvWalletAmount.setText(String.format("%s %s", parseContent.decimalTwoDigitFormat.format(amount), currency));
        switchIsWalletUse.setEnabled(amount > 0);
        setMessageAsParPayment(switchIsWalletUse.isChecked() && amount > 0, amount, currentBooking.getTotalInvoiceAmount());
        walletAmount = amount;
    }

    private void checkPaymentGatewayForPayOrder() {
        if (gatewayItem != null) {
           setIsForRazorpay(false);
            switch (gatewayItem.getId()) {
                case Const.Payment.CASH:
                    payOrderPayment(true, true, null);
                    break;
                case Const.Payment.RAZORPAY:
                    if (walletAmount < 0) {
                        payOrderPayment(false, true, null);
                        return;
                    }
                    if (switchIsWalletUse.isChecked() && walletAmount >= currentBooking.getTotalInvoiceAmount()) {
                        payOrderPayment(false, true, null);
                    } else {
                       setIsForRazorpay(true);
                        createStripePaymentIntentForOrderRazorPay();
                    }
                    break;
                case Const.Payment.STRIPE:
                    StripeFragment stripeFragment = (StripeFragment) viewPagerAdapter.getFragment(tabLayout.getSelectedTabPosition());
                    if (stripeFragment.cardList.isEmpty()) {
                        Utils.showToast(getResources().getString(R.string.msg_plz_add_card_first), this);
                    } else if (!selectCard.getPaymentId().equals(gatewayItem.getId())) {
                        Utils.showToast(getResources().getString(R.string.error_select_card_first), this);
                    } else {
                        payOrderPayment(false, true, null);
                    }
                    break;
                case Const.Payment.PAYSTACK:
                    PaystackFragment paystackFragment = (PaystackFragment) viewPagerAdapter.getFragment(tabLayout.getSelectedTabPosition());
                    if (paystackFragment.cardList.isEmpty()) {
                        Utils.showToast(getResources().getString(R.string.msg_plz_add_card_first), this);
                    } else if (!selectCard.getPaymentId().equals(gatewayItem.getId())) {
                        Utils.showToast(getResources().getString(R.string.error_select_card_first), this);
                    } else {
                        payOrderPayment(false, false, null);
                    }
                    break;
                case Const.Payment.PAY_TABS:
                    PayTabsFragment payTabsFragment = (PayTabsFragment) viewPagerAdapter.getFragment(tabLayout.getSelectedTabPosition());
                    if (payTabsFragment.cardList.isEmpty()) {
                        Utils.showToast(getResources().getString(R.string.msg_plz_add_card_first), this);
                    } else if (!selectCard.getPaymentId().equals(gatewayItem.getId())) {
                        Utils.showToast(getResources().getString(R.string.error_select_card_first), this);
                    } else {
                        payOrderPayment(false, true, null);
                    }
                    break;
                case Const.Payment.PAY_PAL:
                    startPayPalPayment(String.valueOf(remainPay), true);
                    break;
                case Const.Payment.PAY_U_MONEY:
                    break;
                default:
                    break;
            }
        }
    }

    private void startPayPalPayment(String amount, boolean isForCreateOrder) {
        PayPalHelper payPalHelper = PayPalHelper.getInstance(getApplication(), this, CurrencyCode.valueOf(currency));
        payPalHelper.setPayPalConfiguration(gatewayItem.getPaymentKeyId(), gatewayItem.getPaymentEnvironment());
        payPalHelper.setPaymentSuccessListener(new PayPalHelper.OnPaymentListener() {
            @Override
            public void onSuccess(ApprovalData approvalData) {
                if (approvalData != null && approvalData.getOrderId() != null) {
                    if (isForCreateOrder) {
                        payOrderPayment(false, false, approvalData.getOrderId());
                    } else {
                        addWalletAmount(approvalData.getOrderId(), "paypal", approvalData.getPayerId());
                    }
                }
            }

            @Override
            public void onCancel() {
                Utils.showToast(getString(R.string.error_payment_cancel), PaymentActivity.this);
            }
        });
        payPalHelper.startPayPalPaymentFlow(amount);
    }

    private void goToWalletHistoryActivity() {
        Intent intent = new Intent(this, WalletHistoryActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToThankYouActivity() {
        ThankYouFragment thankYouFragment = new ThankYouFragment();
        thankYouFragment.show(getSupportFragmentManager(), thankYouFragment.getTag());
    }

    @SuppressLint("StringFormatInvalid")
    private void setMessageAsParPayment(boolean isWalletUsed, double walletAmount, double totalOrderInvoiceAmount) {
        String payMessage = "";
        String payAmount;
        double youPaid = totalOrderInvoiceAmount - walletAmount;
        if (isWalletUsed) {
            payMessage = getResources().getString(R.string.text_pay_amount_from_wallet, currentBooking.getCurrency() + parseContent.decimalTwoDigitFormat.format(youPaid > 0 ? totalOrderInvoiceAmount - youPaid : totalOrderInvoiceAmount));
            remainPay = youPaid > 0 ? youPaid : 0;
        } else {
            remainPay = totalOrderInvoiceAmount;
        }
        payAmount = currentBooking.getCurrency() + parseContent.decimalTwoDigitFormat.format(remainPay);

        if (!TextUtils.isEmpty(payMessage) && isComingFromCheckout) {
            tvPayMessage.setText(payMessage);
            tvPayMessage.setVisibility(View.VISIBLE);
        } else {
            tvPayMessage.setVisibility(View.GONE);
        }

        tvPayTotal.setText(payAmount);

        if (tabLayout != null && tabLayout.getTabCount() > 0 && TextUtils.equals(tabLayout.getTabAt(0).getText(), getResources().getString(R.string.text_cash))) {
            CashFragment cashFragment = (CashFragment) viewPagerAdapter.getFragment(0);
            cashFragment.setPayCashMessage(remainPay);
        }
    }

    private void saveState(Bundle state) {
        if (state != null) {
            state.putString("stripeKeyId", stripeKeyId);
        }
    }

    private void restoreState(Bundle state) {
        if (state != null) {
            stripeKeyId = state.getString("stripeKeyId");
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        saveState(outState);
        super.onSaveInstanceState(outState);
    }

    private void openAddWalletDialog() {
        if (customDialogVerification != null && customDialogVerification.isShowing()) {
            return;
        }

        customDialogVerification = new AbstractCustomDialogVerification(this, getResources().getString(R.string.text_add_wallet_amount), "", getResources().getString(R.string.text_submit), "", getResources().getString(R.string.text_add_wallet_amount), false, InputType.TYPE_CLASS_NUMBER, InputType.TYPE_CLASS_NUMBER, false) {
            @Override
            public void onClickLeftButton() {
                customDialogVerification.dismiss();
            }

            @Override
            public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne, CustomFontEditTextView etDialogEditTextTwo) {
                if (Utils.isDecimalAndGraterThenZero(etDialogEditTextTwo.getText().toString())) {
                    selectPaymentGatewayForAddWalletAmount(etDialogEditTextTwo.getText().toString());
                    dismiss();
                } else {
                    etDialogEditTextTwo.setError(getResources().getString(R.string.msg_plz_enter_valid_amount));
                }
            }

            @Override
            public void resendOtp() {
//nothing here
            }
        };
        customDialogVerification.show();

        customDialogVerification.setOnDismissListener(dialog -> getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN));
    }

    private void openVerificationDialog(String requiredParam, String reference) {
        AbstractDialogVerifyPayment verifyDialog = new AbstractDialogVerifyPayment(this, requiredParam, reference) {

            @Override
            public void doWithEnable(HashMap<String, Object> map) {
                dismiss();
                sendPayStackRequiredDetails(map);
            }

            @Override
            public void doWithDisable() {
                dismiss();
            }
        };
        verifyDialog.show();
    }

    private void sendPayStackRequiredDetails(HashMap<String, Object> map) {
        map.put(Const.Params.USER_ID, preferenceHelper.getUserId());
        map.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
        map.put(Const.Params.TYPE, Const.Type.USER);
        map.put(Const.Params.PAYMENT_ID, gatewayItem.getId());
        if (isPaymentForWallet) {
            map.put(Const.Params.AMOUNT, addWalletAmount);
        } else {
            map.put(Const.Params.ORDER_PAYMENT_ID, currentBooking.getOrderPaymentId());
        }
        Utils.showCustomProgressDialog(this, false);
        Call<PaymentResponse> call = ApiClient.getClient().create(ApiInterface.class).sendPayStackRequiredDetails(map);
        call.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaymentResponse> call, @NonNull Response<PaymentResponse> response) {
                Utils.hideCustomProgressDialog();
                if (ParseContent.getInstance().isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        if (isPaymentForWallet) {
                            Utils.hideCustomProgressDialog();
                            setWalletAmount(response.body().getWallet(), response.body().getWalletCurrencyCode());
                            Utils.showMessageToast(response.body().getStatusPhrase(), PaymentActivity.this);
                        } else {
                            Utils.showMessageToast(response.body().getStatusPhrase(), PaymentActivity.this);
                            createOrder(deliveryType, "", false);
                        }
                    } else {
                        if (TextUtils.isEmpty(response.body().getRequiredParam())) {
                            if (!TextUtils.isEmpty(response.body().getError())) {
                                Utils.showToast(response.body().getError(), PaymentActivity.this);
                            } else if (!TextUtils.isEmpty(response.body().getErrorMessage())) {
                                Utils.showToast(response.body().getErrorMessage(), PaymentActivity.this);
                            } else {
                                Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), PaymentActivity.this);
                            }
                        } else {
                            openVerificationDialog(response.body().getRequiredParam(), response.body().getReference());
                        }
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<PaymentResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(FeedbackFragment.class.getSimpleName(), t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    private void openCustomSendMoneyDialog() {
        if (customSendMoneyDialog != null && customSendMoneyDialog.isShowing()) {
            return;
        }

        customSendMoneyDialog = new AbstractCustomSendMoneyDialog(PaymentActivity.this) {
            @Override
            public void clickOnSendMoney(String amount, String id) {
                if (Double.parseDouble(amount) > 0) {
                    sendMoney(Double.parseDouble(amount), id);
                } else {
                    Utils.showToast(getResources().getString(R.string.msg_plz_enter_valid_amount), PaymentActivity.this);
                }
            }

        };
        customSendMoneyDialog.show();
    }

    private void sendMoney(double amount, String friendId) {
        Utils.showCustomProgressDialog(this, false);
        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
        map.put(Const.Params.TYPE, Const.Type.USER);
        map.put(Const.Params.USER_ID, preferenceHelper.getUserId());
        map.put(Const.Params.FRIEND_ID, friendId);
        map.put(Const.Params.AMOUNT, amount);

        Call<IsSuccessResponse> call = ApiClient.getClient().create(ApiInterface.class).sendMoneyToFriend(map);
        call.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(@NonNull Call<IsSuccessResponse> call, @NonNull Response<IsSuccessResponse> response) {
                if (parseContent.isSuccessful(response)) {
                    customSendMoneyDialog.dismiss();
                    getPaymentGateWays();
                }

                Utils.hideCustomProgressDialog();
            }

            @Override
            public void onFailure(@NonNull Call<IsSuccessResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(PaymentActivity.class.getSimpleName(), t);
                Utils.hideCustomProgressDialog();
            }
        });

    }
    public static void setIsForRazorpay(boolean isForrazorpay){
        isForRazorPay = isForrazorpay;
    }
}