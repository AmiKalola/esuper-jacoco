package com.edelivery.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomSwitch;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ParseContent;

public class CashFragment extends AbstractBasePaymentFragments {
    public static final String TAG = CashFragment.class.getName();

    private CustomFontTextView tvPayMessage;
    private View llBringChange;
    private CustomSwitch swBringChange;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cash, container, false);
        tvPayMessage = view.findViewById(R.id.tvPayCashMessage);
        llBringChange = view.findViewById(R.id.llBringChange);
        swBringChange = view.findViewById(R.id.swBringChange);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (paymentActivity.preferenceHelper.getIsAllowBringChangeOption()
                && !paymentActivity.preferenceHelper.getIsFromQRCode()) {
            llBringChange.setVisibility(View.VISIBLE);
        } else {
            llBringChange.setVisibility(View.GONE);
        }

        if (paymentActivity.isShowBringChange) {
            llBringChange.setVisibility(View.GONE);
        }
if (paymentActivity.currentBooking.isTableBooking() &&  paymentActivity.currentBooking.getTotalInvoiceAmount() ==0)
{
    llBringChange.setVisibility(View.GONE);
}
        swBringChange.setOnCheckedChangeListener((buttonView, isChecked) -> paymentActivity.isBringChange = isChecked);

        paymentActivity.isBringChange = swBringChange.isChecked();
    }

    @Override
    public void onClick(View view) {
        //nothing here
    }

    @SuppressLint("StringFormatInvalid")
    public void setPayCashMessage(double payAmount) {
        if (tvPayMessage != null) {
            if (payAmount > 0) {
                if (paymentActivity.preferenceHelper.getIsFromQRCode()) {
                    tvPayMessage.setText(paymentActivity.getResources().getString(R.string.text_pay_cash_qr_amount,
                            CurrentBooking.getInstance().getCurrency() + ParseContent.getInstance().decimalTwoDigitFormat.format(payAmount)));
                } else {
                    tvPayMessage.setText(paymentActivity.getResources().getString(R.string.text_pay_cash_amount,
                            CurrentBooking.getInstance().getCurrency() + ParseContent.getInstance().decimalTwoDigitFormat.format(payAmount)));
                }
            } else {
                tvPayMessage.setText(paymentActivity.getResources().getString(R.string.text_no_pay_cash_amount));
            }
        }
    }
}