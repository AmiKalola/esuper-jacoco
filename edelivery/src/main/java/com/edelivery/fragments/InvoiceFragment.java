package com.edelivery.fragments;

import static com.edelivery.utils.Const.Params.ORDER_ID;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edelivery.OrderDetailActivity;
import com.edelivery.R;
import com.edelivery.adapter.InvoiceAdapter;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.Invoice;
import com.edelivery.models.datamodels.Order;
import com.edelivery.models.datamodels.OrderPayment;
import com.edelivery.models.responsemodels.InvoiceResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.OrderResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    private CustomFontTextViewTitle tvOderTotal;
    private CustomFontButton btnInvoiceSubmit;
    private RecyclerView rcvInvoice;
    private OrderDetailActivity orderDetailActivity;
    private OrderPayment orderPayment;
    private CustomFontTextView tvInvoiceMsg;
    private String promoCodeName = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orderDetailActivity = (OrderDetailActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_invoice, container, false);
        btnInvoiceSubmit = view.findViewById(R.id.btnInvoiceSubmit);
        tvOderTotal = view.findViewById(R.id.tvOderTotal);
        rcvInvoice = view.findViewById(R.id.rcvInvoice);
        tvInvoiceMsg = view.findViewById(R.id.tvInvoiceMsg);
        view.findViewById(R.id.btnDialogAlertLeft).setOnClickListener(view1 -> dismiss());
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getDialog() instanceof BottomSheetDialog) {
            BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
            BottomSheetBehavior<?> behavior = dialog.getBehavior();
            behavior.setDraggable(false);
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
        btnInvoiceSubmit.setOnClickListener(this);
        if (orderDetailActivity.isShowHistory) {
            btnInvoiceSubmit.setVisibility(View.GONE);
            orderDetailActivity.getHistoryDetailResponse().getOrderPaymentDetail().setTaxIncluded(orderDetailActivity.getHistoryDetailResponse().getCartDetail().getTaxIncluded());
            getOrderDetails();

        } else {
            getOrderInvoice(orderDetailActivity.getOrder().getId());
            if (orderDetailActivity.getActiveOrderResponse() != null) {
                if (orderDetailActivity.getActiveOrderResponse().isSubmitInvoice()) {
                    btnInvoiceSubmit.setVisibility(View.GONE);
                } else {
                    btnInvoiceSubmit.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnInvoiceSubmit) {
            setShowInvoice(orderDetailActivity.getOrder().getId());
        }
    }

    private void getOrderInvoice(String orderId) {
        Utils.showCustomProgressDialog(orderDetailActivity, false);
        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.SERVER_TOKEN, orderDetailActivity.preferenceHelper.getSessionToken());
        map.put(Const.Params.USER_ID, orderDetailActivity.preferenceHelper.getUserId());
        map.put(ORDER_ID, orderId);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<InvoiceResponse> invoiceResponseCall = apiInterface.getInvoice(map);
        invoiceResponseCall.enqueue(new Callback<InvoiceResponse>() {
            @Override
            public void onResponse(@NonNull Call<InvoiceResponse> call, @NonNull Response<InvoiceResponse> response) {
                Utils.hideCustomProgressDialog();
                if (response.body() != null) {
                    if (response.body().isSuccess()) {
                        response.body().getOrderPayment().setTaxIncluded(response.body().isTaxIncluded());
                        if (response.body().getPromoDetails() != null)
                            promoCodeName = response.body().getPromoDetails().getPromoCodeName();
                        loadInvoiceData(response.body().getOrderPayment(), response.body().getCurrency(), Const.DeliveryType.COURIER);
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), orderDetailActivity);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<InvoiceResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(Const.Tag.INVOICE_FRAGMENT, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    private void setShowInvoice(String orderId) {
        Utils.showCustomProgressDialog(orderDetailActivity, false);
        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.SERVER_TOKEN, orderDetailActivity.preferenceHelper.getSessionToken());
        map.put(Const.Params.USER_ID, orderDetailActivity.preferenceHelper.getUserId());
        map.put(ORDER_ID, orderId);
        map.put(Const.Params.IS_USER_SHOW_INVOICE, true);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.setShowInvoice(map);
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(@NonNull Call<IsSuccessResponse> call, @NonNull Response<IsSuccessResponse> response) {
                Utils.hideCustomProgressDialog();
                if (response.body().isSuccess()) {
                    dismiss();
                    orderDetailActivity.getActiveOrderResponse().setSubmitInvoice(true);
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), orderDetailActivity);
                }
            }

            @Override
            public void onFailure(@NonNull Call<IsSuccessResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(Const.Tag.INVOICE_FRAGMENT, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    private String setInvoiceMessage() {
        String message = "";
        if (orderPayment.isStorePayDeliveryFees() && orderPayment.isPaymentModeCash()) {
            message = requireActivity().getResources().getString(R.string.msg_delivery_fee_free_and_cash_pay);
        } else if (orderPayment.isStorePayDeliveryFees() && !orderPayment.isPaymentModeCash()) {
            message = requireActivity().getResources().getString(R.string.msg_delivery_fee_free_and_other_pay);
        } else if (orderPayment.isPaymentModeCash()) {
            message = requireActivity().getResources().getString(R.string.msg_pay_cash);
        } else {
            message = requireActivity().getResources().getString(R.string.msg_paid_by_card_payment);
        }

        if (orderPayment.getPromoPayment() > 0) {
            String promo = orderPayment.isPromoForDeliveryService() ? requireActivity().getResources().getString(R.string.msg_delivery_promo) : requireActivity().getResources().getString(R.string.msg_order_promo);
            message = message + "\n" + promo;
        }

        return message;
    }

    private void loadInvoiceData(OrderPayment orderPayment, String currency, int deliveryType) {
        CurrentBooking.getInstance().setDeliveryType(deliveryType);
        this.orderPayment = orderPayment;
            orderPayment.setPromoCodeName(promoCodeName);

        List<Invoice> invoiceList = orderDetailActivity.parseContent.parseInvoice(orderPayment, currency, true);
        rcvInvoice.setLayoutManager(new LinearLayoutManager(orderDetailActivity));
        rcvInvoice.setAdapter(new InvoiceAdapter(invoiceList));
        tvOderTotal.setText(String.format("%s%s", currency, orderDetailActivity.parseContent.decimalTwoDigitFormat.format(orderPayment.getPromoPayment() > 0 ? orderPayment.getUserPayPayment() : orderPayment.getTotal())));
        tvInvoiceMsg.setText(setInvoiceMessage());
    }

    private void getOrderDetails() {
        Utils.showCustomProgressDialog(requireActivity(), false);
        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.USER_ID, orderDetailActivity.preferenceHelper.getUserId());
        map.put(Const.Params.SERVER_TOKEN, orderDetailActivity.preferenceHelper.getSessionToken());
        map.put(Const.Params.ORDER_ID, orderDetailActivity.orderID);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderResponse> call = apiInterface.getOrderDetail(map);
        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                Utils.hideCustomProgressDialog();
                if (orderDetailActivity.parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        Order order = response.body().getOrder();
                        if (order.getPromoDetails() != null)
                            promoCodeName = response.body().getOrder().getPromoDetails().getPromoCodeName();
                        loadInvoiceData(orderDetailActivity.getHistoryDetailResponse().getOrderPaymentDetail(),
                                orderDetailActivity.getHistoryDetailResponse().getCurrency(), orderDetailActivity.getHistoryDetailResponse().getOrderDetail().getDeliveryType());
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), requireActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }
}