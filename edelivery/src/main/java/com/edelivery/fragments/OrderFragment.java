package com.edelivery.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.edelivery.OrderAndHistoryActivity;
import com.edelivery.R;
import com.edelivery.adapter.OrdersAdapter;
import com.edelivery.models.datamodels.Order;
import com.edelivery.models.responsemodels.OrdersResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderFragment extends Fragment {

    private OrderAndHistoryActivity activity;
    private RecyclerView rcvOrderNotification;
    private LinearLayout ivEmpty;

    private SwipeRefreshLayout srlNotification;
    private final List<Order> orderList = new ArrayList<>();
    public OrdersAdapter ordersAdapter;
    private boolean isOrderViewed = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (OrderAndHistoryActivity) getActivity();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order, container, false);
        rcvOrderNotification = view.findViewById(R.id.rcvOrder);
        ivEmpty = view.findViewById(R.id.ivEmpty);
        srlNotification = view.findViewById(R.id.swipeRefresh);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        srlNotification.setOnRefreshListener(this::getOrders);
        activity.setColorSwipeToRefresh(srlNotification);
    }

    @Override
    public void onResume() {
        super.onResume();
        new Thread(this::getOrders).start();


    }

    private void getOrders() {
        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.SERVER_TOKEN, activity.preferenceHelper.getSessionToken());
        map.put(Const.Params.USER_ID, activity.preferenceHelper.getUserId());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrdersResponse> responseCall = apiInterface.getOrders(map);
        responseCall.enqueue(new Callback<OrdersResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrdersResponse> call, @NonNull Response<OrdersResponse> response) {
                srlNotification.setRefreshing(false);
                if (activity.parseContent.isSuccessful(response)) {
                    orderList.clear();
                    if (response.body().isSuccess()) {
                        orderList.addAll(response.body().getOrderList());
                        Collections.sort(orderList, (order1, order2) -> activity.compareTwoDate(order1.getCreatedAt(), order2.getCreatedAt()));
                    }
                    initRcvOrders(orderList);
                    updateUiWhenDataAvailable();
                    if (activity.isFromCompleteOrder() && !isOrderViewed) {
                        isOrderViewed = true;

                        Order order = ordersAdapter.getItemList().get(0);
                        activity.goToOrderDetailActivity(order, "", false);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrdersResponse> call, @NonNull Throwable t) {
                srlNotification.setRefreshing(false);
                AppLog.handleThrowable(Const.Tag.CURRENT_ORDER_FRAGMENT, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    private void initRcvOrders(final List<Order> orderList) {
        ordersAdapter = new OrdersAdapter(activity, orderList, position -> {
            if (position > -1) {
                Order order = ordersAdapter.getItemList().get(position);
                activity.goToOrderDetailActivity(order, "", false);
            }
        });
        rcvOrderNotification.setLayoutManager(new LinearLayoutManager(activity));
        rcvOrderNotification.setAdapter(ordersAdapter);

    }

    @SuppressLint("NotifyDataSetChanged")
    public void updateUiWhenDataAvailable() {
        ivEmpty.setVisibility((ordersAdapter != null && ordersAdapter.getItemCount() > 0) ? View.GONE : View.VISIBLE);
        rcvOrderNotification.setVisibility(ordersAdapter == null || ordersAdapter.getItemCount() <= 0 ? View.GONE : View.VISIBLE);

        if (ordersAdapter != null) {
            rcvOrderNotification.post(() -> ordersAdapter.notifyDataSetChanged());
        }

    }
}