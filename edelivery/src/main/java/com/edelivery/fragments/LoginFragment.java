package com.edelivery.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.edelivery.BuildConfig;
import com.edelivery.LoginActivity;
import com.edelivery.R;
import com.edelivery.component.AbstractCustomCountryDialog;
import com.edelivery.component.AbstractDialogForgotPassword;
import com.edelivery.component.AbstractDialogForgotPasswordOTPVerification;
import com.edelivery.component.AbstractDialogResetPassword;
import com.edelivery.component.AbstractServerDialog;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.interfaces.AbstractTripleTapListener;
import com.edelivery.models.datamodels.Country;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.UserDataResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.models.validations.Validator;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.persistentroomdata.notification.NotificationRepository;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.FieldValidation;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.ServerConfig;
import com.edelivery.utils.Utils;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.SignInButton;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends BottomSheetDialogFragment implements TextView.OnEditorActionListener, View.OnClickListener {

    private EditText etLoginEmail;
    private EditText etLoginPassword;
    private EditText etCountryPhoneCode;
    private CustomFontTextView tvForgotPassword;
    private CustomFontTextView tvChangeLogin;
    private CustomFontButton btnLogin;
    private TextInputLayout tilEmail;
    private TextInputLayout tilPassword;
    private TextInputLayout tilCountryPhoneCode;
    private LoginActivity loginActivity;
    private AbstractDialogForgotPassword dialogForgotPassword;
    private LinearLayout llSocialLogin;
    private Spinner spinnerLanguage;
    private View ivAppLogo;
    private int loginType;
    private AbstractCustomCountryDialog customCountryDialog;
    private Country country;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.loginActivity = (LoginActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(loginActivity).inflate(R.layout.fragment_login, container, false);
        etLoginEmail = view.findViewById(R.id.etLoginEmail);
        etLoginPassword = view.findViewById(R.id.etLoginPassword);
        tvForgotPassword = view.findViewById(R.id.tvForgotPassword);
        btnLogin = view.findViewById(R.id.btnLogin);
        tilEmail = view.findViewById(R.id.tilEmail);
        tilPassword = view.findViewById(R.id.tilPassword);
        tilCountryPhoneCode = view.findViewById(R.id.tilCountryPhoneCode);
        etCountryPhoneCode = view.findViewById(R.id.etCountryPhoneCode);
        spinnerLanguage = view.findViewById(R.id.spinnerLanguage);
        ivAppLogo = view.findViewById(R.id.ivAppLogo);
        tvChangeLogin = view.findViewById(R.id.tvChangeLogin);
        TextView btnRegisterNow = view.findViewById(R.id.btnRegisterNow);
        btnRegisterNow.setOnClickListener(this);
        llSocialLogin = view.findViewById(R.id.llSocialButton);
        SignInButton btnGoogleLogin = view.findViewById(R.id.btnGoogleLogin);
        loginActivity.initFBLogin(view);
        loginActivity.initGoogleLogin(view);
        loginActivity.initTwitterLogin(view);
        TextView textView = (TextView) btnGoogleLogin.getChildAt(0);
        textView.setText(getResources().getString(R.string.google_sign_up));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        enabledLoginBy();
        checkSocialLoginISOn(loginActivity.preferenceHelper.getIsLoginBySocial());
        initLanguageSpinner();
        tvForgotPassword.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        etLoginPassword.setOnEditorActionListener(this);
        tvChangeLogin.setOnClickListener(this);
        etCountryPhoneCode.setOnClickListener(this);
        setCountry(loginActivity.getLocatedCountry());
        if (BuildConfig.APPLICATION_ID.equalsIgnoreCase("com.elluminatiinc.edelivery.user")) {
            ivAppLogo.setOnTouchListener(new AbstractTripleTapListener() {
                @Override
                protected void onTripleTap() {
                    showServerDialog();
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() instanceof BottomSheetDialog) {
            BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
            BottomSheetBehavior<?> behavior = dialog.getBehavior();
            behavior.setDraggable(false);
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnLogin) {
            if (isValidate()) {
                if(loginActivity.preferenceHelper.isUseCaptcha()) {
                    loginActivity.checkSafetyNet(token -> login("",token));
                } else {
                     login("","");
                }
            }
        } else if (id == R.id.tvForgotPassword) {
            openForgotPasswordDialog();
        } else if (id == R.id.btnRegisterNow) {
            loginActivity.swipeLoginAndRegister(false);
        } else if (id == R.id.tvChangeLogin) {
            if (TextUtils.equals(tvChangeLogin.getText().toString(), getString(R.string.text_use_phone_number))) {
                tvChangeLogin.setText(getString(R.string.text_use_email));

                loginType = Const.LoginType.PHONE_NUMBER;
                tilEmail.setHint(getString(R.string.text_phone));
                etLoginEmail.setInputType(InputType.TYPE_CLASS_PHONE);
                FieldValidation.setMaxPhoneNumberInputLength(loginActivity, etLoginEmail);
                tilCountryPhoneCode.setVisibility(View.VISIBLE);
            } else {
                tvChangeLogin.setText(getString(R.string.text_use_phone_number));

                loginType = Const.LoginType.EMAIL;
                tilEmail.setHint(getString(R.string.text_email));
                etLoginEmail.setInputType(InputType.TYPE_CLASS_TEXT);
                etLoginEmail.setFilters(new InputFilter[]{});
                tilCountryPhoneCode.setVisibility(View.GONE);
            }
            etLoginEmail.setText(null);
            tilEmail.setError(null);
        } else if (id == R.id.etCountryPhoneCode && loginActivity.getCountryList() != null) {
                openCountryCodeDialog();
        }
    }

    protected boolean isValidate() {
        String msg = null;
        Validator emailValidation = FieldValidation.isEmailValid(loginActivity, etLoginEmail.getText().toString().trim());
        if (loginType == Const.LoginType.EMAIL) {
            if (!emailValidation.isValid()) {
                msg = emailValidation.getErrorMsg();
                tilEmail.setError(msg);
                etLoginEmail.requestFocus();
            } else {
                msg = validatePassword();
            }
        } else if (loginType == Const.LoginType.PHONE_NUMBER) {
            if (country == null) {
                msg = loginActivity.getString(R.string.msg_please_select_country);
                Utils.showToast(msg, loginActivity);
            } else if (!FieldValidation.isValidPhoneNumber(loginActivity, etLoginEmail.getText().toString())) {
                msg = FieldValidation.getPhoneNumberValidationMessage(loginActivity);
                tilEmail.setError(msg);
                etLoginEmail.requestFocus();
            } else {
                msg = validatePassword();
            }
        }
        return TextUtils.isEmpty(msg);
    }

    private String validatePassword() {
        String msg = null;
        Validator passwordValidation = FieldValidation.isPasswordValid(loginActivity, etLoginPassword.getText().toString().trim());

        if (!passwordValidation.isValid()) {
            msg = passwordValidation.getErrorMsg();
            tilPassword.setError(msg);
            etLoginPassword.requestFocus();
        }
        return msg;
    }

    /**
     * this method call a webservice for login
     */
    public void login(String socialId, String reCaptchaToken) {
        HashMap<String, Object> map = new HashMap<>();
        if (TextUtils.isEmpty(socialId)) {
            map.put(Const.Params.EMAIL, etLoginEmail.getText().toString());
            map.put(Const.Params.PASS_WORD, etLoginPassword.getText().toString());
            map.put(Const.Params.SOCIAL_ID, socialId);
            map.put(Const.Params.LOGIN_BY, Const.MANUAL);
            map.put(Const.Params.LOGIN_TYPE, loginType);
            map.put(Const.Params.COUNTRY_PHONE_CODE, etCountryPhoneCode.getText().toString());
        } else {
            map.put(Const.Params.EMAIL, "");
            map.put(Const.Params.PASS_WORD, "");
            map.put(Const.Params.SOCIAL_ID, socialId);
            map.put(Const.Params.LOGIN_BY, Const.SOCIAL);
        }
        map.put(Const.Params.CART_UNIQUE_TOKEN, loginActivity.preferenceHelper.getAndroidId());
        map.put(Const.Params.DEVICE_TYPE, Const.ANDROID);
        map.put(Const.Params.DEVICE_TOKEN, loginActivity.preferenceHelper.getDeviceToken());
        map.put(Const.Params.APP_VERSION, loginActivity.getAppVersion());
        map.put(Const.Params.CAPTCHA_TOKEN, reCaptchaToken);

        Utils.showCustomProgressDialog(loginActivity, false);
        ApiInterface loginInterface = ApiClient.getClient().create(ApiInterface.class);
        final Call<UserDataResponse> login = loginInterface.login(map);
        login.enqueue(new Callback<UserDataResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserDataResponse> call, @NonNull Response<UserDataResponse> response) {
                if (loginActivity.parseContent.parseUserStorageData(response)) {
                    Utils.showMessageToast(response.body().getStatusPhrase(), loginActivity);
                    if (loginActivity.isFromCheckOut()) {
                        loginActivity.setResult(Activity.RESULT_OK);
                        loginActivity.finishAfterTransition();
                    } else {
                        CurrentBooking.getInstance().setBookCityId("");
                        loginActivity.goToHomeActivity();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserDataResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(Const.Tag.LOG_IN_FRAGMENT, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    private void openForgotPasswordDialog() {
        if (dialogForgotPassword != null && dialogForgotPassword.isShowing()) {
            return;
        }

        dialogForgotPassword = new AbstractDialogForgotPassword(loginActivity, loginActivity.getCountryList(), country) {

            @Override
            public void onClickOkButton(String emailOrPhoneNumber, String countryPhoneCode, int loginType) {
                if (loginActivity.preferenceHelper.isUseCaptcha()) {
                    loginActivity.checkSafetyNet(token -> forgotPassword(emailOrPhoneNumber, countryPhoneCode, loginType,token));
                } else {
                    forgotPassword(emailOrPhoneNumber, countryPhoneCode, loginType, "");
                }
            }
        };

        dialogForgotPassword.show();
    }

    /**
     * this method call webservice for forgot password
     */
    private void forgotPassword(String emailOrPhoneNumber, String countryPhoneCode, int loginType, String reCaptchaToken) {
        Utils.showCustomProgressDialog(loginActivity, false);
        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.TYPE, Const.Type.USER);
        if (loginType == Const.LoginType.PHONE_NUMBER) {
            map.put(Const.Params.COUNTRY_PHONE_CODE, countryPhoneCode);
            map.put(Const.Params.PHONE, emailOrPhoneNumber);
        } else {
            map.put(Const.Params.EMAIL, emailOrPhoneNumber);
        }
        map.put(Const.Params.CAPTCHA_TOKEN, reCaptchaToken);
        map.put(Const.Params.DEVICE_TYPE, Const.ANDROID);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.forgotPassword(map);
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(@NonNull Call<IsSuccessResponse> call, @NonNull Response<IsSuccessResponse> response) {
                Utils.hideCustomProgressDialog();
                if (loginActivity.parseContent.isSuccessful(response)) {
                    if (Objects.requireNonNull(response.body()).isSuccess()) {
                        if (dialogForgotPassword != null && dialogForgotPassword.isShowing()) {
                            dialogForgotPassword.dismiss();
                        }
                        AbstractDialogForgotPasswordOTPVerification verification = new AbstractDialogForgotPasswordOTPVerification(loginActivity, emailOrPhoneNumber, countryPhoneCode, loginType) {
                            @Override
                            public void optVerifySuccessfully(String id, String severToken) {
                                AbstractDialogResetPassword resetPassword = new AbstractDialogResetPassword(loginActivity, id, severToken) {
                                };
                                resetPassword.show();
                            }
                        };
                        verification.show();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), loginActivity);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<IsSuccessResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(Const.Tag.LOG_IN_FRAGMENT, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (textView.getId() == R.id.etLoginPassword && actionId == EditorInfo.IME_ACTION_DONE) {

                if (isValidate()) {
                    if(loginActivity.preferenceHelper.isUseCaptcha()) {
                        loginActivity.checkSafetyNet(token -> login("",token));
                    } else {
                        login("","");
                    }
                }
                return true;
            }

        return false;
    }

    private void enabledLoginBy() {
        if (loginActivity.preferenceHelper.getIsLoginByEmail() && loginActivity.preferenceHelper.getIsLoginByPhone()) {
            tilEmail.setHint(getResources().getString(R.string.text_email));
            tvChangeLogin.setVisibility(View.VISIBLE);
            tilCountryPhoneCode.setVisibility(View.GONE);
            loginType = Const.LoginType.EMAIL;
        } else if (loginActivity.preferenceHelper.getIsLoginByPhone()) {
            tilEmail.setHint(getResources().getString(R.string.text_phone));
            tvChangeLogin.setVisibility(View.GONE);
            etLoginEmail.setInputType(InputType.TYPE_CLASS_PHONE);
            FieldValidation.setMaxPhoneNumberInputLength(loginActivity, etLoginEmail);
            tilCountryPhoneCode.setVisibility(View.VISIBLE);
            loginType = Const.LoginType.PHONE_NUMBER;
        } else {
            tilEmail.setHint(getResources().getString(R.string.text_email));
            tvChangeLogin.setVisibility(View.GONE);
            tilCountryPhoneCode.setVisibility(View.GONE);
            loginType = Const.LoginType.EMAIL;
        }
    }

    private void openCountryCodeDialog() {
        if (customCountryDialog != null && customCountryDialog.isShowing()) {
            return;
        }

        customCountryDialog = new AbstractCustomCountryDialog(loginActivity, loginActivity.getCountryList()) {
            @Override
            public void onSelect(Country country) {
                setCountry(country);
                dismiss();
            }
        };
        customCountryDialog.show();
    }

    public void setCountry(Country country) {
        if (country == null) {
            return;
        }
        if (this.country == null || !TextUtils.equals(this.country.getCode(), country.getCode())) {
            this.country = country;
            etCountryPhoneCode.setText(country.getCallingCode());
            tilCountryPhoneCode.setError(null);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void checkSocialLoginISOn(boolean isSocialLogin) {
        if (isSocialLogin) {
            llSocialLogin.setVisibility(View.VISIBLE);
        } else {
            llSocialLogin.setVisibility(View.GONE);
        }
    }

    private void initLanguageSpinner() {
        TypedArray array = loginActivity.getResources().obtainTypedArray(R.array.language_code);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(loginActivity, R.array.language_name, R.layout.spiner_view_small);
        adapter.setDropDownViewResource(R.layout.item_spiner_view_small);
        spinnerLanguage.setAdapter(adapter);

        spinnerLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String languageCode = array.getString(i);
                if (!TextUtils.equals(loginActivity.preferenceHelper.getLanguageCode(), languageCode)) {
                    loginActivity.preferenceHelper.putLanguageCode(languageCode);
                    loginActivity.preferenceHelper.putLanguageIndex(loginActivity.getLangIndxex(languageCode, loginActivity.currentBooking.getLangs(), false));
                    CurrentBooking.getInstance().setLanguageChanged(true);
                    loginActivity.finishAffinity();
                    loginActivity.restartApp();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
//nothing here
            }
        });

        int size = array.length();
        for (int i = 0; i < size; i++) {
            if (TextUtils.equals(loginActivity.preferenceHelper.getLanguageCode(), array.getString(i))) {
                spinnerLanguage.setSelection(i);
                break;
            }
        }
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (loginActivity != null && loginActivity.getRegisterFragment() == null && !loginActivity.isFinishing()) {
            loginActivity.onBackPressed();
        }
    }

    private void showServerDialog() {
        AbstractServerDialog serverDialog = new AbstractServerDialog(loginActivity) {
            @Override
            public void onOkClicked() {
                PreferenceHelper.getInstance(loginActivity).putBaseUrl(ServerConfig.baseUrl);
                PreferenceHelper.getInstance(loginActivity).putUserPanelUrl(ServerConfig.userPanelUrl);
                PreferenceHelper.getInstance(loginActivity).putImageUrl(ServerConfig.imageUrl);
                PreferenceHelper.getInstance(loginActivity).putSocketUrl(ServerConfig.socketUrl);

                loginActivity.preferenceHelper.putAndroidId(Utils.generateRandomString());
                loginActivity.preferenceHelper.clearVerification();
                loginActivity.preferenceHelper.logout();
                LoginManager.getInstance().logOut();
                NotificationRepository.getInstance(loginActivity).clearNotification();
                loginActivity.currentBooking.clearCurrentBookingModel();
                loginActivity.goToSplashActivity();
            }
        };
        serverDialog.show();
    }
}