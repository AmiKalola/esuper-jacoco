package com.edelivery.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.edelivery.OrderAndHistoryActivity;
import com.edelivery.OrdersActivity;
import com.edelivery.R;
import com.edelivery.adapter.HistoryAdapter;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.models.datamodels.OrderHistory;
import com.edelivery.models.responsemodels.OrderHistoryResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderHistoryFragment extends Fragment {

    private OrderAndHistoryActivity activity;
    private RecyclerView rcvOrderNotification;
    private LinearLayout ivEmpty;
    private SwipeRefreshLayout srlNotification;
    public HistoryAdapter ordersHistoryAdapter;
    private final List<OrderHistory> orderHistories = new ArrayList<>();
    private View ivFilter;
    private Calendar calendar;
    private CustomFontEditTextView tvFromDate;
    private CustomFontEditTextView tvToDate;
    private boolean isFromDateSet;
    private boolean isToDateSet;
    private long fromDateSetTime;
    private long toDateSetTime;
    private BottomSheetDialog historyFilterDialog;
    private int day;
    private int month;
    private int currentYear;
    private DatePickerDialog.OnDateSetListener fromDateSet;
    private DatePickerDialog.OnDateSetListener toDateSet;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (OrderAndHistoryActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order, container, false);
        rcvOrderNotification = view.findViewById(R.id.rcvOrder);
        ivEmpty = view.findViewById(R.id.ivEmpty);
        ivFilter = view.findViewById(R.id.ivFilter);
        srlNotification = view.findViewById(R.id.swipeRefresh);
        Utils.showCustomProgressDialog(activity, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        srlNotification.setOnRefreshListener(() -> getOrderHistory("", ""));
        activity.setColorSwipeToRefresh(srlNotification);
        setViewListener();
    }

    private void setViewListener() {
        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        currentYear = calendar.get(Calendar.YEAR);
        fromDateSet = (view, year, monthOfYear, dayOfMonth) -> {
            calendar.clear();
            calendar.set(year, monthOfYear, dayOfMonth);
            fromDateSetTime = calendar.getTimeInMillis();
            isFromDateSet = true;
            tvFromDate.setText(activity.parseContent.dateFormat.format(calendar.getTime()));
        };
        toDateSet = (view, year, monthOfYear, dayOfMonth) -> {
            calendar.clear();
            calendar.set(year, monthOfYear, dayOfMonth);
            toDateSetTime = calendar.getTimeInMillis();
            isToDateSet = true;
            tvToDate.setText(activity.parseContent.dateFormat.format(calendar.getTime()));
        };
        ivFilter.setOnClickListener(v -> openHistoryFilterDialog());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisible())
            getOrderHistory("", "");
    }

    private void getOrderHistory(String fromDate, String toDate) {

        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.START_DATE, fromDate);
        map.put(Const.Params.END_DATE, toDate);
        map.put(Const.Params.USER_ID, activity.preferenceHelper.getUserId());
        map.put(Const.Params.SERVER_TOKEN, activity.preferenceHelper.getSessionToken());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderHistoryResponse> responseCall = apiInterface.getOrdersHistory(map);
        responseCall.enqueue(new Callback<OrderHistoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderHistoryResponse> call, @NonNull Response<OrderHistoryResponse> response) {
                Utils.hideCustomProgressDialog();
                if (activity.parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        orderHistories.clear();
                        orderHistories.addAll(response.body().getOrderList());
                    }else
                    {
                        orderHistories.clear();
                    }
                    initRcvHistoryList();
                    updateUiWhenDataAvailable();
                    srlNotification.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderHistoryResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(OrdersActivity.class.getName(), t);
                Utils.hideCustomProgressDialog();
                srlNotification.setRefreshing(false);
            }
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    private void initRcvHistoryList() {
        Collections.sort(orderHistories, (lhs, rhs) -> activity.compareTwoDate(lhs.getCreatedAt(), rhs.getCreatedAt()));
        if (ordersHistoryAdapter != null) {
            ordersHistoryAdapter.notifyDataSetChanged();
        } else {
            rcvOrderNotification.setLayoutManager(new LinearLayoutManager(activity));
            ordersHistoryAdapter = new HistoryAdapter(activity, orderHistories, position -> {
                OrderHistory orderHistory = ordersHistoryAdapter.getItemList().get(position);
                activity.goToOrderDetailActivity(null, orderHistory.getId(), true);
            });
            rcvOrderNotification.setAdapter(ordersHistoryAdapter);
        }


    }

    @SuppressLint("NotifyDataSetChanged")
    public void updateUiWhenDataAvailable() {
        ivEmpty.setVisibility((ordersHistoryAdapter != null && ordersHistoryAdapter.getItemCount() > 0) ? View.GONE : View.VISIBLE);
        rcvOrderNotification.setVisibility(ordersHistoryAdapter == null || ordersHistoryAdapter.getItemCount() <= 0 ? View.GONE : View.VISIBLE);
        ivFilter.setVisibility(ordersHistoryAdapter == null || ordersHistoryAdapter.getItemCount() <= 0 ? View.GONE : View.VISIBLE);

        if (ordersHistoryAdapter != null) {
            rcvOrderNotification.post(() -> ordersHistoryAdapter.notifyDataSetChanged());
        }

    }

    private void openHistoryFilterDialog() {
        if (historyFilterDialog != null && historyFilterDialog.isShowing()) {
            return;
        }
        historyFilterDialog = new BottomSheetDialog(activity);
        historyFilterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        historyFilterDialog.setContentView(R.layout.dialog_history_filter);
        tvFromDate = historyFilterDialog.findViewById(R.id.tvFromDate);
        tvToDate = historyFilterDialog.findViewById(R.id.tvToDate);
        tvFromDate.setOnClickListener(view -> openFromDatePicker());
        tvToDate.setOnClickListener(view -> openToDatePicker());
        historyFilterDialog.findViewById(R.id.tvHistoryReset).setOnClickListener(view -> {
            clearData();
            getOrderHistory("", "");
        });
        historyFilterDialog.findViewById(R.id.tvHistoryApply).setOnClickListener(view -> {
            if (fromDateSetTime > 0 && toDateSetTime > 0) {
                getOrderHistory(activity.parseContent.dateFormat2.format(new Date(fromDateSetTime)), activity.parseContent.dateFormat2.format(new Date(toDateSetTime)));
                clearData();
                historyFilterDialog.dismiss();
            } else {
                Utils.showToast(getResources().getString(R.string.msg_plz_select_valid_date), activity);
            }
        });

        historyFilterDialog.findViewById(R.id.btnNegative).setOnClickListener(view -> historyFilterDialog.dismiss());
        WindowManager.LayoutParams params = historyFilterDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        historyFilterDialog.show();
    }

    private void clearData() {
        isFromDateSet = false;
        isToDateSet = false;
        tvToDate.setText(getResources().getString(R.string.text_to));
        tvFromDate.setText(getResources().getString(R.string.text_from));
        fromDateSetTime = 0;
        toDateSetTime = 0;
        calendar.setTimeInMillis(System.currentTimeMillis());
    }

    private void openFromDatePicker() {
        DatePickerDialog fromPiker = new DatePickerDialog(activity, fromDateSet, currentYear, month, day);
        fromPiker.setTitle(getResources().getString(R.string.text_select_from_date));
        fromPiker.getDatePicker();
        if (isToDateSet) {
            fromPiker.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        } else {
            fromPiker.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
        }
        fromPiker.show();
    }

    private void openToDatePicker() {
        DatePickerDialog toPiker = new DatePickerDialog(activity, toDateSet, currentYear, month, day);
        toPiker.setTitle(getResources().getString(R.string.text_select_to_date));
        if (isFromDateSet) {
            toPiker.getDatePicker().setMaxDate(System.currentTimeMillis());
            toPiker.getDatePicker().setMinDate(fromDateSetTime);
        } else {
            toPiker.getDatePicker().setMaxDate(System.currentTimeMillis());
        }
        toPiker.show();
    }
}