package com.edelivery.models.datamodels;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryDetail implements Parcelable {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("default_image_url")
    @Expose
    private String defaultImageUrl;

    public DeliveryDetail() {

    }

    protected DeliveryDetail(Parcel in) {
        id = in.readString();
        defaultImageUrl = in.readString();
    }

    public static final Creator<DeliveryDetail> CREATOR = new Creator<DeliveryDetail>() {
        @Override
        public DeliveryDetail createFromParcel(Parcel in) {
            return new DeliveryDetail(in);
        }

        @Override
        public DeliveryDetail[] newArray(int size) {
            return new DeliveryDetail[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDefaultImageUrl() {
        return defaultImageUrl;
    }

    public void setDefaultImageUrl(String defaultImageUrl) {
        this.defaultImageUrl = defaultImageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int i) {
        dest.writeString(this.defaultImageUrl);
        dest.writeString(this.id);
    }
}
