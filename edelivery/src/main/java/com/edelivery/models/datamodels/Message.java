package com.edelivery.models.datamodels;

import com.google.gson.annotations.SerializedName;

public class Message {
    @SerializedName("id")
    private String id;
    @SerializedName("chat_type")
    private int chatType;
    @SerializedName("message")
    private String messageString;
    @SerializedName("time")
    private String time;
    @SerializedName("sender_type")
    private int senderType;
    @SerializedName("is_read")
    private boolean isRead;
    @SerializedName("receiver_id")
    private String receiverId;

    public Message() {
    }

    public Message(String id,
                   int chatType,
                   String messageString,
                   String time,
                   int senderType,
                   String receiverId,
                   boolean isRead) {
        this.id = id;
        this.chatType = chatType;
        this.messageString = messageString;
        this.time = time;
        this.senderType = senderType;
        this.isRead = isRead;
        this.receiverId = receiverId;
    }

    public int getChatType() {
        return chatType;
    }

    public void setChatType(int chatType) {
        this.chatType = chatType;
    }

    public String getMessageString() {
        return messageString;
    }

    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getSenderType() {
        return senderType;
    }

    public void setSenderType(int senderType) {
        this.senderType = senderType;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        this.isRead = read;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }
}
