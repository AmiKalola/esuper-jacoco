package com.edelivery.models.datamodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Country {
    @SerializedName("country_phone_code")
    private String callingCode;

    @SerializedName("calling_codes")
    private List<String> callingCodes;

    @SerializedName("country_flag")
    private String flag;

    @SerializedName("country_code")
    private String code;

    @SerializedName("country_name")
    private String name;

    @SerializedName("code_3")
    private String code3;

    @SerializedName("currency_code")
    private String currencies;

    public List<String> getCallingCodes() {
        return callingCodes;
    }

    public String getFlag() {
        return flag;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getCode3() {
        return code3;
    }

    public String getCurrencies() {
        return currencies;
    }

    public String getCallingCode() {
        return callingCode;
    }

    public void setCallingCode(String callingCode) {
        this.callingCode = callingCode;
    }
}