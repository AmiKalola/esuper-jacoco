package com.edelivery.models.datamodels;

import com.google.gson.annotations.SerializedName;

public class ServiceData {
    @SerializedName("is_waiting_time")
    private boolean isWaitingTime;
    @SerializedName("is_waiting_time_every_stop")
    private boolean isWaitingTimeEveryStop;
    @SerializedName("waiting_time_after_min")
    private long waitingTimeAfterMin;
    @SerializedName("waiting_time_price")
    private double waitingTimePrice;

    public boolean isWaitingTime() {
        return isWaitingTime;
    }

    public void setWaitingTime(boolean waitingTime) {
        isWaitingTime = waitingTime;
    }

    public boolean isWaitingTimeEveryStop() {
        return isWaitingTimeEveryStop;
    }

    public void setWaitingTimeEveryStop(boolean waitingTimeEveryStop) {
        isWaitingTimeEveryStop = waitingTimeEveryStop;
    }

    public long getWaitingTimeAfterMin() {
        return waitingTimeAfterMin;
    }

    public void setWaitingTimeAfterMin(long waitingTimeAfterMin) {
        this.waitingTimeAfterMin = waitingTimeAfterMin;
    }

    public double getWaitingTimePrice() {
        return waitingTimePrice;
    }

    public void setWaitingTimePrice(double waitingTimePrice) {
        this.waitingTimePrice = waitingTimePrice;
    }
}
