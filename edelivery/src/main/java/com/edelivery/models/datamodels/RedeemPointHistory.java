package com.edelivery.models.datamodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class RedeemPointHistory implements Parcelable {

    public static final Creator<RedeemPointHistory> CREATOR = new Creator<RedeemPointHistory>() {
        @Override
        public RedeemPointHistory createFromParcel(Parcel source) {
            return new RedeemPointHistory(source);
        }

        @Override
        public RedeemPointHistory[] newArray(int size) {
            return new RedeemPointHistory[size];
        }
    };

    @SerializedName("_id")
    private String id;
    @SerializedName("user_type")
    private int userType;
    @SerializedName("user_unique_id")
    private int userUniqueId;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("country_id")
    private String countryId;
    @SerializedName("redeem_point_type")
    private int redeemPointType;
    @SerializedName("redeem_point_currency")
    private String redeemPointCurrency;
    @SerializedName("redeem_point_description")
    private String redeemPointDescription;
    @SerializedName("added_redeem_point")
    private int addedRedeemPoint;
    @SerializedName("total_redeem_point")
    private int totalRedeemPoint;
    @SerializedName("redeem_status")
    private int redeemStatus;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("unique_id")
    private int uniqueId;


    public RedeemPointHistory() {
    }

    protected RedeemPointHistory(Parcel in) {
        this.id = in.readString();
        this.userType = in.readInt();
        this.userUniqueId = in.readInt();
        this.userId = in.readString();
        this.countryId = in.readString();
        this.redeemPointType = in.readInt();
        this.redeemPointCurrency = in.readString();
        this.redeemPointDescription = in.readString();
        this.addedRedeemPoint = in.readInt();
        this.totalRedeemPoint = in.readInt();
        this.redeemStatus = in.readInt();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.uniqueId = in.readInt();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public int getUserUniqueId() {
        return userUniqueId;
    }

    public void setUserUniqueId(int userUniqueId) {
        this.userUniqueId = userUniqueId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public int getRedeemPointType() {
        return redeemPointType;
    }

    public void setRedeemPointType(int redeemPointType) {
        this.redeemPointType = redeemPointType;
    }

    public String getRedeemPointCurrency() {
        return redeemPointCurrency;
    }

    public void setRedeemPointCurrency(String redeemPointCurrency) {
        this.redeemPointCurrency = redeemPointCurrency;
    }

    public String getRedeemPointDescription() {
        return redeemPointDescription;
    }

    public void setRedeemPointDescription(String redeemPointDescription) {
        this.redeemPointDescription = redeemPointDescription;
    }

    public int getAddedRedeemPoint() {
        return addedRedeemPoint;
    }

    public void setAddedRedeemPoint(int addedRedeemPoint) {
        this.addedRedeemPoint = addedRedeemPoint;
    }

    public int getTotalRedeemPoint() {
        return totalRedeemPoint;
    }

    public void setTotalRedeemPoint(int totalRedeemPoint) {
        this.totalRedeemPoint = totalRedeemPoint;
    }

    public int getRedeemStatus() {
        return redeemStatus;
    }

    public void setRedeemStatus(int redeemStatus) {
        this.redeemStatus = redeemStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeInt(this.userType);
        dest.writeInt(this.userUniqueId);
        dest.writeString(this.userId);
        dest.writeString(this.countryId);
        dest.writeInt(this.redeemPointType);
        dest.writeString(this.redeemPointCurrency);
        dest.writeString(this.redeemPointDescription);
        dest.writeInt(this.addedRedeemPoint);
        dest.writeInt(this.totalRedeemPoint);
        dest.writeInt(this.redeemStatus);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeInt(this.uniqueId);
    }
}