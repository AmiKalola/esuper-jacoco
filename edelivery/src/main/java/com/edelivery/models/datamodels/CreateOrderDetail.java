package com.edelivery.models.datamodels;

import com.google.gson.annotations.SerializedName;

public class CreateOrderDetail {
    @SerializedName("delivery_user_name")
    private String deliveryUserName;
    @SerializedName("delivery_user_phone")
    private String deliveryUserPhone;
    @SerializedName("delivery_note")
    private String deliveryNote;
    @SerializedName("is_allow_contactless_delivery")
    private boolean isAllowContactLessDelivery;
    @SerializedName("order_start_at2")
    private long orderStartAt2;
    @SerializedName("order_start_at")
    private long orderStartAt;
    @SerializedName("delivery_type")
    private int deliveryType;
    @SerializedName("is_bring_change")
    private boolean isBringChange;
    @SerializedName("vehicle_id")
    private String vehicleId;

    public void setDeliveryUserName(String deliveryUserName) {
        this.deliveryUserName = deliveryUserName;
    }

    public String getDeliveryUserName() {
        return deliveryUserName;
    }

    public void setDeliveryUserPhone(String deliveryUserPhone) {
        this.deliveryUserPhone = deliveryUserPhone;
    }

    public String getDeliveryUserPhone() {
        return deliveryUserPhone;
    }

    public void setDeliveryNote(String deliveryNote) {
        this.deliveryNote = deliveryNote;
    }

    public String getDeliveryNote() {
        return deliveryNote;
    }

    public void setAllowContactLessDelivery(boolean allowContactLessDelivery) {
        isAllowContactLessDelivery = allowContactLessDelivery;
    }

    public boolean isAllowContactLessDelivery() {
        return isAllowContactLessDelivery;
    }

    public void setOrderStartAt2(long orderStartAt2) {
        this.orderStartAt2 = orderStartAt2;
    }

    public long getOrderStartAt2() {
        return orderStartAt2;
    }

    public void setOrderStartAt(long orderStartAt) {
        this.orderStartAt = orderStartAt;
    }

    public long getOrderStartAt() {
        return orderStartAt;
    }

    public void setDeliveryType(int deliveryType) {
        this.deliveryType = deliveryType;
    }

    public int getDeliveryType() {
        return deliveryType;
    }

    public void setBringChange(boolean bringChange) {
        isBringChange = bringChange;
    }

    public boolean isBringChange() {
        return isBringChange;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }
}