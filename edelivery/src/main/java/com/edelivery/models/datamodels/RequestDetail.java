package com.edelivery.models.datamodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RequestDetail implements Parcelable {
    @SerializedName("city_id")
    private String cityId;
    @SerializedName("vehicle_id")
    private String vehicleId;
    @SerializedName("date_time")
    private List<Status> statusTime;

    protected RequestDetail(Parcel in) {
        cityId = in.readString();
        vehicleId = in.readString();
        statusTime = in.createTypedArrayList(Status.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cityId);
        dest.writeString(vehicleId);
        dest.writeTypedList(statusTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RequestDetail> CREATOR = new Creator<RequestDetail>() {
        @Override
        public RequestDetail createFromParcel(Parcel in) {
            return new RequestDetail(in);
        }

        @Override
        public RequestDetail[] newArray(int size) {
            return new RequestDetail[size];
        }
    };

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public List<Status> getStatusTime() {
        return statusTime;
    }

    public void setStatusTime(List<Status> statusTime) {
        this.statusTime = statusTime;
    }
}
