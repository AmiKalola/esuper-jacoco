package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchSendUserResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("user_detail")
    @Expose
    private User userDetail;

    @SerializedName("error_code")
    @Expose
    private int errorCode;

    @SerializedName("status_phrase")
    @Expose
    private String statusPhrase;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public User getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(User userDetail) {
        this.userDetail = userDetail;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public void setStatusPhrase(String statusPhrase) {
        this.statusPhrase = statusPhrase;
    }

    public String getStatusPhrase() {
        return statusPhrase;
    }
}
