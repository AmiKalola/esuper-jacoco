package com.edelivery.models.responsemodels;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CancellationChargeResponse extends IsSuccessResponse {

    @SerializedName("cancellation_charge")
    private Double cancellationCharge;

    @SerializedName("cancellation_reason")
    private List<String> cancellationReason;

    public Double getCancellationCharge() {
        return cancellationCharge;
    }

    public List<String> getCancellationReason() {
        if (cancellationReason == null) {
            return new ArrayList<>();
        } else {
            return cancellationReason;
        }
    }
}
