package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.RedeemPointHistory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RedeemPointHistoryResponse {

    @SerializedName("success")
    private boolean success;

    @SerializedName("message")
    private int message;
    @SerializedName("status_phrase")
    private String statusPhrase;
    @SerializedName("error_code")
    @Expose
    private int errorCode;

    @SerializedName("redeem_history")
    private List<RedeemPointHistory> walletHistory;

    @SerializedName("total_redeem_point")
    private int totalRedeemPoint;

    @SerializedName("user_redeem_point_value")
    private String userRedeemPointValue;

    @SerializedName("user_minimum_point_require_for_withdrawal")
    private int userMinimumPointRequireForWithdrawal;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public String getStatusPhrase() {
        return statusPhrase;
    }

    public void setStatusPhrase(String statusPhrase) {
        this.statusPhrase = statusPhrase;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public List<RedeemPointHistory> getWalletHistory() {
        return walletHistory;
    }

    public void setWalletHistory(List<RedeemPointHistory> walletHistory) {
        this.walletHistory = walletHistory;
    }

    public int getTotalRedeemPoint() {
        return totalRedeemPoint;
    }

    public void setTotalRedeemPoint(int totalRedeemPoint) {
        this.totalRedeemPoint = totalRedeemPoint;
    }

    public String getUserRedeemPointValue() {
        return userRedeemPointValue;
    }

    public void setUserRedeemPointValue(String userRedeemPointValue) {
        this.userRedeemPointValue = userRedeemPointValue;
    }

    public int getUserMinimumPointRequireForWithdrawal() {
        return userMinimumPointRequireForWithdrawal;
    }

    public void setUserMinimumPointRequireForWithdrawal(int userMinimumPointRequireForWithdrawal) {
        this.userMinimumPointRequireForWithdrawal = userMinimumPointRequireForWithdrawal;
    }
}