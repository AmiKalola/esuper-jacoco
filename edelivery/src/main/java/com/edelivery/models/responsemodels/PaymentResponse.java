package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.UnavailableItems;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class PaymentResponse extends IsSuccessResponse {

    @SerializedName("authorization_url")
    private String authorizationUrl;

    @SerializedName("required_param")
    private String requiredParam;

    @SerializedName("reference")
    private String reference;

    @SerializedName("options")
    private RazorPayOptionModel options;

    public RazorPayOptionModel getOptions() {
        return options;
    }

    @SerializedName("html_form")
    private String htmlForm;

    @SerializedName("wallet")
    private double wallet;

    @SerializedName("wallet_currency_code")
    private String walletCurrencyCode;

    @SerializedName("error")
    private String error;

    @SerializedName("error_message")
    private String errorMessage;

    @SerializedName("tran_ref")
    private String tranRef;

    @SerializedName("payment_intent_id")
    private String paymentIntentId;

    @SerializedName("unavailable_items")
    private List<UnavailableItems> unavailableItems;

    @SerializedName("unavailable_products")
    private List<UnavailableItems> unavailableProducts;

    public String getAuthorizationUrl() {
        return authorizationUrl;
    }

    public String getRequiredParam() {
        return requiredParam;
    }

    public String getReference() {
        return reference;
    }

    public String getHtmlForm() {
        return htmlForm;
    }

    public double getWallet() {
        return wallet;
    }

    public String getWalletCurrencyCode() {
        return walletCurrencyCode;
    }

    public String getError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public List<UnavailableItems> getUnavailableItems() {
        return unavailableItems;
    }

    public List<UnavailableItems> getUnavailableProducts() {
        return unavailableProducts;
    }

    public String getTranRef() {
        return tranRef;
    }

    public String getPaymentIntentId() {
        return paymentIntentId;
    }
}