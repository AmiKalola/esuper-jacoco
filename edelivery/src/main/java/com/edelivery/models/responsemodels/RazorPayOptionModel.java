package com.edelivery.models.responsemodels;

public class RazorPayOptionModel {

        public String key;
        public int amount;
        public String currency;
        public String name;
        public String description;
        public String image;
    @SuppressWarnings("squid:S116")
        public String order_id;
    @SuppressWarnings("squid:S116")
        public String callback_url;
    @SuppressWarnings("squid:S116")
        public String cancel_url;
        public Prefill prefill;
        public Notes notes;
        public Theme theme;

    public class Theme{
        public String color;
    }

    public class Prefill{
        public String name;
        public String email;
        public String contact;
    }
    public class Notes{
        public String address;
    }

}
