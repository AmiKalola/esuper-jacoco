package com.edelivery.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.edelivery.BuildConfig;
import com.edelivery.models.responsemodels.AppSettingDetailResponse;
import com.edelivery.parser.ApiClient;
import com.google.firebase.messaging.FirebaseMessaging;

public class PreferenceHelper {

    /**
     * Preference Const
     */
    private static final String PREF_NAME = "EDelivery";
    private static final PreferenceHelper preferenceHelper = new PreferenceHelper();
    private static final String DEVICETOKEN = "device_token";
    private static final String GOOGLEKEY = "google_key";
    private static final String ISPROFILEPICTUREREQUIRED = "is_profile_picture_required";
    private static final String ISSMSVERIFICATION = "is_sms_verification";
    private static final String ISMAILVERIFICATION = "is_mail_verification";
    private static final String ISUPLOADDOCUMENTS = "is_upload_documents";
    private static final String ISHIDEOPTIONALFIELDINREGISTER = "is_hide_optional_field_in_register";
    private static final String USERID = "user_id";
    private static final String SESSION_TOKEN = "session_token";
    private static final String FIRSTNAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String ADDRESS = "address";
    private static final String ZIPCODE = "zip_code";
    private static final String PROFILE_PIC = "profile_pic";
    private static final String PHONE_NUMBER = "phone_number";
    private static final String PHONE_COUNTRY_CODE = "phone_country_code";
    private static final String EMAIL = "email";
    private static final String COUNTRY = "country";
    private static final String IS_PUSH_SOUND_ON = "is_push_sound_on";
    private static final String IS_APPROVED = "is_approved";
    private static final String IS_LOGIN_BY_EMAIL = "is_login_by_email";
    private static final String IS_LOGIN_BY_PHONE = "is_login_by_phone";
    private static final String REFERRAL = "referral";
    private static final String IS_ADMIN_DOCUMENT_MANDATORY = "is_admin_document_mandatory";
    private static final String IS_MAIL_VERIFIED = "is_mail_verified";
    private static final String IS_PHONE_NUMBER_VERIFIED = "is_phone_number_verified";
    private static final String ADMIN_EMAIL = "admin_email";
    private static final String MINIMUM_PHONE_NUMBER_LENGTH = "minimum_phone_number_length";
    private static final String MAXIMUM_PHONE_NUMBER_LENGTH = "maximum_phone_number_length";
    private static final String LANGUAGE_CODE = "language_code";
    private static final String LANGUAGE_INDEX = "language_index";
    private static final String IS_REFERRAL_ON = "is_referral_on";
    private static final String WALLET_AMOUNT = "wallet_amount";
    private static final String SOCIAL_ID = "social_id";
    private static final String IS_LOGIN_BY_SOCIAL = "is_login_by_social";
    private static final String ANDROID_ID = "android_id";
    private static final String IS_WELCOME = "is_welcome";
    private static final String ADMIN_CONTACT = "admin_contact";
    private static final String T_AND_C = "t_and_c";
    private static final String POLICY = "policy";
    private static final String IS_LOAD_STORE_IMAGE = "is_load_store_image";
    private static final String IS_LOAD_PRODUCT_IMAGE = "is_load_product_image";
    private static final String COUNTRY_CODE_ISO2 = "country_code_iso2";
    private static final String PREVIOUS_SAVE_LATITUDE = "previous_save_latitude";
    private static final String PREVIOUS_SAVE_LONGITUDE = "previous_save_longitude";
    private static final String COUNTRY_CODE = "country_code";
    private static final String WALLET_CURRENCY_CODE = "wallet_currency_code";
    private static final String THEME = "theme";
    private static final String FIREBASE_USER_TOKEN = "firebase_user_token";
    private static final String IS_ALLOW_BRING_CHANGE_OPTION = "is_allow_bring_change_option";
    private static final String IS_FROM_QR_CODE = "is_from_qr_code";
    private static final String IS_REGISTER_QR_USER = "is_register_qr_user";
    private static final String IS_ENABLE_TWILIO_CALL_MASKING = "is_enable_twilio_call_masking";
    private static final String MAX_COURIER_STOP_LIMIT = "max_courier_stop_limit";
    private static final String BASE_URL = "base_url";
    private static final String USER_PANEL_URL = "user_panel_url";
    private static final String IMAGE_URL = "image_url";
    private static final String SOCKET_URL = "socket_url";
    private static final String IS_USE_CAPTCHA = "is_use_captcha";
    private static final String ANDROID_USER_APP_GCM_KEY = "android_user_app_gcm_key";
    private static final String CAPTCHA_SITE_KEY_FOR_ANDROID = "captcha_site_key_for_android";

    private static final String USER_BASE_URL = "user_base_url";

    private static final String IS_SEND_MONEY = "is_send_money_for_user";


    private static final String ANDROID_CUSTOMER_APP_GOOGLE_MAP_KEY = "android_customer_app_google_map_key";
    private static final String ANDROID_CUSTOMER_APP_GOOGLE_PLACES_AUTOCOMPLETE_KEY = "android_customer_app_google_places_autocomplete_key";
    private static final String ANDROID_CUSTOMER_APP_GOOGLE_GEOCODING_KEY = "android_customer_app_google_geocoding_key";
    private static final String ANDROID_CUSTOMER_APP_GOOGLE_DISTANCE_MATRIX_KEY = "android_customer_app_google_distance_matrix_key";
    private static final String ANDROID_CUSTOMER_APP_GOOGLE_DIRECTION_MATRIX_KEY = "android_customer_app_google_direction_matrix_key";
    private static SharedPreferences appPreferences;


    private PreferenceHelper() {
    }

    public static PreferenceHelper getInstance(Context context) {
        appPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return preferenceHelper;
    }

    public void putCountryCode(String code) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(COUNTRY_CODE, code);
        edit.apply();
    }

    public String getCountryCode() {
        return appPreferences.getString(COUNTRY_CODE, "");
    }

    public void putWalletCurrencyCode(String currencyCode) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(WALLET_CURRENCY_CODE, currencyCode);
        edit.apply();
    }

    public String getWalletCurrencyCode() {
        return appPreferences.getString(WALLET_CURRENCY_CODE, "");
    }

    public void putDeviceToken(String deviceToken) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putString(DEVICETOKEN, deviceToken);
        editor.apply();
    }

    public String getDeviceToken() {
        return appPreferences.getString(DEVICETOKEN, null);
    }

    public void putGoogleKey(String key) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putString(GOOGLEKEY, key.trim());
        editor.apply();
    }

    public void putAll5GoogleKey(AppSettingDetailResponse model) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putString(ANDROID_CUSTOMER_APP_GOOGLE_MAP_KEY, model.getAndroidCustomerAppGoogleMapKey());
        editor.putString(ANDROID_CUSTOMER_APP_GOOGLE_PLACES_AUTOCOMPLETE_KEY, model.getAndroidCustomerAppGooglePlacesAutocompleteKey());
        editor.putString(ANDROID_CUSTOMER_APP_GOOGLE_GEOCODING_KEY, model.getAndroidCustomerAppGoogleGeocodingKey());
        editor.putString(ANDROID_CUSTOMER_APP_GOOGLE_DISTANCE_MATRIX_KEY, model.getAndroidCustomerAppGoogleDistanceMatrixKey());
        editor.putString(ANDROID_CUSTOMER_APP_GOOGLE_DIRECTION_MATRIX_KEY, model.getAndroidCustomerAppGoogleDirectionMatrixKey());
        editor.apply();
    }


    public String getAndroidCustomerAppGoogleMapKey() {
        return appPreferences.getString(ANDROID_CUSTOMER_APP_GOOGLE_MAP_KEY, "");
    }

    public String getAndroidCustomerAppGooglePlacesAutocompleteKey() {
        return appPreferences.getString(ANDROID_CUSTOMER_APP_GOOGLE_PLACES_AUTOCOMPLETE_KEY, "");
    }

    public String getAndroidCustomerAppGoogleGeocodingKey() {
        return appPreferences.getString(ANDROID_CUSTOMER_APP_GOOGLE_GEOCODING_KEY, "");
    }

    public String getAndroidCustomerAppGoogleDistanceMatrixKey() {
        return appPreferences.getString(ANDROID_CUSTOMER_APP_GOOGLE_DISTANCE_MATRIX_KEY, "");
    }

    public String getAndroidCustomerAppGoogleDirectionMatrixKey() {
        return appPreferences.getString(ANDROID_CUSTOMER_APP_GOOGLE_DIRECTION_MATRIX_KEY, "");
    }

    public String getGoogleKey() {
        return appPreferences.getString(GOOGLEKEY, null);
    }

    public void putIsProfilePictureRequired(boolean isRequired) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(ISPROFILEPICTUREREQUIRED, isRequired);
        editor.apply();
    }

    public boolean getIsProfilePictureRequired() {
        return appPreferences.getBoolean(ISPROFILEPICTUREREQUIRED, false);
    }

    public void putIsSmsVerification(boolean isRequired) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(ISSMSVERIFICATION, isRequired);
        editor.apply();
    }

    public boolean getIsSmsVerification() {
        return appPreferences.getBoolean(ISSMSVERIFICATION, false);
    }

    public void putIsMailVerification(boolean isRequired) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(ISMAILVERIFICATION, isRequired);
        editor.apply();
    }

    public boolean getIsMailVerification() {
        return appPreferences.getBoolean(ISMAILVERIFICATION, false);
    }

    public void putIsEmailVerified(boolean isVerified) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(IS_MAIL_VERIFIED, isVerified);
        editor.apply();
    }

    public boolean getIsEmailVerified() {
        return appPreferences.getBoolean(IS_MAIL_VERIFIED, false);
    }

    public void putIsPhoneNumberVerified(boolean isVerified) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(IS_PHONE_NUMBER_VERIFIED, isVerified);
        editor.apply();
    }

    public boolean getIsPhoneNumberVerified() {
        return appPreferences.getBoolean(IS_PHONE_NUMBER_VERIFIED, false);
    }

    public void putIsUserAllDocumentsUpload(boolean isRequired) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(ISUPLOADDOCUMENTS, isRequired);
        editor.apply();
    }

    public boolean getIsUserAllDocumentsUpload() {
        return appPreferences.getBoolean(ISUPLOADDOCUMENTS, false);
    }

    public void putIsShowOptionalFieldInRegister(boolean isRequired) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(ISHIDEOPTIONALFIELDINREGISTER, isRequired);
        editor.apply();
    }

    public boolean getIsShowOptionalFieldInRegister() {
        return appPreferences.getBoolean(ISHIDEOPTIONALFIELDINREGISTER, false);
    }

    public void putSessionToken(String sessionToken) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(SESSION_TOKEN, sessionToken);
        edit.apply();
    }

    public String getSessionToken() {
        return appPreferences.getString(SESSION_TOKEN, "");
    }

    public void putUserId(String userId) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(USERID, userId);
        edit.apply();
    }

    public String getUserId() {
        return appPreferences.getString(USERID, "");
    }

    public void putFirstName(String firstName) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(FIRSTNAME, firstName);
        edit.apply();
    }

    public String getFirstName() {
        return appPreferences.getString(FIRSTNAME, "");
    }

    public void putLastName(String lastName) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(LAST_NAME, lastName);
        edit.apply();
    }

    public String getLastName() {
        return appPreferences.getString(LAST_NAME, "");
    }

    public void putProfilePic(String profilePic) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(PROFILE_PIC, profilePic);
        edit.apply();
    }

    public String getProfilePic() {
        return appPreferences.getString(PROFILE_PIC, null);
    }

    public void putAddress(String address) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(PreferenceHelper.ADDRESS, address);
        edit.apply();
    }

    public String getAddress() {
        return appPreferences.getString(ADDRESS, null);
    }

    public void putZipCode(String zipCode) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(PreferenceHelper.ZIPCODE, zipCode);
        edit.apply();
    }

    public void putCountryPhoneCode(String phoneCountryCode) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(PHONE_COUNTRY_CODE, phoneCountryCode);
        edit.apply();
    }

    public String getCountryPhoneCode() {
        return appPreferences.getString(PHONE_COUNTRY_CODE, "");
    }

    public void putPhoneNumber(String phoneNumber) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(PHONE_NUMBER, phoneNumber);
        edit.apply();
    }

    public String getPhoneNumber() {
        return appPreferences.getString(PHONE_NUMBER, "");
    }

    public void putEmail(String userEmail) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(EMAIL, userEmail);
        edit.apply();
    }

    public String getEmail() {
        return appPreferences.getString(EMAIL, "");
    }

    public void putReferral(String referral) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(REFERRAL, referral);
        edit.apply();
    }

    public String getReferral() {
        return appPreferences.getString(REFERRAL, null);
    }

    public void putIsPushNotificationSoundOn(boolean isSoundOn) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putBoolean(IS_PUSH_SOUND_ON, isSoundOn);
        edit.apply();
    }

    public boolean getIsPushNotificationSoundOn() {
        return appPreferences.getBoolean(IS_PUSH_SOUND_ON, true);
    }

    public void putIsReferralOn(boolean isReferralOn) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putBoolean(IS_REFERRAL_ON, isReferralOn);
        edit.apply();
    }

    public boolean getIsReferralOn() {
        return appPreferences.getBoolean(IS_REFERRAL_ON, false);
    }

    public void putIsApproved(boolean isApproved) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putBoolean(IS_APPROVED, isApproved);
        edit.apply();
    }

    public boolean getIsApproved() {
        return appPreferences.getBoolean(IS_APPROVED, false);
    }

    public void putIsLoginByEmail(boolean isLoginEmail) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(IS_LOGIN_BY_EMAIL, isLoginEmail);
        editor.apply();
    }

    public boolean getIsLoginByEmail() {
        return appPreferences.getBoolean(IS_LOGIN_BY_EMAIL, false);
    }

    public void putIsLoginByPhone(boolean isLoginPhone) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(IS_LOGIN_BY_PHONE, isLoginPhone);
        editor.apply();
    }

    public boolean getIsLoginByPhone() {
        return appPreferences.getBoolean(IS_LOGIN_BY_PHONE, false);
    }

    public void putIsAdminDocumentMandatory(boolean isUpload) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(IS_ADMIN_DOCUMENT_MANDATORY, isUpload);
        editor.apply();
    }

    public boolean getIsAdminDocumentMandatory() {
        return appPreferences.getBoolean(IS_ADMIN_DOCUMENT_MANDATORY, false);
    }

    public void putAdminContactEmail(String email) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(ADMIN_EMAIL, email);
        edit.apply();
    }

    public String getAdminContactEmail() {
        return appPreferences.getString(ADMIN_EMAIL, null);
    }

    public void putMinimumPhoneNumberLength(int length) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putInt(MINIMUM_PHONE_NUMBER_LENGTH, length);
        editor.apply();
    }

    public int getMinimumPhoneNumberLength() {
        return appPreferences.getInt(MINIMUM_PHONE_NUMBER_LENGTH, Const.PhoneNumber.MINIMUM_PHONE_NUMBER_LENGTH);
    }

    public void putMaximumPhoneNumberLength(int length) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putInt(MAXIMUM_PHONE_NUMBER_LENGTH, length);
        editor.apply();
    }

    public int getMaximumPhoneNumberLength() {
        return appPreferences.getInt(MAXIMUM_PHONE_NUMBER_LENGTH, Const.PhoneNumber.MAXIMUM_PHONE_NUMBER_LENGTH);
    }

    public void putLanguageCode(String code) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(LANGUAGE_CODE, code);
        edit.apply();
        ApiClient.setLanguageCode(code);
    }

    public String getLanguageCode() {
        return appPreferences.getString(LANGUAGE_CODE, "en");
    }

    public void putLanguageIndex(int index) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putInt(LANGUAGE_INDEX, index);
        edit.apply();
        ApiClient.setLanguage(index);
    }

    public int getLanguageIndex() {
        return appPreferences.getInt(LANGUAGE_INDEX, 0);
    }

    public void putCountryId(String id) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(COUNTRY, id);
        edit.apply();
    }

    public String getCountryId() {
        return appPreferences.getString(COUNTRY, "en");
    }

    public void putWalletAmount(float amount) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putFloat(WALLET_AMOUNT, amount);
        edit.apply();
    }

    public float getWalletAmount() {
        return appPreferences.getFloat(WALLET_AMOUNT, 0);
    }

    public void putSocialId(String id) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(SOCIAL_ID, id);
        edit.apply();
    }

    public String getSocialId() {
        return appPreferences.getString(SOCIAL_ID, null);
    }

    public void putIsLoginBySocial(boolean isLogin) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(IS_LOGIN_BY_SOCIAL, isLogin);
        editor.apply();
    }

    public boolean getIsLoginBySocial() {
        return appPreferences.getBoolean(IS_LOGIN_BY_SOCIAL, false);
    }

    public void putAndroidId(String id) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(ANDROID_ID, id);
        edit.apply();
    }

    public String getAndroidId() {
        return appPreferences.getString(ANDROID_ID, "");
    }

    public void putIsHideWelcomeScreen(boolean isHide) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(IS_WELCOME, isHide);
        editor.apply();
    }

    public boolean getIsHideWelcomeScreen() {
        return appPreferences.getBoolean(IS_WELCOME, false);
    }

    public void putAdminContact(String contact) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(ADMIN_CONTACT, contact);
        edit.apply();
    }

    public String getAdminContact() {
        return appPreferences.getString(ADMIN_CONTACT, null);
    }

    public void putTermsANdConditions(String tandc) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(T_AND_C, tandc);
        edit.apply();
    }

    public String getTermsANdConditions() {
        return appPreferences.getString(T_AND_C, null);
    }

    public void putPolicy(String policy) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(POLICY, policy);
        edit.apply();
    }

    public String getPolicy() {
        return appPreferences.getString(POLICY, null);
    }

    public void putCountryCodeISO2(String code) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(COUNTRY_CODE_ISO2, code);
        edit.apply();
    }

    public String getCountryCodeISO2() {
        return appPreferences.getString(COUNTRY_CODE_ISO2, "US");
    }

    public void putIsLoadStoreImage(boolean isLoad) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(IS_LOAD_STORE_IMAGE, isLoad);
        editor.apply();
    }

    public boolean getIsLoadStoreImage() {
        return appPreferences.getBoolean(IS_LOAD_STORE_IMAGE, true);
    }

    public void putIsLoadProductImage(boolean isLoad) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(IS_LOAD_PRODUCT_IMAGE, isLoad);
        editor.apply();
    }

    public boolean getIsLoadProductImage() {
        return appPreferences.getBoolean(IS_LOAD_PRODUCT_IMAGE, true);
    }

    public void clearVerification() {
        putIsEmailVerified(false);
        putIsPhoneNumberVerified(false);
    }

    public void logout() {
        if (preferenceHelper.getUserId() != null && !preferenceHelper.getUserId().equals("")) {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(preferenceHelper.getUserId());
        }
        putSessionToken(null);
        putUserId("");
        ApiClient.setLoginDetail("", "");
        putFirebaseUserToken("");
    }

    public void putPreviousSaveLatitude(String latitude) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(PREVIOUS_SAVE_LATITUDE, latitude);
        edit.apply();
    }

    public String getPreviousSaveLatitude() {
        return appPreferences.getString(PREVIOUS_SAVE_LATITUDE, null);
    }

    public void putPreviousSaveLongitude(String longitude) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(PREVIOUS_SAVE_LONGITUDE, longitude);
        edit.apply();
    }

    public String getPreviousSaveLongitude() {
        return appPreferences.getString(PREVIOUS_SAVE_LONGITUDE, null);
    }

    public void putTheme(int theme) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putInt(THEME, theme);
        editor.apply();
    }

    public int getTheme() {
        return appPreferences.getInt(THEME, AppColor.DEVICE_DEFAULT);
    }

    public void putFirebaseUserToken(String firebaseUserToken) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(FIREBASE_USER_TOKEN, firebaseUserToken);
        edit.apply();
    }

    public String getFirebaseUserToken() {
        return appPreferences.getString(FIREBASE_USER_TOKEN, null);
    }

    public void putBaseUrl(String baseUrl) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(BASE_URL, baseUrl);
        edit.apply();
    }

    public String getBaseUrl() {
        return appPreferences.getString(BASE_URL, BuildConfig.BASE_URL);
    }

    public void putSocketUrl(String socketUrl) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(SOCKET_URL, socketUrl);
        edit.apply();
    }

    public String getSocketUrl() {
        return appPreferences.getString(SOCKET_URL, BuildConfig.SOCKET_URL);
    }

    public void putUserPanelUrl(String userPanelUrl) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(USER_PANEL_URL, userPanelUrl);
        edit.apply();
    }

    public String getUserPanelUrl() {
        return appPreferences.getString(USER_PANEL_URL, BuildConfig.USER_PANEL_URL);
    }

    public void putImageUrl(String imageUrl) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(IMAGE_URL, imageUrl);
        edit.apply();
    }

    public String getImageUrl() {
        return appPreferences.getString(IMAGE_URL, BuildConfig.IMAGE_URL);
    }

    public void putIsAllowBringChangeOption(Boolean isAllowBringChangeOption) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putBoolean(IS_ALLOW_BRING_CHANGE_OPTION, isAllowBringChangeOption);
        edit.apply();
    }

    public Boolean getIsAllowBringChangeOption() {
        return appPreferences.getBoolean(IS_ALLOW_BRING_CHANGE_OPTION, false);
    }

    public void putIsFromQRCode(Boolean isFromQRCode) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putBoolean(IS_FROM_QR_CODE, isFromQRCode);
        edit.apply();
    }

    public Boolean getIsFromQRCode() {
        return appPreferences.getBoolean(IS_FROM_QR_CODE, false);
    }

    public void putIsRegisterQRUser(Boolean isRegisterQRUser) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putBoolean(IS_REGISTER_QR_USER, isRegisterQRUser);
        edit.apply();
    }

    public Boolean getIsRegisterQRUser() {
        return appPreferences.getBoolean(IS_REGISTER_QR_USER, false);
    }

    public void putIsEnableTwilioCallMasking(Boolean isEnableTwilioCallMasking) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putBoolean(IS_ENABLE_TWILIO_CALL_MASKING, isEnableTwilioCallMasking);
        edit.apply();
    }

    public Boolean getIsEnableTwilioCallMasking() {
        return appPreferences.getBoolean(IS_ENABLE_TWILIO_CALL_MASKING, false);
    }

    public void putMaxCourierStopLimit(int maxCourierStopLimit) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putInt(MAX_COURIER_STOP_LIMIT, maxCourierStopLimit);
        edit.apply();
    }

    public int getMaxCourierStopLimit() {
        return appPreferences.getInt(MAX_COURIER_STOP_LIMIT, 0);
    }

    public void putIsSendMoney(boolean isSendMoney) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(IS_SEND_MONEY, isSendMoney);
        editor.apply();
    }

    public boolean isSendMoney() {
        return appPreferences.getBoolean(IS_SEND_MONEY, true);
    }

    public void putIsUseCaptcha(boolean isUseCaptcha) {
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean(IS_USE_CAPTCHA, isUseCaptcha);
        editor.apply();
    }

    public boolean isUseCaptcha() {
        return appPreferences.getBoolean(IS_USE_CAPTCHA, false);
    }

    public void putCaptchaAndroidKey(String captchaSiteKeyForAndroid) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(CAPTCHA_SITE_KEY_FOR_ANDROID, captchaSiteKeyForAndroid);
        edit.apply();
    }

    public String getCaptchaAndroidKey() {
        return appPreferences.getString(CAPTCHA_SITE_KEY_FOR_ANDROID, "");
    }

    public String getUserBaseUrl() {
        return appPreferences.getString(USER_BASE_URL, "");
    }

    public void putUserBaseUrl(String userBaseUrl) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(USER_BASE_URL, userBaseUrl);
        edit.apply();
    }

    public void putAndroidUserAppGcmKey(String androidUserAppGcmKey) {
        SharedPreferences.Editor edit = appPreferences.edit();
        edit.putString(ANDROID_USER_APP_GCM_KEY, androidUserAppGcmKey);
        edit.apply();
    }

    public String getAndroidUserAppGcmKey() {
        return appPreferences.getString(ANDROID_USER_APP_GCM_KEY, "");
    }

}