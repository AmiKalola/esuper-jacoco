package com.edelivery.utils;

import com.edelivery.BuildConfig;

public class AppLog {
    private AppLog() {
        //nothing here
    }
    public static final boolean ISDEBUG = BuildConfig.DEBUG;

    public static void log(String tag, String message) {
        if (ISDEBUG) {
            android.util.Log.d(tag, message);
        }
    }

    public static void handleException(String tag, Exception e) {
        if (ISDEBUG && e != null) {

                android.util.Log.d(tag, e + "");

        }
    }

    public static void handleThrowable(String tag, Throwable t) {
        if (ISDEBUG && t != null) {

                android.util.Log.d(tag, t + "");

        }
    }
}