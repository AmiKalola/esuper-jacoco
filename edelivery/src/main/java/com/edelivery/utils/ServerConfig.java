package com.edelivery.utils;

import android.content.Context;

import com.edelivery.BuildConfig;
import com.edelivery.parser.ApiClient;

public class ServerConfig {
    private ServerConfig() {
        //nothing here
    }

    public static String baseUrl = BuildConfig.BASE_URL;
    public static String userPanelUrl = BuildConfig.USER_PANEL_URL;
    public static String imageUrl = BuildConfig.IMAGE_URL;
    public static String socketUrl = BuildConfig.SOCKET_URL;

    public static void setURL(Context context) {
        baseUrl = PreferenceHelper.getInstance(context).getBaseUrl();
        userPanelUrl = PreferenceHelper.getInstance(context).getUserPanelUrl();
        imageUrl = PreferenceHelper.getInstance(context).getImageUrl();
        socketUrl = PreferenceHelper.getInstance(context).getSocketUrl();
        new ApiClient().changeAllApiBaseUrl(baseUrl);
    }
}