package com.edelivery.utils;

import android.content.Context;
import android.graphics.Typeface;

import androidx.annotation.Nullable;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public final class FontCache {
    @NotNull
    public static final FontCache INSTANCE;
    private static final HashMap<String, Typeface> font_Cache;

    static {
        INSTANCE = new FontCache();
        font_Cache = new HashMap<>();
    }

    private FontCache() {
    }

    @Nullable
    public Typeface get(@NotNull String name, @NotNull Context context) {
        Typeface tf = font_Cache.get(name);
        if (tf == null) {
            try {
                tf = Typeface.createFromAsset(context.getAssets(), name);
            } catch (Exception var5) {
                return null;
            }
            font_Cache.put(name, tf);
        }
        return tf;
    }
}
