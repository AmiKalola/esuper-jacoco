package com.edelivery.utils;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.edelivery.BuildConfig;
import com.paypal.checkout.PayPalCheckout;
import com.paypal.checkout.approve.ApprovalData;
import com.paypal.checkout.config.CheckoutConfig;
import com.paypal.checkout.config.Environment;
import com.paypal.checkout.config.SettingsConfig;
import com.paypal.checkout.createorder.CreateOrder;
import com.paypal.checkout.createorder.CurrencyCode;
import com.paypal.checkout.createorder.OrderIntent;
import com.paypal.checkout.createorder.UserAction;
import com.paypal.checkout.order.Amount;
import com.paypal.checkout.order.AppContext;
import com.paypal.checkout.order.CaptureOrderResult;
import com.paypal.checkout.order.OrderRequest;
import com.paypal.checkout.order.PurchaseUnit;

import java.util.ArrayList;
import java.util.List;

public class PayPalHelper {
    public static final String TAG = PayPalHelper.class.getSimpleName();

    private final Application application;
    private final Context context;
    private final CurrencyCode currencyCode;
    private OnPaymentListener onPaymentListener;

    private PayPalHelper(Application application, Context context, CurrencyCode currencyCode) {
        this.application = application;
        this.context = context;
        this.currencyCode = currencyCode;
    }

    public static PayPalHelper getInstance(Application application, Context context, CurrencyCode currencyCode) {
        return new PayPalHelper(application, context, currencyCode);
    }

    private static void onCaptureComplete(CaptureOrderResult captureOrderResult) {
        //nothing here
    }

    /**
     * set paypal configuration
     */
    public void setPayPalConfiguration(String clientId, String environment) {
        AppLog.log(TAG, "getPayPalClientId --> " + clientId);
        AppLog.log(TAG, "getPaypalEnvironment --> " + environment);
        AppLog.log(TAG, "currencyCode --> " + currencyCode);
        if (!TextUtils.isEmpty(environment) && !TextUtils.isEmpty(clientId)) {
            CheckoutConfig config = new CheckoutConfig(
                    application,
                    clientId,
                    environment.equalsIgnoreCase("live") ? Environment.LIVE : Environment.SANDBOX,
                    currencyCode,
                    UserAction.PAY_NOW,
                    null,
                    new SettingsConfig(false, false),
                    BuildConfig.APPLICATION_ID + "://paypalpay"
            );
            PayPalCheckout.setConfig(config);

            PayPalCheckout.registerCallbacks(approval -> {
                AppLog.log(TAG, "getData --> " + approval.getData());
                if (approval.getData().getOrderId() != null && onPaymentListener != null) {

                        onPaymentListener.onSuccess(approval.getData());

                }
                approval.getOrderActions().capture(PayPalHelper::onCaptureComplete);
            }, () -> {
                if (onPaymentListener != null) {
                    onPaymentListener.onCancel();
                }
            }, errorInfo -> {
                AppLog.log(TAG, "errorInfo --> " + errorInfo);
                Utils.showToast(errorInfo.toString(), context);
            });
        }
    }

    /**
     * start paypal payment flow
     */
    public void startPayPalPaymentFlow(String amount) {
        if (PayPalCheckout.INSTANCE.isConfigSet() && amount != null) {
            List<PurchaseUnit> purchaseUnits = new ArrayList<>();
            PurchaseUnit purchaseUnit = new PurchaseUnit.Builder()
                    .amount(
                            new Amount.Builder()
                                    .currencyCode(currencyCode)
                                    .value(amount)
                                    .build()
                    ).build();
            purchaseUnits.add(purchaseUnit);
            OrderRequest orderRequest = new OrderRequest.Builder()
                    .appContext(
                            new AppContext.Builder()
                                    .userAction(UserAction.PAY_NOW)
                                    .build()
                    )
                    .intent(OrderIntent.CAPTURE)
                    .purchaseUnitList(purchaseUnits)
                    .build();

            CreateOrder createOrder = createOrderActions -> createOrderActions.create(orderRequest, s -> {});


            PayPalCheckout.startCheckout(createOrder);
        }
    }

    /**
     * set payment listener
     *
     * @param onPaymentListener onPaymentListener
     */
    public void setPaymentSuccessListener(OnPaymentListener onPaymentListener) {
        this.onPaymentListener = onPaymentListener;
    }

    public interface OnPaymentListener {
        void onSuccess(ApprovalData approvalData);

        void onCancel();
    }
}
