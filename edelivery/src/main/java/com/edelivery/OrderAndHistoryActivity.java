package com.edelivery;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Filter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.edelivery.adapter.ViewPagerAdapter;
import com.edelivery.fragments.OrderFragment;
import com.edelivery.fragments.OrderHistoryFragment;
import com.edelivery.models.datamodels.Order;
import com.edelivery.utils.AppColor;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.google.android.material.tabs.TabLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class OrderAndHistoryActivity extends AbstractBaseAppCompatActivity {

    private TabLayout notificationTabsLayout;
    private ViewPager viewPager;
    private OrderFragment orderFragment;
    private OrderHistoryFragment orderHistoryFragment;
    private boolean isFromCompleteOrder = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_orders));
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_filter);
        Objects.requireNonNull(drawable).setTint(AppColor.colorTheme);
        toolbar.setOverflowIcon(drawable);
        if (getIntent() != null) {
            isFromCompleteOrder = getIntent().getBooleanExtra(Const.IS_FROM_COMPLETE_ORDER, false);
        }
        initViewById();
        setViewListener();
        initTabLayout();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void initViewById() {
        notificationTabsLayout = findViewById(R.id.notificationTabsLayout);
        viewPager = findViewById(R.id.notificationViewpager);
        toolbar.setElevation(getResources().getDimension(R.dimen.dimen_app_tab_elevation));
    }

    @Override
    protected void setViewListener() {
        //nothing here
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
//nothing here
    }

    private void initTabLayout() {
        orderFragment = new OrderFragment();
        orderHistoryFragment = new OrderHistoryFragment();
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(orderFragment, getResources().getString(R.string.text_current_orders));
        viewPagerAdapter.addFragment(orderHistoryFragment, getResources().getString(R.string.text_order_history));
        viewPager.setAdapter(viewPagerAdapter);
        notificationTabsLayout.setupWithViewPager(viewPager);
        notificationTabsLayout.setSelectedTabIndicatorColor(AppColor.colorTheme);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!isFromCompleteOrder)
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        else {
            goToHomeActivity();
        }
    }

    public void goToOrderDetailActivity(Order order, String orderId, boolean isShowHistory) {
        Intent intent = new Intent(this, OrderDetailActivity.class);
        intent.putExtra(Const.Params.ORDER, order);
        intent.putExtra(Const.Params.IS_SHOW_HISTORY, isShowHistory);
        intent.putExtra(Const.Params.ORDER_ID, orderId);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public int compareTwoDate(String firstStrDate, String secondStrDate) {
        try {
            SimpleDateFormat dateFormat = parseContent.webFormat;
            Date date2 = dateFormat.parse(secondStrDate);
            Date date1 = dateFormat.parse(firstStrDate);
            return date2.compareTo(date1);
        } catch (ParseException e) {
            AppLog.handleException(OrdersActivity.class.getName(), e);
        }
        return 0;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_filter_order, menu);
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
            spanString.setSpan(new ForegroundColorSpan(Color.BLACK), 0, spanString.length(), 0);
            item.setTitle(spanString);
        }
        return super.onCreateOptionsMenu(menu);
    }

    private final Filter.FilterListener filterListener = count -> {
        orderFragment.updateUiWhenDataAvailable();
        orderHistoryFragment.updateUiWhenDataAvailable();
    };

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.item_all) {
            orderFragment.ordersAdapter.getFilter().filter("", filterListener);
            orderHistoryFragment.ordersHistoryAdapter.getFilter().filter("", filterListener);
        } else if (id == R.id.item_courier) {
            orderFragment.ordersAdapter.getFilter().filter(String.valueOf(Const.DeliveryType.COURIER), filterListener);
            orderHistoryFragment.ordersHistoryAdapter.getFilter().filter(String.valueOf(Const.DeliveryType.COURIER), filterListener);
        } else if (id == R.id.item_store) {
            orderFragment.ordersAdapter.getFilter().filter(String.valueOf(Const.DeliveryType.STORE), filterListener);
            orderHistoryFragment.ordersHistoryAdapter.getFilter().filter(String.valueOf(Const.DeliveryType.STORE), filterListener);
        } else if (id == R.id.item_table_reservation) {
            orderFragment.ordersAdapter.getFilter().filter(String.valueOf(Const.DeliveryType.TABLE_BOOKING), filterListener);
            orderHistoryFragment.ordersHistoryAdapter.getFilter().filter(String.valueOf(Const.DeliveryType.TABLE_BOOKING), filterListener);
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isFromCompleteOrder() {
        return isFromCompleteOrder;
    }
}