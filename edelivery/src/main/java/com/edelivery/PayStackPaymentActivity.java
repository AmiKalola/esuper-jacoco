package com.edelivery;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.edelivery.utils.Const;
import com.edelivery.utils.ServerConfig;

public class PayStackPaymentActivity extends AbstractBaseAppCompatActivity {
    private static final String TAG = PayStackPaymentActivity.class.getSimpleName();

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paystack_payment);
        initToolBar();
        String payUHtml = getIntent().getStringExtra(Const.Params.PAYU_HTML);
        WebView webViewTerms = findViewById(R.id.webview);
        webViewTerms.getSettings().setLoadsImagesAutomatically(true);
        webViewTerms.getSettings().setDisplayZoomControls(false);
        webViewTerms.getSettings().setBuiltInZoomControls(true);
        webViewTerms.getSettings().setJavaScriptEnabled(true);
        webViewTerms.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webViewTerms.getSettings().setDomStorageEnabled(true);
        if (TextUtils.isEmpty(payUHtml)) {
            setTitleOnToolBar(getResources().getString(R.string.text_add_card));
            webViewTerms.loadUrl(getIntent().getStringExtra(Const.Params.AUTHORIZATION_URL));
        } else {
            setTitleOnToolBar(getResources().getString(R.string.text_payments));
            webViewTerms.loadDataWithBaseURL(null, payUHtml, "text/html", "utf-8", null);
        }
        webViewTerms.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e(TAG, "URL --> " + url);
                if (url.contains("add_card_success")) {

                    setResultOk();
                } else if (url.contains(ServerConfig.baseUrl + "payments")) {

                    setResultOk();
                } else if (url.contains(ServerConfig.baseUrl + "payment_fail")) {
                    setResult(RESULT_CANCELED);
                    finish();
                } else if (url.contains(ServerConfig.baseUrl + "fail_stripe_intent_payment")) {
                    setResultCancel();
                } else if (url.contains("add_card_failed")) {
                    setResultCancel();
                }
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
    }

    private void setResultOk() {
        setResult(RESULT_OK);
        finish();
    }

    private void setResultCancel() {
        setResult(RESULT_CANCELED);
        finish();
    }
    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void initViewById() {
        //nothing here
    }

    @Override
    protected void setViewListener() {
        //nothing here
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        //nothing here
    }
}