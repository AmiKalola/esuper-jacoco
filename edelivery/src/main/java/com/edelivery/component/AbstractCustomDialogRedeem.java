package com.edelivery.component;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.edelivery.R;
import com.edelivery.parser.ParseContent;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;

public abstract class AbstractCustomDialogRedeem extends BottomSheetDialog implements View.OnClickListener, TextView.OnEditorActionListener {

    private final CustomFontEditTextView etDialogEditTextTwo;
    private final TextView tvDialogEditTextTitle;
    private final Button btnDialogEditTextRight;
    private final ImageView btnDialogEditTextLeft;
    private final TextInputLayout dialogItlTwo;
    private final CustomFontTextView tvRedeemWarning;
    private int totalRedeemPoint = 0;
    private final String driverRedeemPointValue;
    private final int driverMinimumPointRequireForWithdrawal;
    private final String countryCode;
    private final Context context;

    protected AbstractCustomDialogRedeem(Context context, String titleDialog, String titleRightButton,
                                         String editTextTwoHint,
                                         int editTextTwoInputType, String countryCode, int totalRedeemPoint, String driverRedeemPointValue, int driverMinimumPointRequireForWithdrawal) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom_redeem);
        tvDialogEditTextTitle = findViewById(R.id.tvDialogAlertTitle);
        btnDialogEditTextLeft = findViewById(R.id.btnDialogAlertLeft);
        btnDialogEditTextRight = findViewById(R.id.btnDialogAlertRight);
        etDialogEditTextTwo = findViewById(R.id.etDialogEditTextTwo);
        dialogItlTwo = findViewById(R.id.dialogItlTwo);
        tvRedeemWarning = findViewById(R.id.tvRedeemWarning);
        btnDialogEditTextLeft.setOnClickListener(this);
        btnDialogEditTextRight.setOnClickListener(this);

        tvDialogEditTextTitle.setText(titleDialog);
        btnDialogEditTextRight.setText(titleRightButton);
        etDialogEditTextTwo.setInputType(editTextTwoInputType);
        etDialogEditTextTwo.setOnEditorActionListener(this);
        dialogItlTwo.setHint(editTextTwoHint);
        if (editTextTwoInputType == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
            dialogItlTwo.setEndIconMode(TextInputLayout.END_ICON_PASSWORD_TOGGLE);
        }
        this.countryCode = countryCode;
        this.totalRedeemPoint = totalRedeemPoint;
        this.driverRedeemPointValue = driverRedeemPointValue;
        this.driverMinimumPointRequireForWithdrawal = driverMinimumPointRequireForWithdrawal;
        this.context = context;

        etDialogEditTextTwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//nothing here
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//nothing here
            }

            @Override
            public void afterTextChanged(Editable editable) {
                isValidate(false);

            }
        });


        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setCancelable(false);
        getBehavior().setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnDialogAlertLeft) {
            onClickLeftButton();
        } else if (id == R.id.btnDialogAlertRight && isValidate(true)) {

                onClickRightButton(etDialogEditTextTwo);
        }
    }

    private boolean isValidate(boolean fromSubmit) {
        boolean isValid;

        if (etDialogEditTextTwo.getText().toString().length() >= 1) {
            tvRedeemWarning.setVisibility(View.VISIBLE);
            if (totalRedeemPoint < Integer.parseInt(etDialogEditTextTwo.getText().toString())) {
                tvRedeemWarning.setText(context.getString(R.string.text_insufficient_redeem_points));
                tvRedeemWarning.setTextColor(context.getResources().getColor(R.color.color_red));
                isValid = false;
            } else if (Integer.parseInt(etDialogEditTextTwo.getText().toString()) < driverMinimumPointRequireForWithdrawal) {
                tvRedeemWarning.setText(context.getResources().getString(R.string.text_please_enter_at_least_points_for_redeem, driverMinimumPointRequireForWithdrawal));
                tvRedeemWarning.setTextColor(context.getResources().getColor(R.color.color_red));
                isValid = false;
            } else {
                Double total = Integer.parseInt(etDialogEditTextTwo.getText().toString()) * Double.parseDouble(driverRedeemPointValue);
                tvRedeemWarning.setText(String.format("%s %s %s %s", etDialogEditTextTwo.getText().toString(), context.getString(R.string.points_are_equal_to), ParseContent.getInstance().decimalTwoDigitFormat.format(total), countryCode));
                tvRedeemWarning.setTextColor(context.getResources().getColor(R.color.color_app_wallet_added));
                isValid = true;
            }
        } else {
            if (fromSubmit) {
                tvRedeemWarning.setText(context.getString(R.string.text_please_enter_valid_redeem_point));
                tvRedeemWarning.setVisibility(View.VISIBLE);
            } else {
                tvRedeemWarning.setText("--");
                tvRedeemWarning.setVisibility(View.GONE);
            }
            tvRedeemWarning.setTextColor(context.getResources().getColor(R.color.color_red));
            isValid = false;
        }
        return isValid;
    }

    public abstract void onClickLeftButton();

    public abstract void onClickRightButton(CustomFontEditTextView etDialogEditTextTwo);


    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (textView.getId() == R.id.etDialogEditTextTwo && actionId == EditorInfo.IME_ACTION_DONE && isValidate(true)) {
                onClickRightButton(etDialogEditTextTwo);
                return true;
        }
        return false;
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }
}