package com.edelivery.component;


import static com.edelivery.utils.ServerConfig.imageUrl;

import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.edelivery.PaymentActivity;
import com.edelivery.R;
import com.edelivery.models.datamodels.User;
import com.edelivery.models.responsemodels.SearchSendUserResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.FieldValidation;
import com.edelivery.utils.GlideApp;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class AbstractCustomSendMoneyDialog extends BottomSheetDialog {

    private final PaymentActivity paymentActivity;
    private final TextView tvUserName;
    private final TextView tvUserEmail;
    private final TextView tvUserPhone;
    private final TextView tvEmptyPayment;
    private final LinearLayout llUserDetails;
    private final EditText etContactNumber;
    private final EditText etMoney;
    private final TextView btnSend;
    private final ImageView ivProfileImage;
    private User userData;

    protected AbstractCustomSendMoneyDialog(PaymentActivity paymentActivity) {
        super(paymentActivity);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_send_money);
        this.paymentActivity = paymentActivity;
        tvUserName = findViewById(R.id.tvUserName);
        tvUserEmail = findViewById(R.id.tvUserEmail);
        tvUserPhone = findViewById(R.id.tvUserPhone);
        tvEmptyPayment = findViewById(R.id.tvEmptyPayment);
        llUserDetails = findViewById(R.id.llUserDetails);
        EditText etSelectCountryPhCode = findViewById(R.id.etSelectCountryPhCode);
        etContactNumber = findViewById(R.id.etContactNumber);
        etMoney = findViewById(R.id.etMoney);
        TextView btnDialogAlertRight = findViewById(R.id.btnDialogAlertRight);
        btnSend = findViewById(R.id.btnSend);
        ivProfileImage = findViewById(R.id.ivProfileImage);
        etSelectCountryPhCode.setText(paymentActivity.preferenceHelper.getCountryPhoneCode());
        findViewById(R.id.btnNegative).setOnClickListener(view -> dismiss());
        btnDialogAlertRight.setOnClickListener(v-> {
            if (TextUtils.isEmpty(etContactNumber.getText().toString().trim())) {
                etContactNumber.setError(paymentActivity.getResources().getString(R.string.please_enter_phone_no));
                etContactNumber.requestFocus();
            } else if (!FieldValidation.isValidPhoneNumber(paymentActivity, etContactNumber.getText().toString())) {
                etContactNumber.requestFocus();
                etContactNumber.setError(FieldValidation.getPhoneNumberValidationMessage(paymentActivity));
            } else {
                searchUser();
            }
        });
        btnSend.setOnClickListener(v->{
            if(Double.parseDouble(etMoney.getText().toString().trim()) <= PreferenceHelper.getInstance(paymentActivity).getWalletAmount()) {
                clickOnSendMoney(etMoney.getText().toString().trim(), userData.getId());
            } else {
                etMoney.setError(paymentActivity.getResources().getString(R.string.msg_enter_valid_amount));
                etMoney.requestFocus();
            }
        });
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    private void searchUser() {
        Utils.showCustomProgressDialog(paymentActivity,false);
        HashMap<String, Object> map = new HashMap<>();

        map.put(Const.Params.TYPE, Const.Type.USER);
        map.put(Const.Params.USER_ID, paymentActivity.preferenceHelper.getUserId());
        map.put(Const.Params.PHONE, etContactNumber.getText().toString().trim());

        Call<SearchSendUserResponse> call = ApiClient.getClient().create(ApiInterface.class).searchUserToSendMoney(map);
        call.enqueue(new Callback<SearchSendUserResponse>() {
            @Override
            public void onResponse(@NonNull Call<SearchSendUserResponse> call, @NonNull Response<SearchSendUserResponse> response) {
                if (paymentActivity.parseContent.isSuccessful(response)) {
                    if(response.body().getSuccess()) {
                        llUserDetails.setVisibility(View.VISIBLE);
                        userData = response.body().getUserDetail();
                        tvUserName.setText(String.format("%s %s", userData.getFirstName(), userData.getLastName()));
                        tvUserEmail.setText(userData.getEmail());
                        tvUserPhone.setText(String.format("%s %s", userData.getCountryPhoneCode(), userData.getPhone()));
                        GlideApp.with(paymentActivity)
                                .load(imageUrl + userData.getImageUrl()).dontAnimate()
                                .placeholder(R.drawable.placeholder)
                                .override(200, 200)
                                .into(ivProfileImage);
                        tvEmptyPayment.setVisibility(View.GONE);
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),response.body().getStatusPhrase(),paymentActivity);
                        llUserDetails.setVisibility(View.GONE);
                        tvEmptyPayment.setVisibility(View.VISIBLE);
                    }
                }

                Utils.hideCustomProgressDialog();
            }

            @Override
            public void onFailure(@NonNull Call<SearchSendUserResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(PaymentActivity.class.getSimpleName(), t);
                Utils.hideCustomProgressDialog();
            }
        });

    }

    public abstract void clickOnSendMoney(String promoCode, String id);
}