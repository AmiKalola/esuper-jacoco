package com.edelivery.component.tag;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.R;
import com.edelivery.component.tag.utils.ColorsFactory;
import com.edelivery.component.tag.utils.MeasureUtils;
import com.edelivery.utils.AppColor;

import java.util.ArrayList;
import java.util.List;


public class TagLayout extends ViewGroup {

    private final List<TagView> mTagViews = new ArrayList<>();
    private final SparseBooleanArray mCheckSparseArray = new SparseBooleanArray();
    private Paint mPaint;
    private int mBgColor;
    private int mBorderColor;
    private float mBorderWidth;
    private float mRadius;
    private int mVerticalInterval;
    private int mHorizontalInterval;
    private RectF mRect;
    private int mAvailableWidth;
    private int mTagBgColor;
    private int mTagBorderColor;
    private int mTagTextColor;
    private int mTagBgColorCheck;
    private int mTagBorderColorCheck;
    private int mTagTextColorCheck;
    private float mTagBorderWidth;
    private float mTagTextSize;
    private float mTagRadius;
    private int mTagHorizontalPadding;
    private int mTagVerticalPadding;
    private TagView.OnTagClickListener mTagClickListener;
    private TagView.OnTagLongClickListener mTagLongClickListener;
    private TagView.OnTagCheckListener mTagCheckListener;
    private TagView.OnTagCheckListener mInsideTagCheckListener;
    private int mTagShape;
    private int mFitTagNum;
    private int mIconPadding;
    private boolean mIsPressFeedback;
    private int mTagMode;
    private TagView mFitTagView;
    private TagEditView mFitTagEditView;
    private boolean mEnableRandomColor;
    private boolean mIsHorizontalReverse;


    public TagLayout(Context context) {
        this(context, null);
    }

    public TagLayout(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public TagLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mRect = new RectF();
        mTagTextSize = MeasureUtils.sp2px(context, 13.0f);

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TagLayout);
        try {
            mTagMode = a.getInteger(R.styleable.TagLayout_tag_layout_mode, TagView.MODE_NORMAL);
            mTagShape = a.getInteger(R.styleable.TagLayout_tag_layout_shape, TagView.SHAPE_ROUND_RECT);
            mIsPressFeedback = a.getBoolean(R.styleable.TagLayout_tag_layout_press_feedback, false);
            mEnableRandomColor = a.getBoolean(R.styleable.TagLayout_tag_layout_random_color, false);
            mFitTagNum = a.getInteger(R.styleable.TagLayout_tag_layout_fit_num, TagView.INVALID_VALUE);
            mBgColor = a.getColor(R.styleable.TagLayout_tag_layout_bg_color, Color.WHITE);
            mBorderColor = a.getColor(R.styleable.TagLayout_tag_layout_border_color, Color.WHITE);
            mBorderWidth = a.getDimension(R.styleable.TagLayout_tag_layout_border_width, MeasureUtils.dp2px(context, 0.5f));
            mRadius = a.getDimension(R.styleable.TagLayout_tag_layout_border_radius, MeasureUtils.dp2px(context, 5f));
            mHorizontalInterval = (int) a.getDimension(R.styleable.TagLayout_tag_layout_horizontal_interval, MeasureUtils.dp2px(context, 5f));
            mVerticalInterval = (int) a.getDimension(R.styleable.TagLayout_tag_layout_vertical_interval, MeasureUtils.dp2px(context, 5f));

            mTagBgColor = a.getColor(R.styleable.TagLayout_tag_view_bg_color, Color.WHITE);
            mTagBorderColor = a.getColor(R.styleable.TagLayout_tag_view_border_color, Color.parseColor("#ff333333"));
            mTagTextColor = a.getColor(R.styleable.TagLayout_tag_view_text_color, Color.parseColor("#ff666666"));
            if (mIsPressFeedback || mTagMode == TagView.MODE_SINGLE_CHOICE || mTagMode == TagView.MODE_MULTI_CHOICE || mTagMode == TagView.MODE_NORMAL) {
                mTagBgColorCheck = AppColor.colorTheme;
                mTagBorderColorCheck = AppColor.colorTheme;
                mTagTextColorCheck = a.getColor(R.styleable.TagLayout_tag_view_text_color_check, Color.WHITE);
            } else {
                mTagBgColorCheck = AppColor.colorTheme;
                mTagBorderColorCheck = a.getColor(R.styleable.TagLayout_tag_view_border_color_check, mTagBorderColor);
                mTagTextColorCheck = a.getColor(R.styleable.TagLayout_tag_view_text_color_check, mTagTextColor);
            }
            mTagBorderWidth = a.getDimension(R.styleable.TagLayout_tag_view_border_width, MeasureUtils.dp2px(context, 0.5f));
            mTagTextSize = a.getDimension(R.styleable.TagLayout_tag_view_text_size, mTagTextSize);
            mTagRadius = a.getDimension(R.styleable.TagLayout_tag_view_border_radius, MeasureUtils.dp2px(context, 5f));
            mTagHorizontalPadding = (int) a.getDimension(R.styleable.TagLayout_tag_view_horizontal_padding, MeasureUtils.dp2px(context, 5f));
            mTagVerticalPadding = (int) a.getDimension(R.styleable.TagLayout_tag_view_vertical_padding, MeasureUtils.dp2px(context, 5f));
            mIconPadding = (int) a.getDimension(R.styleable.TagLayout_tag_view_icon_padding, MeasureUtils.dp2px(context, 3f));
            mIsHorizontalReverse = a.getBoolean(R.styleable.TagLayout_tag_layout_horizontal_reverse, false);
        } finally {
            a.recycle();
        }
        setWillNotDraw(false);
        setPadding(mHorizontalInterval, mVerticalInterval, mHorizontalInterval, mVerticalInterval);
        if (mTagMode == TagView.MODE_CHANGE) {
            mFitTagView = initTagView("换一换", TagView.MODE_CHANGE);
            addView(mFitTagView);
        } else if (mTagMode == TagView.MODE_EDIT) {
            initTagEditView();
            addView(mFitTagEditView);
        } else if (mTagMode == TagView.MODE_SINGLE_CHOICE || mTagMode == TagView.MODE_MULTI_CHOICE) {
            mIsPressFeedback = false;
            mInsideTagCheckListener = (position, text, isChecked) -> {
                if (mTagCheckListener != null) {
                    mTagCheckListener.onTagCheck(position, text, isChecked);
                }
                for (int i = 0; i < mTagViews.size(); i++) {
                    if (mTagViews.get(i).getText().equals(text)) {
                        mCheckSparseArray.put(i, isChecked);
                    } else if (mTagMode == TagView.MODE_SINGLE_CHOICE && mCheckSparseArray.get(i)) {
                        mTagViews.get(i).cleanTagCheckStatus();
                        mCheckSparseArray.put(i, false);
                    }
                }
            };
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSpecSize = MeasureSpec.getSize(heightMeasureSpec);
        int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
        mAvailableWidth = widthSpecSize - getPaddingLeft() - getPaddingRight();
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        int childCount = getChildCount();
        int tmpWidth = 0;
        int measureHeight = 0;
        int maxLineHeight = 0;
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            if (maxLineHeight == 0) {
                maxLineHeight = child.getMeasuredHeight();
            } else {
                maxLineHeight = Math.max(maxLineHeight, child.getMeasuredHeight());
            }
            tmpWidth += child.getMeasuredWidth() + mHorizontalInterval;
            if (tmpWidth - mHorizontalInterval > mAvailableWidth) {
                measureHeight += maxLineHeight + mVerticalInterval;
                tmpWidth = child.getMeasuredWidth() + mHorizontalInterval;
                maxLineHeight = child.getMeasuredHeight();
            }
        }
        measureHeight += maxLineHeight;

        if (childCount == 0) {
            setMeasuredDimension(0, 0);
        } else if (heightSpecMode == MeasureSpec.UNSPECIFIED || heightSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(widthSpecSize, measureHeight + getPaddingTop() + getPaddingBottom());
        } else {
            setMeasuredDimension(widthSpecSize, heightSpecSize);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int childCount = getChildCount();
        if (childCount <= 0) {
            return;
        }

        mAvailableWidth = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        int curTop = getPaddingTop();
        int curLeft;
        final int maxRight = mAvailableWidth + getPaddingLeft();
        if (mIsHorizontalReverse) {
            curLeft = maxRight;
        } else {
            curLeft = getPaddingLeft();
        }
        int maxHeight = 0;
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);

            if (maxHeight == 0) {
                maxHeight = child.getMeasuredHeight();
            } else {
                maxHeight = Math.max(maxHeight, child.getMeasuredHeight());
            }

            int width = child.getMeasuredWidth();
            int height = child.getMeasuredHeight();
            if (mIsHorizontalReverse) {
                curLeft -= width;
                if (getPaddingLeft() > curLeft) {
                    curLeft = maxRight - width;
                    curLeft = Math.max(curLeft, getPaddingLeft());
                    curTop += maxHeight + mVerticalInterval;
                    maxHeight = child.getMeasuredHeight();
                }
                child.layout(curLeft, curTop, curLeft + width, curTop + height);
                curLeft -= mHorizontalInterval;
            } else {
                if (width + curLeft > maxRight) {
                    curLeft = getPaddingLeft();
                    curTop += maxHeight + mVerticalInterval;
                    maxHeight = child.getMeasuredHeight();
                }
                child.layout(curLeft, curTop, curLeft + width, curTop + height);
                curLeft += width + mHorizontalInterval;
            }
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mRect.set(mBorderWidth, mBorderWidth, w - mBorderWidth, h - mBorderWidth);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(mBgColor);
        canvas.drawRoundRect(mRect, mRadius, mRadius, mPaint);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(mBorderWidth);
        mPaint.setColor(mBorderColor);
        canvas.drawRoundRect(mRect, mRadius, mRadius, mPaint);
    }

    public float getRadius() {
        return mRadius;
    }

    public void setRadius(float radius) {
        mRadius = radius;
    }

    public int getHorizontalInterval() {
        return mHorizontalInterval;
    }

    protected int getAvailableWidth() {
        return mAvailableWidth;
    }

    public int getFitTagNum() {
        return mFitTagNum;
    }

    private void initTagEditView() {
        mFitTagEditView = new TagEditView(getContext());
        if (mEnableRandomColor) {
            int[] color = ColorsFactory.provideColor();
            mFitTagEditView.setBorderColor(color[0]);
            mFitTagEditView.setTextColor(color[1]);
        } else {
            mFitTagEditView = new TagEditView(getContext());
            mFitTagEditView.setBorderColor(mTagBorderColor);
            mFitTagEditView.setTextColor(mTagTextColor);
        }
        mFitTagEditView.setBorderWidth(mTagBorderWidth);
        mFitTagEditView.setRadius(mTagRadius);
        mFitTagEditView.setHorizontalPadding(mTagHorizontalPadding);
        mFitTagEditView.setVerticalPadding(mTagVerticalPadding);
        mFitTagEditView.setTextSize(MeasureUtils.px2sp(getContext(), mTagTextSize));
        mFitTagEditView.updateView();
    }

    private TagView initTagView(String text, @TagView.TagMode int tagMode) {
        TagView tagView = new TagView(getContext(), text);
        if (mEnableRandomColor) {
            setTagRandomColors(tagView);
        } else {
            tagView.setBgColorLazy(mTagBgColor);
            tagView.setBorderColorLazy(mTagBorderColor);
            tagView.setTextColorLazy(mTagTextColor);
            tagView.setBgColorCheckedLazy(mTagBgColorCheck);
            tagView.setBorderColorCheckedLazy(mTagBorderColorCheck);
            tagView.setTextColorCheckedLazy(mTagTextColorCheck);
        }
        tagView.setBorderWidthLazy(mTagBorderWidth);
        tagView.setRadiusLazy(mTagRadius);
        tagView.setTextSizeLazy(mTagTextSize);
        tagView.setHorizontalPaddingLazy(mTagHorizontalPadding);
        tagView.setVerticalPaddingLazy(mTagVerticalPadding);
        tagView.setPressFeedback(mIsPressFeedback);
        tagView.setTagClickListener(mTagClickListener);
        tagView.setTagLongClickListener(mTagLongClickListener);
        tagView.setTagCheckListener(mInsideTagCheckListener);
        tagView.setTagShapeLazy(mTagShape);
        tagView.setTagModeLazy(tagMode);
        tagView.setIconPaddingLazy(mIconPadding);
        tagView.updateView();
        mTagViews.add(tagView);
        tagView.setTag(mTagViews.size() - 1);
        return tagView;
    }

    private void setTagRandomColors(TagView tagView) {
        int[] color = ColorsFactory.provideColor();
        if (mIsPressFeedback) {
            tagView.setTextColorLazy(color[1]);
            tagView.setBgColorLazy(Color.WHITE);
            tagView.setBgColorCheckedLazy(color[0]);
            tagView.setBorderColorCheckedLazy(color[0]);
            tagView.setTextColorCheckedLazy(Color.WHITE);
        } else {
            tagView.setBgColorLazy(color[1]);
            tagView.setTextColorLazy(mTagTextColor);
            tagView.setBgColorCheckedLazy(color[1]);
            tagView.setBorderColorCheckedLazy(color[0]);
            tagView.setTextColorCheckedLazy(mTagTextColor);
        }
        tagView.setBorderColorLazy(color[0]);
    }

    public void setTagCheckListener(TagView.OnTagCheckListener tagCheckListener) {
        mTagCheckListener = tagCheckListener;
    }

    public void addTag(String text) {
        if (mTagMode == TagView.MODE_CHANGE || (mTagMode == TagView.MODE_EDIT && mFitTagEditView != null)) {
            addView(initTagView(text, TagView.MODE_NORMAL), getChildCount() - 1);
        } else {
            addView(initTagView(text, mTagMode));
        }
    }

    private void refreshPositionTag(int startPos) {
        for (int i = startPos; i < mTagViews.size(); i++) {
            mTagViews.get(i).setTag(i);
        }
    }

    public void deleteTag(int position) {
        if (position < 0 || position >= getChildCount()) {
            return;
        }
        int pos = mTagMode == TagView.MODE_CHANGE ? position + 1 : position;
        if (mTagMode == TagView.MODE_CHANGE || (mTagMode == TagView.MODE_EDIT && mFitTagEditView != null)) {
            if (position == getChildCount() - 1) {
                return;
            }
            mTagViews.remove(pos);
        } else {
            mTagViews.remove(position);
        }
        removeViewAt(position);
        refreshPositionTag(position);
    }



    public void addTags(List<String> textList) {
        for (String text : textList) {
            addTag(text);
        }
    }


    public void cleanTags() {
        if (mTagMode == TagView.MODE_CHANGE || (mTagMode == TagView.MODE_EDIT && mFitTagEditView != null)) {
            removeViews(0, getChildCount() - 1);
            mTagViews.clear();
            mCheckSparseArray.clear();
            mTagViews.add(mFitTagView);
        } else {
            removeAllViews();
            mTagViews.clear();
        }
        postInvalidate();
    }

    public void setTags(List<String> textList) {
        cleanTags();
        addTags(textList);
    }

    public List<String> getCheckedTags() {
        List<String> checkTags = new ArrayList<>();
        for (int i = 0; i < mCheckSparseArray.size(); i++) {
            if (mCheckSparseArray.valueAt(i)) {
                checkTags.add(mTagViews.get(mCheckSparseArray.keyAt(i)).getText());
            }
        }
        return checkTags;
    }

    public void setCheckTag(String text) {
        if (mTagMode == TagView.MODE_SINGLE_CHOICE) {
            for (TagView tagView : mTagViews) {
                if (tagView.getText().equals(text)) {
                    tagView.setChecked(true);
                }
            }
        }
    }

    public void setCheckTag(int... indexs) {
        if (mTagMode == TagView.MODE_SINGLE_CHOICE) {
            for (int i : indexs) {
                if (mTagViews.get(i) != null) {
                    mTagViews.get(i).setChecked(true);
                }
            }
        }
    }

    public void deleteCheckedTags() {
        for (int i = mCheckSparseArray.size() - 1; i >= 0; i--) {
            if (mCheckSparseArray.valueAt(i)) {
                deleteTag(mCheckSparseArray.keyAt(i));
                mCheckSparseArray.delete(mCheckSparseArray.keyAt(i));
            }
        }
    }


}
