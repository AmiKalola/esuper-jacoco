package com.edelivery.component;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;

import com.edelivery.R;
import com.edelivery.models.responsemodels.AppSettingDetailResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.ServerConfig;
import com.edelivery.utils.Utils;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class AbstractServerDialog extends BottomSheetDialog implements View.OnClickListener {

    private final Activity activity;

    private final RadioGroup radioGroup;
    private final RadioButton rbServer1;
    private final RadioButton rbServer2;
    private final RadioButton rbServer3;
    private final RadioButton rbServer4;
    private final CustomFontEditTextView etServerUrl;
    private final CustomImageView btnClose;
    private final CustomFontButton btnOk;

    protected AbstractServerDialog(@NonNull Activity activity) {
        super(activity);
        this.activity = activity;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_server);

        btnClose = findViewById(R.id.btnClose);
        btnOk = findViewById(R.id.btnOk);
        radioGroup = findViewById(R.id.radioGroup);
        rbServer1 = findViewById(R.id.rbServer1);
        rbServer2 = findViewById(R.id.rbServer2);
        rbServer3 = findViewById(R.id.rbServer3);
        rbServer4 = findViewById(R.id.rbServer4);
        etServerUrl = findViewById(R.id.etServerUrl);

        setCancelable(false);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(params);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        etServerUrl.setVisibility(View.GONE);
        switch (ServerConfig.baseUrl) {
            case "https://apiedelivery.elluminatiinc.net/v4/":
                rbServer1.setChecked(true);
                break;
            case "https://apiedeliverydemo.appemporio.net/v4/":
                rbServer2.setChecked(true);
                break;
            case "https://apiedeliverydeveloper.elluminatiinc.net/v4/":
                rbServer3.setChecked(true);
                break;
            default:
                rbServer4.setChecked(true);
                etServerUrl.setText(ServerConfig.baseUrl);
                etServerUrl.setVisibility(View.VISIBLE);
                break;
        }

        rbServer4.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                etServerUrl.setVisibility(View.VISIBLE);
            } else {
                etServerUrl.setVisibility(View.GONE);
            }
        });

        btnClose.setOnClickListener(this);
        btnOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btnClose) {
            dismiss();
        } else if (id == R.id.btnOk) {
            int checkedRadioButtonId = radioGroup.getCheckedRadioButtonId();
            if (checkedRadioButtonId == R.id.rbServer1) {
                ServerConfig.baseUrl = "https://apiedelivery.elluminatiinc.net/v4/";
                ServerConfig.userPanelUrl = "https://webappedelivery.appemporio.net/";
                ServerConfig.imageUrl = "https://apiedelivery.elluminatiinc.net/";
            } else if (checkedRadioButtonId == R.id.rbServer2) {
                ServerConfig.baseUrl = "https://apiedeliverydemo.appemporio.net/v4/";
                ServerConfig.userPanelUrl = "https://apiedeliverydemo.appemporio.net/";
                ServerConfig.imageUrl = "https://webappedeliverydemo.appemporio.net/";
            } else if (checkedRadioButtonId == R.id.rbServer3) {
                ServerConfig.baseUrl = "https://apiedeliverydeveloper.elluminatiinc.net/v4/";
                ServerConfig.userPanelUrl = "https://webappedeliverynew.elluminatiinc.net/";
                ServerConfig.imageUrl = "https://apiedeliverydeveloper.elluminatiinc.net/";
            } else if (checkedRadioButtonId == R.id.rbServer4) {
                if (TextUtils.isEmpty(etServerUrl.getText()) || !Patterns.WEB_URL.matcher(etServerUrl.getText()).matches()) {
                    Utils.showToast(activity.getString(R.string.msg_please_enter_url), activity);
                    return;
                } else {
                    ServerConfig.baseUrl = etServerUrl.getText().toString();
                }
            }

            if (rbServer4.isChecked()) {
                goWithOtherURL();
            } else {
                goWithOk();
            }
        }
    }

    private void goWithOk() {
        if (!ServerConfig.baseUrl.equals(PreferenceHelper.getInstance(activity).getBaseUrl())) {
            onOkClicked();
        }
        dismiss();
    }

    private void goWithOtherURL() {
        Utils.showCustomProgressDialog(activity, false);
        HashMap<String, Object> map = new HashMap<>();
        map.put(Const.Params.TYPE, Const.Type.USER);
        map.put(Const.Params.DEVICE_TYPE, Const.ANDROID);

        if (!ServerConfig.baseUrl.endsWith("/")) {
            ServerConfig.baseUrl += "/";
        }

        ApiInterface apiInterface = new ApiClient().changeApiBaseUrl(ServerConfig.baseUrl).create(ApiInterface.class);
        Call<AppSettingDetailResponse> detailResponseCall = apiInterface.getAppSettingDetail(map);
        detailResponseCall.enqueue(new Callback<AppSettingDetailResponse>() {
            @Override
            public void onResponse(@NonNull Call<AppSettingDetailResponse> call, @NonNull Response<AppSettingDetailResponse> response) {
                Utils.hideCustomProgressDialog();
                if (ParseContent.getInstance().isSuccessful(response)) {
                    if (response.body() != null && response.body().isSuccess()) {
                        goWithOk();
                    }
                } else {
                    if (response.body() != null) {
                        Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), activity);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AppSettingDetailResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(Const.Tag.SPLASH_SCREEN_ACTIVITY, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    public abstract void onOkClicked();
}