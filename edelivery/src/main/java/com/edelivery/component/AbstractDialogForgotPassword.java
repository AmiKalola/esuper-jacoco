package com.edelivery.component;

import android.content.Context;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.edelivery.R;
import com.edelivery.models.datamodels.Country;
import com.edelivery.models.validations.Validator;
import com.edelivery.utils.Const;
import com.edelivery.utils.FieldValidation;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

public abstract class AbstractDialogForgotPassword extends BottomSheetDialog implements View.OnClickListener, TextView.OnEditorActionListener {

    private final Context context;
    private final PreferenceHelper preferenceHelper;

    private final TextInputLayout tilCountryPhoneCode;
    private final TextInputLayout tilLoginEmailOrPhone;
    private final CustomFontEditTextView etCountryPhoneCode;
    private final CustomFontEditTextView etLoginEmailOrPhone;
    private final CustomFontTextView tvChangeLogin;

    private int loginType;

    private final List<Country> countryList;
    private Country country;
    private AbstractCustomCountryDialog customCountryDialog;

    protected AbstractDialogForgotPassword(Context context, List<Country> countryList, Country country) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_forgot_password);
        this.context = context;
        this.countryList = countryList;
        this.country = country;
        preferenceHelper = PreferenceHelper.getInstance(context);

        tilLoginEmailOrPhone = findViewById(R.id.tilLoginEmailOrPhone);
        etLoginEmailOrPhone = findViewById(R.id.etLoginEmailOrPhone);
        tilCountryPhoneCode = findViewById(R.id.tilCountryPhoneCode);
        etCountryPhoneCode = findViewById(R.id.etCountryPhoneCode);
        tvChangeLogin = findViewById(R.id.tvChangeLogin);

        findViewById(R.id.btnClose).setOnClickListener(this);
        findViewById(R.id.btnOk).setOnClickListener(this);
        tvChangeLogin.setOnClickListener(this);
        etCountryPhoneCode.setOnClickListener(this);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setCancelable(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enabledLoginBy();
        setCountry(country);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnClose) {
            dismiss();
        } else if (id == R.id.btnOk) {
            if (isValidate()) {
                onClickOkButton(etLoginEmailOrPhone.getText().toString(), etCountryPhoneCode.getText().toString(), loginType);
            }
        } else if (id == R.id.tvChangeLogin) {
            if (TextUtils.equals(tvChangeLogin.getText().toString(), context.getString(R.string.text_use_phone_number))) {
                tvChangeLogin.setText(context.getString(R.string.text_use_email));

                loginType = Const.LoginType.PHONE_NUMBER;
                tilLoginEmailOrPhone.setHint(context.getString(R.string.text_phone));
                etLoginEmailOrPhone.setInputType(InputType.TYPE_CLASS_PHONE);
                FieldValidation.setMaxPhoneNumberInputLength(context, etLoginEmailOrPhone);
                tilCountryPhoneCode.setVisibility(View.VISIBLE);
            } else {
                tvChangeLogin.setText(context.getString(R.string.text_use_phone_number));

                loginType = Const.LoginType.EMAIL;
                tilLoginEmailOrPhone.setHint(context.getString(R.string.text_email));
                etLoginEmailOrPhone.setInputType(InputType.TYPE_CLASS_TEXT);
                etLoginEmailOrPhone.setFilters(new InputFilter[]{});
                tilCountryPhoneCode.setVisibility(View.GONE);
            }
            etLoginEmailOrPhone.setText(null);
            tilLoginEmailOrPhone.setError(null);
        } else if (id == R.id.etCountryPhoneCode) {
            openCountryCodeDialog();
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (textView.getId() == R.id.etLoginEmailOrPhone && actionId == EditorInfo.IME_ACTION_DONE) {

                if (isValidate()) {
                    onClickOkButton(etLoginEmailOrPhone.getText().toString(), etCountryPhoneCode.getText().toString(), loginType);
                }
                return true;

        }
        return false;
    }

    private boolean isValidate() {
        String msg = null;
        Validator emailValidation = FieldValidation.isEmailValid(context, etLoginEmailOrPhone.getText().toString().trim());
        if (loginType == Const.LoginType.EMAIL) {
            if (!emailValidation.isValid()) {
                msg = emailValidation.getErrorMsg();
            }
        } else if (loginType == Const.LoginType.PHONE_NUMBER) {
            if (country == null) {
                msg = context.getString(R.string.msg_please_select_country);
            } else if (!FieldValidation.isValidPhoneNumber(context, etLoginEmailOrPhone.getText().toString())) {
                msg = FieldValidation.getPhoneNumberValidationMessage(context);
            }
        }
        if (!TextUtils.isEmpty(msg)) {
            Utils.showToast(msg, context);
        }
        return TextUtils.isEmpty(msg);
    }

    private void enabledLoginBy() {
        if (preferenceHelper.getIsLoginByEmail() && preferenceHelper.getIsLoginByPhone()) {
            tilLoginEmailOrPhone.setHint(context.getString(R.string.text_email));
            tvChangeLogin.setVisibility(View.VISIBLE);
            tilCountryPhoneCode.setVisibility(View.GONE);
            loginType = Const.LoginType.EMAIL;
        } else if (preferenceHelper.getIsLoginByPhone()) {
            tilLoginEmailOrPhone.setHint(context.getString(R.string.text_phone));
            tvChangeLogin.setVisibility(View.GONE);
            etLoginEmailOrPhone.setInputType(InputType.TYPE_CLASS_PHONE);
            FieldValidation.setMaxPhoneNumberInputLength(context, etLoginEmailOrPhone);
            tilCountryPhoneCode.setVisibility(View.VISIBLE);
            loginType = Const.LoginType.PHONE_NUMBER;
        } else {
            tilLoginEmailOrPhone.setHint(context.getString(R.string.text_email));
            tvChangeLogin.setVisibility(View.GONE);
            tilCountryPhoneCode.setVisibility(View.GONE);
            loginType = Const.LoginType.EMAIL;
        }
    }

    private void openCountryCodeDialog() {
        if (countryList != null && countryList.size() > 0) {
            if (customCountryDialog != null && customCountryDialog.isShowing()) {
                return;
            }

            customCountryDialog = new AbstractCustomCountryDialog(context, countryList) {
                @Override
                public void onSelect(Country country) {
                    setCountry(country);
                    dismiss();
                }
            };

            customCountryDialog.show();
        }
    }

    private void setCountry(Country country) {
        if (country != null) {
            this.country = country;
            etCountryPhoneCode.setText(country.getCallingCode());
        }
    }

    public abstract void onClickOkButton(String emailOrPhoneNumber, String countryPhoneCode, int loginType);
}