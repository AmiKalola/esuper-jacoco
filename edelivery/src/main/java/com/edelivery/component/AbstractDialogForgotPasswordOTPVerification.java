package com.edelivery.component;

import android.content.Context;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.edelivery.R;
import com.edelivery.models.responsemodels.ForgotPasswordOTPVerificationResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppColor;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class AbstractDialogForgotPasswordOTPVerification extends BottomSheetDialog implements View.OnClickListener {

    private final CustomFontEditTextView etOtp;
    private final TextInputLayout textInputLayoutOtp;
    private final TextView tvResendIn;
    private final String emailOrPhoneNumber;
    private final String countryPhoneCode;
    private final int loginType;
    private final Context context;
    private int resendTime = Const.RESENT_CODE_SECONDS;//second
    private final TextView tvResend;
    private boolean isCountDownTimerStart = false;
    private CountDownTimer resendTimer;

    protected AbstractDialogForgotPasswordOTPVerification(Context context, String emailOrPhoneNumber, String countryPhoneCode, int loginType) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_forgot_password_otp_verification);
        this.context = context;
        this.emailOrPhoneNumber = emailOrPhoneNumber;
        this.countryPhoneCode = countryPhoneCode;
        this.loginType = loginType;

        etOtp = findViewById(R.id.etOtp);
        textInputLayoutOtp = findViewById(R.id.textInputLayoutOtp);
        textInputLayoutOtp.setHint(context.getString(R.string.text_phone_otp));
        tvResendIn = findViewById(R.id.tvResendIn);
        tvResend = findViewById(R.id.tvResend);
        tvResend.setOnClickListener(this);
        findViewById(R.id.btnDialogAlertLeft).setOnClickListener(v -> dismiss());
        findViewById(R.id.btnPositive).setOnClickListener(v -> {
            if (etOtp.getText().toString().trim().length() == 6) {
                if(PreferenceHelper.getInstance(context).isUseCaptcha()) {
                    checkSafetyNet(token -> forgotPasswordOTPVerification(etOtp.getText().toString(),token));
                } else {
                    forgotPasswordOTPVerification(etOtp.getText().toString(), "");
                }
            } else {
                Utils.showToast(context.getResources().getString(R.string.please_enter_otp), context);
            }
        });

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        setCancelable(false);
        startTimer(resendTime);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    private void stopTimer() {
        if (isCountDownTimerStart) {
            isCountDownTimerStart = false;
            resendTimer.cancel();
            resendTimer = null;
        }
    }

    private void startTimer(int seconds) {
        tvResend.setText(getContext().getString(R.string.text_resend_code_in));
        tvResend.setTextColor(AppColor.getThemeTextColor(context));
        if (!isCountDownTimerStart && seconds > 0) {
            isCountDownTimerStart = true;
            long milliSecond = 1000;
            long millisUntilFinished = seconds * milliSecond;
            resendTimer = new CountDownTimer(millisUntilFinished, milliSecond) {
                @Override
                public void onTick(long millisUntilFinished) {
                    String time = String.format(Locale.US, "%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)), TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                    tvResendIn.setText(time);
                }

                @Override
                public void onFinish() {
                    stopTimer();
                    tvResendIn.setText("");
                    tvResend.setText(getContext().getString(R.string.text_resend_code));
                    tvResend.setTextColor(AppColor.colorTheme);
                }
            }.start();
        }
    }

    public abstract void optVerifySuccessfully(String id, String serverToken);

    private void forgotPassword(String emailOrPhoneNumber, String countryPhoneCode, int loginType, String reCaptchaToken) {
        HashMap<String, Object> map = new HashMap<>();
        Utils.showCustomProgressDialog(context, false);
        map.put(Const.Params.TYPE, Const.Type.USER);
        if (loginType == Const.LoginType.PHONE_NUMBER) {
            map.put(Const.Params.COUNTRY_PHONE_CODE, countryPhoneCode);
            map.put(Const.Params.PHONE, emailOrPhoneNumber);
        } else {
            map.put(Const.Params.EMAIL, emailOrPhoneNumber);
        }
        map.put(Const.Params.CAPTCHA_TOKEN, reCaptchaToken);
        map.put(Const.Params.DEVICE_TYPE, Const.ANDROID);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.forgotPassword(map);
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(@NonNull Call<IsSuccessResponse> call, @NonNull Response<IsSuccessResponse> response) {
                Utils.hideCustomProgressDialog();
                if (ParseContent.getInstance().isSuccessful(response)) {
                    if (Objects.requireNonNull(response.body()).isSuccess()) {
                        startTimer(resendTime);
                        Utils.showToast(context.getResources().getString(R.string.text_resent_code), context);
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), context);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<IsSuccessResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(Const.Tag.LOG_IN_FRAGMENT, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    private void forgotPasswordOTPVerification(String otp, String reCaptchaToken) {
        HashMap<String, Object> map = new HashMap<>();
        Utils.showCustomProgressDialog(context, false);
        map.put(Const.Params.TYPE, Const.Type.USER);
        if (loginType == Const.LoginType.PHONE_NUMBER) {
            map.put(Const.Params.COUNTRY_PHONE_CODE, countryPhoneCode);
            map.put(Const.Params.PHONE, emailOrPhoneNumber);
        } else {
            map.put(Const.Params.EMAIL, emailOrPhoneNumber);
        }
        map.put(Const.Params.OTP, otp);
        map.put(Const.Params.CAPTCHA_TOKEN, reCaptchaToken);
        map.put(Const.Params.DEVICE_TYPE, Const.ANDROID);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ForgotPasswordOTPVerificationResponse> responseCall = apiInterface.verifyForgotPasswordOTP(map);
        responseCall.enqueue(new Callback<ForgotPasswordOTPVerificationResponse>() {
            @Override
            public void onResponse(@NonNull Call<ForgotPasswordOTPVerificationResponse> call, @NonNull Response<ForgotPasswordOTPVerificationResponse> response) {
                Utils.hideCustomProgressDialog();
                if (ParseContent.getInstance().isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        dismiss();
                        ForgotPasswordOTPVerificationResponse verificationResponse = response.body();
                        optVerifySuccessfully(verificationResponse.getId(), verificationResponse.getServerToken());
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(), context);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotPasswordOTPVerificationResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(Const.Tag.LOG_IN_FRAGMENT, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    @Override
    public void onDetachedFromWindow() {
        stopTimer();
        super.onDetachedFromWindow();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvResend && !isCountDownTimerStart) {

                if (PreferenceHelper.getInstance(context).isUseCaptcha()) {
                    checkSafetyNet(token -> forgotPassword(emailOrPhoneNumber, countryPhoneCode, loginType,token));
                } else {
                    forgotPassword(emailOrPhoneNumber, countryPhoneCode, loginType, "");
                }
            }

    }

    public void checkSafetyNet(@NonNull CaptchaTokenListener captchaTokenListener) {
        SafetyNet.getClient(context).verifyWithRecaptcha(PreferenceHelper.getInstance(context).getCaptchaAndroidKey()).addOnSuccessListener(response -> {
            // Indicates communication with reCAPTCHA service was
            // successful.
            String userResponseToken = response.getTokenResult();
            if (!userResponseToken.isEmpty()) {
                // Validate the user response token using the
                // reCAPTCHA siteverify API.
                captchaTokenListener.onToken(userResponseToken);
            } else {
                Utils.showToast( context.getResources().getString(R.string.error_recaptcha),context);
            }
        }).addOnFailureListener(e -> {
            String error;
            if (e instanceof ApiException) {
                // An error occurred when communicating with the
                // reCAPTCHA service. Refer to the status code to
                // handle the error appropriately.
                ApiException apiException = (ApiException) e;
                int statusCode = apiException.getStatusCode();
                error = CommonStatusCodes.getStatusCodeString(statusCode);
            } else {
                // A different, unknown type of error occurred.
                error = e.getMessage();
            }
            AppLog.log(AbstractDialogForgotPasswordOTPVerification.class.getName(), "Error: " + error);
            Utils.showToast(context.getResources().getString(R.string.error_recaptcha) + "\n" + error,context);
        }).addOnCanceledListener(() -> Utils.showToast( context.getResources().getString(R.string.error_recaptcha),context));
    }

    public interface CaptchaTokenListener {
        void onToken(String token);
    }
}