package com.edelivery;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edelivery.adapter.UserMenuAdapter;
import com.edelivery.component.AbstractCustomDialogAlert;
import com.edelivery.component.AbstractServerDialog;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.interfaces.AbstractTripleTapListener;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.ServerConfig;

public class AccountActivity extends AbstractBaseAppCompatActivity {

    private RecyclerView rcvUserMenu;
    private CustomFontTextView tvAppVersion;
    private AbstractCustomDialogAlert logoutDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        initToolBar();
        initViewById();
        setViewListener();
        initRcvUserMenu();
        setAppVersion();
    }

    private void initRcvUserMenu() {
        UserMenuAdapter userMenuAdapter = new UserMenuAdapter(this);
        rcvUserMenu.setLayoutManager(new LinearLayoutManager(this));
        rcvUserMenu.setAdapter(userMenuAdapter);
        rcvUserMenu.addOnItemTouchListener(new RecyclerTouchListener(this, rcvUserMenu, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                switch (position) {
                    case 0:
                        goToProfileActivity();
                        break;
                    case 1:
                        goToPaymentActivity(false);
                        break;
                    case 2:
                        goToRedeemActivity();
                        break;
                    case 3:
                        goToDocumentActivity(false);
                        break;
                    case 4:
                        goToOrdersActivity();
                        break;
                    case 5:
                        goToSettingActivity();
                        break;
                    case 6:
                        goToFavouriteActivity();
                        break;
                    case 7:
                        goToFavouriteAddressActivity();
                        break;
                    case 8:
                        goToNotificationAddressActivity();
                        break;
                    case 9:
                        goToHelpActivity();
                        break;
                    case 10:
                        openLogoutDialog();
                        break;
                    case 11:
                        startNewActivity(AccountActivity.this, "com.elluminati.eber");
                        break;
                    default:
                        // do with default
                        break;
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                //nothing here
            }
        }));
    }

    private void openLogoutDialog() {
        if (logoutDialog != null && logoutDialog.isShowing()) {
            return;
        }
        logoutDialog = new AbstractCustomDialogAlert(this, this.getResources().getString(R.string.text_log_out), this.getResources().getString(R.string.msg_are_you_sure_to_logout), this.getResources().getString(R.string.text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
            }

            @Override
            public void onClickRightButton() {
                logOut(false);
                dismiss();
            }
        };
        logoutDialog.show();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setAppVersion() {
        tvAppVersion.setText(String.format("%s %s", this.getResources().getString(R.string.text_app_version), this.getAppVersion()));

        if (BuildConfig.APPLICATION_ID.equalsIgnoreCase("com.elluminatiinc.edelivery.user")) {
            tvAppVersion.setOnTouchListener(new AbstractTripleTapListener() {
                @Override
                protected void onTripleTap() {
                    showServerDialog();
                }
            });
        }
    }

    private void showServerDialog() {
        AbstractServerDialog serverDialog = new AbstractServerDialog(this) {
            @Override
            public void onOkClicked() {
                PreferenceHelper.getInstance(AccountActivity.this).putBaseUrl(ServerConfig.baseUrl);
                PreferenceHelper.getInstance(AccountActivity.this).putUserPanelUrl(ServerConfig.userPanelUrl);
                PreferenceHelper.getInstance(AccountActivity.this).putImageUrl(ServerConfig.imageUrl);
                PreferenceHelper.getInstance(AccountActivity.this).putSocketUrl(ServerConfig.socketUrl);
                logOut(true);
            }
        };
        serverDialog.show();
    }

    private void goToOrdersActivity() {

        Intent intent = new Intent(this, OrderAndHistoryActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToProfileActivity() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToSettingActivity() {
        Intent intent = new Intent(this, SettingActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToFavouriteActivity() {
        Intent intent = new Intent(this, FavouriteStoreActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToFavouriteAddressActivity() {
        Intent intent = new Intent(this, FavouriteAddressActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToNotificationAddressActivity() {
        Intent intent = new Intent(this, NotificationActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToHelpActivity() {
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToPaymentActivity(boolean isPayNowInvisible) {
        Intent homeIntent = new Intent(this, PaymentActivity.class);
        homeIntent.putExtra(Const.Tag.PAYMENT_ACTIVITY, isPayNowInvisible);
        startActivity(homeIntent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToRedeemActivity() {
        Intent homeIntent = new Intent(this, RewardPointActivity.class);
        startActivity(homeIntent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void startNewActivity(Context context, String packageName) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent == null) {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + packageName));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void initViewById() {
        rcvUserMenu = findViewById(R.id.rcvUserMenu);
        tvAppVersion = findViewById(R.id.tvAppVersion);

    }

    @Override
    protected void setViewListener() {
        setToolbarProfile(false, null);
        setTitleOnToolBar(getResources().getString(R.string.text_user));
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
      //  nothing here
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}