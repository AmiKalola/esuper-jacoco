package com.edelivery.persistentroomdata.notification;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Notification.class}, version = 1, exportSchema = false)
public abstract class AbstractNotificationDatabase extends RoomDatabase {
    private static final Object sLock = new Object();
    private static AbstractNotificationDatabase instance;

    public static AbstractNotificationDatabase getInstance(Context context) {

        synchronized (sLock) {
            if (instance == null) {
                instance = Room.databaseBuilder(context.getApplicationContext(), AbstractNotificationDatabase.class, context.getPackageName() + ".notification.db").build();
            }
            return instance;
        }
    }

    public abstract NotificationDao notificationDao();

}
