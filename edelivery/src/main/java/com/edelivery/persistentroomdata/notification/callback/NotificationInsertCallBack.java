package com.edelivery.persistentroomdata.notification.callback;

import androidx.annotation.MainThread;

import com.edelivery.persistentroomdata.notification.Notification;

public interface NotificationInsertCallBack {

    @MainThread
    void onNotificationInsert(Notification notification);

}
