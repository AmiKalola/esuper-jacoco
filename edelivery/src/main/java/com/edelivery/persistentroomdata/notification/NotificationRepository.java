package com.edelivery.persistentroomdata.notification;

import android.content.Context;

import com.edelivery.persistentroomdata.address.AppExecutors;
import com.edelivery.persistentroomdata.notification.callback.NotificationInsertCallBack;
import com.edelivery.persistentroomdata.notification.callback.NotificationLoadCallBack;

import java.util.List;

public class NotificationRepository {

    private static NotificationRepository instance;
    private final AppExecutors appExecutors;
    private final AbstractNotificationDatabase notificationDatabase;

    private NotificationRepository(Context context) {
        appExecutors = new AppExecutors();
        notificationDatabase = AbstractNotificationDatabase.getInstance(context);
    }

    public static NotificationRepository getInstance(Context context) {
        if (instance == null) {
            instance = new NotificationRepository(context);
        }
        return instance;
    }


    public void getNotification(int notificationType, NotificationLoadCallBack notificationLoadCallBack) {
        // request the addresses on the I/O thread
        appExecutors.diskIO().execute(() -> {
            final List<Notification> notifications = notificationDatabase.notificationDao().getAllNotification(notificationType);
            // notify on the main thread
            appExecutors.mainThread().execute(() -> {
                if (notificationLoadCallBack == null) {
                    return;
                }
                if (notifications == null) {
                    notificationLoadCallBack.onDataNotAvailable();
                } else {
                    notificationLoadCallBack.onNotificationLoad(notifications);
                }
            });
        });
    }

    public void insertNotification(Notification notification, NotificationInsertCallBack notificationInsertCallBack) {
        // request the addresses on the I/O thread
        appExecutors.diskIO().execute(() -> {
            notificationDatabase.notificationDao().insert(notification);
            // notify on the main thread
            appExecutors.mainThread().execute(() -> {
                if (notificationInsertCallBack == null) {
                    return;
                }
                notificationInsertCallBack.onNotificationInsert(notification);
            });
        });
    }

    public void clearNotification() {
        appExecutors.diskIO().execute(() -> notificationDatabase.notificationDao().deleteAllNotification());
    }
}
