package com.edelivery;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.LinearLayout;

import com.edelivery.component.CustomFontTextView;
import com.edelivery.utils.AppColor;
import com.edelivery.utils.Utils;

public class HelpActivity extends AbstractBaseAppCompatActivity {

    private LinearLayout llMail;
    private LinearLayout llCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_help));
        initViewById();
        setViewListener();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void initViewById() {
        llCall = findViewById(R.id.llCall);
        llMail = findViewById(R.id.llMail);
        CustomFontTextView tvTandC = findViewById(R.id.tvTandC);
        CustomFontTextView tvPolicy = findViewById(R.id.tvPolicy);
        tvTandC.setText(Utils.fromHtml("<a href=\"" + preferenceHelper.getTermsANdConditions() + "\"" + ">" + getResources().getString(R.string.text_t_and_c) + "</a>"));
        tvTandC.setMovementMethod(LinkMovementMethod.getInstance());
        tvPolicy.setText(Utils.fromHtml("<a href=\"" + preferenceHelper.getPolicy() + "\"" + ">" + getResources().getString(R.string.text_policy) + "</a>"));
        tvPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        tvTandC.setLinkTextColor(AppColor.colorTheme);
        tvPolicy.setLinkTextColor(AppColor.colorTheme);
    }

    @Override
    protected void setViewListener() {
        llCall.setOnClickListener(this);
        llMail.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.llCall) {
            Utils.openCallChooser(HelpActivity.this, preferenceHelper.getAdminContact());
        } else if (id == R.id.llMail) {
            contactUsWithAdmin();
        }
    }
}