package com.edelivery.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.edelivery.R;
import com.edelivery.RewardPointActivity;
import com.edelivery.models.datamodels.RedeemPointHistory;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class RedeemPointHistoryAdapter extends RecyclerView.Adapter<RedeemPointHistoryAdapter.RedeemPointHistoryHolder> {

    private final List<RedeemPointHistory> redeemPointHistories;
    private final RewardPointActivity rewardPointActivity;

    public RedeemPointHistoryAdapter(RewardPointActivity rewardPointActivity, List<RedeemPointHistory> redeemPointHistories) {
        this.redeemPointHistories = redeemPointHistories;
        this.rewardPointActivity = rewardPointActivity;
    }

    @NonNull
    @Override
    public RedeemPointHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_redeem_point_history, parent, false);
        return new RedeemPointHistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RedeemPointHistoryHolder holder, int position) {
        RedeemPointHistory redeemPointHistory = redeemPointHistories.get(position);
        try {
            Date date = ParseContent.getInstance().webFormat.parse(redeemPointHistory.getCreatedAt());
            if (date != null) {
                holder.tvTransactionDate.setText(ParseContent.getInstance().dateFormat3.format(date));
                holder.tvTransactionTime.setText(ParseContent.getInstance().timeFormatAm.format(date));
            }
            holder.tvWithdrawalId.setText(String.format("%s %s", rewardPointActivity.getResources().getString(R.string.text_total_redeem_points), redeemPointHistory.getTotalRedeemPoint()));
            holder.tvTransactionState.setText(redeemPointHistory.getRedeemPointDescription());
            switch (redeemPointHistory.getRedeemStatus()) {
                case Const.Redeem.ADD_REDEEM_POINT:
                    holder.tvTransactionAmount.setTextColor(ResourcesCompat.getColor(rewardPointActivity.getResources(), R.color.color_app_wallet_added, null));
                    holder.tvTransactionAmount.setText(String.format("+%s", redeemPointHistory.getAddedRedeemPoint()));
                    break;
                case Const.Redeem.REMOVE_REDEEM_POINT:
                    holder.tvTransactionAmount.setTextColor(ResourcesCompat.getColor(rewardPointActivity.getResources(), R.color.color_app_wallet_deduct, null));
                    holder.tvTransactionAmount.setText(String.format("-%s", redeemPointHistory.getAddedRedeemPoint()));
                    break;
            }
        } catch (ParseException e) {
            AppLog.handleException(RedeemPointHistoryAdapter.class.getName(), e);
        }
    }

    @Override
    public int getItemCount() {
        return redeemPointHistories.size();
    }

    protected static class RedeemPointHistoryHolder extends RecyclerView.ViewHolder {
        TextView tvTransactionState;
        TextView tvTransactionAmount;
        TextView tvTransactionDate;
        TextView tvWithdrawalId;
        TextView tvTransactionTime;

        public RedeemPointHistoryHolder(View itemView) {
            super(itemView);
            tvWithdrawalId = itemView.findViewById(R.id.tvWithdrawalID);
            tvTransactionAmount = itemView.findViewById(R.id.tvTransactionAmount);
            tvTransactionDate = itemView.findViewById(R.id.tvTransactionDate);
            tvTransactionState = itemView.findViewById(R.id.tvTransactionState);
            tvTransactionTime = itemView.findViewById(R.id.tvTransactionTime);
        }
    }
}