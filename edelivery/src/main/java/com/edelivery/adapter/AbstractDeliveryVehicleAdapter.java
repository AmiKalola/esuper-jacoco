package com.edelivery.adapter;

import static com.edelivery.utils.ServerConfig.imageUrl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.edelivery.R;
import com.edelivery.models.datamodels.Vehicle;
import com.edelivery.utils.AppColor;
import com.edelivery.utils.GlideApp;

import java.util.List;

public abstract class AbstractDeliveryVehicleAdapter extends RecyclerView.Adapter<AbstractDeliveryVehicleAdapter.DeliveryVehicleViewHolder> {
    private final Context context;
    private final List<Vehicle> vehicleList;

    private Vehicle currentVehical;

    protected AbstractDeliveryVehicleAdapter(Context context, List<Vehicle> vehicleList) {
        this.context = context;
        this.vehicleList = vehicleList;
        if (vehicleList != null && !vehicleList.isEmpty()) {
            currentVehical = vehicleList.get(0);
            vehicleList.get(0).setSelected(true);
        }
    }

    public Vehicle getCurrentVehical() {
        return currentVehical;
    }

    @NonNull
    @Override
    public DeliveryVehicleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_courier_vehicle, parent, false);
        return new DeliveryVehicleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeliveryVehicleViewHolder holder, int position) {
        Vehicle currentvehicle = vehicleList.get(position);
        holder.viewSelect.setVisibility(currentvehicle.isSelected() ? View.VISIBLE : View.GONE);
        GlideApp.with(context)
                .load(imageUrl + currentvehicle.getImageUrl())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .override(200, 200)
                .into(holder.ivVehicleImage);
        holder.tvVehicleName.setText(currentvehicle.getVehicleName());
    }

    @Override
    public int getItemCount() {
        if (vehicleList == null) {
            return 0;
        } else {
            return vehicleList.size();
        }
    }

    public abstract void onSelect(Vehicle vehicle);

    protected class DeliveryVehicleViewHolder extends RecyclerView.ViewHolder {
        TextView tvVehicleName;
        View viewSelect;
        ImageView ivVehicleImage;

        @SuppressLint("NotifyDataSetChanged")
        public DeliveryVehicleViewHolder(View itemView) {
            super(itemView);
            tvVehicleName = itemView.findViewById(R.id.tvVehicleName);
            ivVehicleImage = itemView.findViewById(R.id.ivVehicleImage);
            viewSelect = itemView.findViewById(R.id.viewSelect);
            viewSelect.setBackgroundColor(AppColor.colorTheme);
            itemView.setOnClickListener(v -> {
                for (Vehicle vehicle : vehicleList) {
                    vehicle.setSelected(false);
                }
                vehicleList.get(getAbsoluteAdapterPosition()).setSelected(true);
                currentVehical = vehicleList.get(getAbsoluteAdapterPosition());
                onSelect(currentVehical);
                notifyDataSetChanged();
            });
        }
    }
}
