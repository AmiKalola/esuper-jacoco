package com.edelivery.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.edelivery.R;
import com.edelivery.component.CustomFontCheckBox;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Product;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ProductFilterAdapter extends RecyclerView.Adapter<ProductFilterAdapter.ProductFilterViewHolder> {
    private final List<Product> storeProductList;

    public ProductFilterAdapter(List<Product> storeProductList) {
        this.storeProductList = storeProductList;
    }

    @NotNull
    @Override
    public ProductFilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ProductFilterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_filter, parent, false));
    }

    @Override
    public void onBindViewHolder(ProductFilterViewHolder holder, int position) {
        holder.tvProductNameFilter.setText(storeProductList.get(position).getProductDetail().getName());
        holder.rbSelectProductFilter.setChecked(storeProductList.get(position).isProductFiltered());
    }

    @Override
    public int getItemCount() {
        return storeProductList == null ? 0 : storeProductList.size();
    }

    protected class ProductFilterViewHolder extends RecyclerView.ViewHolder {
        private final CustomFontTextView tvProductNameFilter;
        private final CustomFontCheckBox rbSelectProductFilter;

        public ProductFilterViewHolder(View itemView) {
            super(itemView);
            tvProductNameFilter = itemView.findViewById(R.id.tvProductNameFilter);
            rbSelectProductFilter = itemView.findViewById(R.id.rbSelectProductFilter);
            rbSelectProductFilter.setOnCheckedChangeListener((buttonView, isChecked) -> storeProductList.get(getAbsoluteAdapterPosition()).setProductFiltered(isChecked));
        }
    }
}
