package com.edelivery.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.edelivery.R;
import com.edelivery.WalletHistoryActivity;
import com.edelivery.models.datamodels.WalletHistory;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.WalletHistoryHolder> {
    private final List<WalletHistory> walletHistories;
    private final WalletHistoryActivity walletHistoryFragment;

    public WalletHistoryAdapter(WalletHistoryActivity walletHistoryFragment, List<WalletHistory> walletHistories) {
        this.walletHistories = walletHistories;
        this.walletHistoryFragment = walletHistoryFragment;
    }

    @NonNull
    @Override
    public WalletHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wallet_history, parent, false);
        return new WalletHistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WalletHistoryHolder holder, int position) {
        WalletHistory walletHistory = walletHistories.get(position);
        try {
            Date date = ParseContent.getInstance().webFormat.parse(walletHistory.getCreatedAt());
            if (date != null) {
                holder.tvTransactionDate.setText(ParseContent.getInstance().dateFormat3.format(date));
                holder.tvTransactionTime.setText(ParseContent.getInstance().timeFormatAm.format(date));
            }
            holder.tvWithdrawalId.setText(String.format(Locale.getDefault(), "%s %d", walletHistoryFragment.getResources().getString(R.string.text_id), walletHistory.getUniqueId()));

            holder.tvTransactionState.setText(walletComment(walletHistory.getWalletCommentId()));
            switch (walletHistory.getWalletStatus()) {
                case Const.Wallet.ADD_WALLET_AMOUNT:
                case Const.Wallet.ORDER_REFUND_AMOUNT:
                case Const.Wallet.ORDER_PROFIT_AMOUNT:
                    holder.tvTransactionAmount.setTextColor(ResourcesCompat.getColor(walletHistoryFragment.getResources(), R.color.color_app_wallet_added, null));
                    holder.tvTransactionAmount.setText(String.format("+%s %s", ParseContent.getInstance().decimalTwoDigitFormat.format(walletHistory.getAddedWallet()), walletHistory.getToCurrencyCode()));
                    break;
                case Const.Wallet.REMOVE_WALLET_AMOUNT:
                case Const.Wallet.ORDER_CHARGE_AMOUNT:
                case Const.Wallet.ORDER_CANCELLATION_CHARGE_AMOUNT:
                case Const.Wallet.REQUEST_CHARGE_AMOUNT:
                case Const.Wallet.ORDER_PROFIT_DEDUCT_AMOUNT:
                    holder.tvTransactionAmount.setTextColor(ResourcesCompat.getColor(walletHistoryFragment.getResources(), R.color.color_app_wallet_deduct, null));
                    holder.tvTransactionAmount.setText(String.format("-%s %s", ParseContent.getInstance().decimalTwoDigitFormat.format(walletHistory.getAddedWallet()), walletHistory.getFromCurrencyCode()));
                    break;
            }
        } catch (ParseException e) {
            AppLog.handleException(WalletHistoryAdapter.class.getName(), e);
        }
    }

    @Override
    public int getItemCount() {
        return walletHistories.size();
    }

    private String walletComment(int id) {
        String comment;
        switch (id) {
            case Const.Wallet.ADDED_BY_ADMIN:
                comment = walletHistoryFragment.getResources().getString(R.string.text_wallet_status_added_by_admin);
                break;
            case Const.Wallet.ADDED_BY_CARD:
                comment = walletHistoryFragment.getResources().getString(R.string.text_wallet_status_added_by_card);
                break;
            case Const.Wallet.ADDED_BY_REFERRAL:
                comment = walletHistoryFragment.getResources().getString(R.string.text_wallet_status_added_by_referral);
                break;
            case Const.Wallet.ORDER_REFUND:
                comment = walletHistoryFragment.getResources().getString(R.string.text_wallet_status_order_refund);
                break;
            case Const.Wallet.ORDER_PROFIT:
                comment = walletHistoryFragment.getResources().getString(R.string.text_wallet_status_order_profit);
                break;
            case Const.Wallet.ORDER_CANCELLATION_CHARGE:
                comment = walletHistoryFragment.getResources().getString(R.string.text_wallet_status_order_cancellation_charge);
                break;
            case Const.Wallet.ORDER_CHARGED:
                comment = walletHistoryFragment.getResources().getString(R.string.text_wallet_status_order_charged);
                break;
            case Const.Wallet.WALLET_REQUEST_CHARGE:
                comment = walletHistoryFragment.getResources().getString(R.string.text_wallet_status_wallet_request_charge);
                break;
            case Const.Wallet.ADDED_BY_REDEEM_POINTS:
                comment = walletHistoryFragment.getResources().getString(R.string.text_wallet_status_added_by_redeem_points);
                break;
            case Const.Wallet.ADDED_BY_FRIEND:
                comment = walletHistoryFragment.getResources().getString(R.string.text_wallet_status_added_by_friend);
                break;
            case Const.Wallet.SEND_TO_FRIEND:
                comment = walletHistoryFragment.getResources().getString(R.string.text_wallet_status_sent_to_friend);
                break;
            case Const.Wallet.ADDED_BY_REDEEM_WITHDRAWAL:
                comment = walletHistoryFragment.getResources().getString(R.string.text_wallet_status_added_reddem_withdrawal);
                break;
            default:
                comment = "NA";
                break;
        }
        return comment;
    }

    protected static class WalletHistoryHolder extends RecyclerView.ViewHolder {
        TextView tvTransactionState;
        TextView tvTransactionAmount;
        TextView tvTransactionDate;
        TextView tvWithdrawalId;
        TextView tvTransactionTime;

        public WalletHistoryHolder(View itemView) {
            super(itemView);
            tvWithdrawalId = itemView.findViewById(R.id.tvWithdrawalID);
            tvTransactionAmount = itemView.findViewById(R.id.tvTransactionAmount);
            tvTransactionDate = itemView.findViewById(R.id.tvTransactionDate);
            tvTransactionState = itemView.findViewById(R.id.tvTransactionState);
            tvTransactionTime = itemView.findViewById(R.id.tvTransactionTime);
        }
    }
}