package com.edelivery.adapter;

import android.app.Activity;
import android.os.CountDownTimer;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.Addresses;

import java.util.List;

public class CourierDeliveryAddressAdapter extends RecyclerView.Adapter<CourierDeliveryAddressAdapter.CourierDeliveryAddressViewHolder> {

    private final Activity activity;
    private final List<Addresses> addressesList;

    private CountDownTimer countDownTimer;
    private boolean isWaitTimeCountDownTimerStart;
    private long seconds;
    private int waitingStop;
    String string = "%s %s";

    public CourierDeliveryAddressAdapter(Activity activity, List<Addresses> addressesList) {
        this.activity = activity;
        this.addressesList = addressesList;
    }

    @NonNull
    @Override
    public CourierDeliveryAddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CourierDeliveryAddressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_courier_delivery_address, parent, false));
    }

    @Override
    public void onBindViewHolder(CourierDeliveryAddressViewHolder holder, int position) {
        Addresses addresses = addressesList.get(position);

        holder.tvNumber.setText(String.valueOf(holder.getAbsoluteAdapterPosition() + 1));
        holder.tvDeliveredTo.setText(addresses.getUserDetails().getName());
        holder.tvDeliveredAddress.setText(addresses.getAddress());
        holder.tvPhoneNumber.setText(String.format(string, addresses.getUserDetails().getCountryPhoneCode(), addresses.getUserDetails().getPhone()));
        if (addresses.getArrivedTime() != null) {
            holder.tvArrivedTime.setVisibility(View.VISIBLE);
            holder.tvArrivedTime.setText(addresses.getArrivedTime());
        } else {
            holder.tvArrivedTime.setVisibility(View.GONE);
        }

        holder.tvWaitTime.setVisibility(View.GONE);
        if (addresses.getWaitingTime() - addresses.getFreeWaitingTime() > 0) {
            holder.tvWaitTime.setText(String.format("%s %s %s", activity.getString(R.string.text_wait_time), addresses.getWaitingTime() - addresses.getFreeWaitingTime(), activity.getString(R.string.unit_min)));
            holder.tvWaitTime.setVisibility(View.VISIBLE);
        }

        if (position < getItemCount() - 1 && position == waitingStop - 1) {
            startWaitTimeCountDownTimer(seconds, holder.tvWaitTime);
            holder.tvWaitTime.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        if (addressesList == null) {
            return 0;
        } else {
            return addressesList.size();
        }
    }

    public void setWaitingTimeData(long seconds, int waitingStop) {
        this.seconds = seconds;
        this.waitingStop = waitingStop;
    }

    /**
     * Use to start waiting timer
     *
     * @param seconds seconds
     */
    private void startWaitTimeCountDownTimer(final long seconds, final TextView tvWaitTime) {
        if (!isWaitTimeCountDownTimerStart) {
            isWaitTimeCountDownTimerStart = true;
            final long milliSecond = 1000;
            final long totalSeconds = 86400 * milliSecond;
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
            countDownTimer = null;
            countDownTimer = new CountDownTimer(totalSeconds, milliSecond) {
                long remain = seconds;

                public void onTick(long millisUntilFinished) {
                    remain = remain + 1;
                    activity.runOnUiThread(() -> {
                        if (remain < 0) {
                            tvWaitTime.setText(String.format(string, activity.getString(R.string.text_wait_time_start_after), DateUtils.formatElapsedTime(remain * (-1))));
                        } else {
                            tvWaitTime.setText(String.format(string, activity.getString(R.string.text_wait_time), DateUtils.formatElapsedTime(remain)));
                        }
                    });
                }

                public void onFinish() {
                    isWaitTimeCountDownTimerStart = false;
                }

            }.start();
        }
    }

    public static class CourierDeliveryAddressViewHolder extends RecyclerView.ViewHolder {
        CustomFontTextViewTitle tvNumber;
        CustomFontTextViewTitle tvDeliveredTo;
        CustomFontTextView tvDeliveredAddress;
        CustomFontTextView tvPhoneNumber;
        CustomFontTextView tvArrivedTime;
        CustomFontTextView tvWaitTime;

        public CourierDeliveryAddressViewHolder(View itemView) {
            super(itemView);
            tvNumber = itemView.findViewById(R.id.tvNumber);
            tvDeliveredTo = itemView.findViewById(R.id.tvDeliveredTo);
            tvDeliveredAddress = itemView.findViewById(R.id.tvDeliveredAddress);
            tvPhoneNumber = itemView.findViewById(R.id.tvPhoneNumber);
            tvArrivedTime = itemView.findViewById(R.id.tvArrivedTime);
            tvWaitTime = itemView.findViewById(R.id.tvWaitTime);
        }
    }
}