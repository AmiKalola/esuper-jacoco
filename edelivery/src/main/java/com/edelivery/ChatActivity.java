package com.edelivery;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edelivery.adapter.ChatAdapter;
import com.edelivery.models.datamodels.Message;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.service.FcmMessagingService;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AbstractBaseAppCompatActivity {

    private ChatAdapter chatAdapter;
    private RecyclerView rcvChat;
    private DatabaseReference firebaseDatabaseReference;
    private Button btnSend;
    private EditText messageEditText;
    private String messagesChild = "Demo";
    private SimpleDateFormat webFormat;
    private String chatType = "";
    private String receiverId = "";
    private String firebaseToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        initToolBar();
        messagesChild = getIntent().getStringExtra(Const.Params.ORDER_ID);
        chatType = getIntent().getStringExtra(Const.Params.TYPE);
        receiverId = getIntent().getStringExtra(Const.RECEIVER_ID);
        firebaseToken = getIntent().getStringExtra(Const.Params.TOKEN);

        setTitleOnToolBar(getIntent().getStringExtra(Const.TITLE));

        initFirebaseChat();

        rcvChat = findViewById(R.id.rcvChat);
        btnSend = findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);
        btnSend.setEnabled(false);
        btnSend.setAlpha(0.5f);
        messageEditText = findViewById(R.id.messageEditText);
        messageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // nothing here
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    btnSend.setEnabled(true);
                    btnSend.setAlpha(1f);
                } else {
                    btnSend.setEnabled(false);
                    btnSend.setAlpha(0.5f);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //nothing here
            }
        });
        initChatRcv();
    }

    @Override
    public void onPause() {
        if (chatAdapter != null) {
            chatAdapter.stopListening();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (chatAdapter != null) {
            chatAdapter.startListening();
        }
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void initViewById() {
        //nothing here
    }

    @Override
    protected void setViewListener() {
        //nothing here
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    public void onClick(View view) {
        if (view.getId() == R.id.btnSend) {
            sendMessage();
        }
    }

    private void initChatRcv() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        SnapshotParser<Message> parser = dataSnapshot -> dataSnapshot.getValue(Message.class);

        DatabaseReference messagesRef = firebaseDatabaseReference.child(messagesChild).child(chatType);
        FirebaseRecyclerOptions<Message> options = new FirebaseRecyclerOptions.Builder<Message>().setQuery(messagesRef, parser).build();
        chatAdapter = new ChatAdapter(this, options);
        chatAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = chatAdapter.getItemCount();
                int lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (lastVisiblePosition == -1 || (positionStart >= (friendlyMessageCount - 1) && lastVisiblePosition == (positionStart - 1))) {
                    rcvChat.scrollToPosition(positionStart);
                } else {
                    rcvChat.scrollToPosition(friendlyMessageCount - 1);
                }
            }
        });

        rcvChat.setLayoutManager(linearLayoutManager);
        rcvChat.setItemAnimator(null);
        rcvChat.setAdapter(chatAdapter);
    }

    private void initFirebaseChat() {
        webFormat = new SimpleDateFormat(Const.DATE_TIME_FORMAT_WEB, Locale.US);
        webFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        firebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void setAsReadMessage(String id) {
        DatabaseReference chatMessage = firebaseDatabaseReference.child(messagesChild).child(chatType).child(id).child("is_read");
        chatMessage.setValue(true);
    }

    private void sendMessage() {
        if (firebaseDatabaseReference != null) {
            String key = firebaseDatabaseReference.child(messagesChild).child(chatType).push().getKey();
            if (!TextUtils.isEmpty(key)) {
                Message chatMessage = new Message(key, Integer.parseInt(chatType), messageEditText.getText().toString().trim(), webFormat.format(new Date()), Const.USER_CHAT_ID, receiverId, false);
                firebaseDatabaseReference.child(messagesChild).child(chatType).child(key).setValue(chatMessage);

                if (!firebaseToken.isEmpty()) {
                    sendFCMNotification(messageEditText.getText().toString().trim());
                }

                messageEditText.setText("");

            }
        }
    }

    public void sendFCMNotification(String message) {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization",
                "key=" + preferenceHelper.getAndroidUserAppGcmKey());
        header.put("Content-Type", "application/json");
        JSONObject data = new JSONObject();
        JSONObject notificationBody = new JSONObject();
        try {
            data.put("id", FcmMessagingService.CHAT);
            notificationBody.put("body", message);

        } catch (JSONException e) {
            AppLog.handleException(ChatActivity.class.getName(),e);
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("notification", notificationBody);
            jsonObject.put("data", data);
            jsonObject.put("to", firebaseToken);
        } catch (JSONException e) {
            AppLog.handleException(ChatActivity.class.getName(),e);
        }

        ApiInterface apiInterface = new ApiClient().changeApiBaseUrl("https://fcm.googleapis.com/").create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.sendFCMNotification(header, ApiClient.makeTextRequestBody(jsonObject));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(tag, "response: " + response.message());

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(tag, "onFailure: " + t.getMessage());

            }
        });


    }

}