package com.edelivery;

import static com.edelivery.utils.Const.Google.GOOGLE_SIGN_IN;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;

import com.edelivery.fragments.LoginFragment;
import com.edelivery.fragments.RegisterFragment;
import com.edelivery.models.datamodels.Country;
import com.edelivery.models.responsemodels.CountriesResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.persistentroomdata.notification.NotificationRepository;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.LocationHelper;
import com.edelivery.utils.Utils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.internal.ImageRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.tasks.Task;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

public class LoginActivity extends AbstractBaseAppCompatActivity implements LocationHelper.OnLocationReceived {

    private List<Country> countryList;
    private LocationHelper locationHelper;
    private CallbackManager callbackManager;
    private RegisterFragment registerFragment;
    private LoginFragment loginFragment;
    private GoogleSignInClient googleSignInClient;
    private TwitterLoginButton twitterLoginButton;
    private ProfileTracker profileTracker;
    private boolean isFromCheckOut = false;
    private Country locatedCountry;
    private Location location;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        preferenceHelper.clearVerification();
        preferenceHelper.logout();
        LoginManager.getInstance().logOut();
        NotificationRepository.getInstance(this).clearNotification();
        initTwitter();
        initViewById();
        setViewListener();
        isFromCheckOut = getIntent().getBooleanExtra(Const.IS_FROM_CHECKOUT, false);
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().requestProfile().build();
        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
        locationHelper = new LocationHelper(this);
        locationHelper.setLocationReceivedLister(this);
        countryList = new ArrayList<>();
        getCountries();
        callbackManager = CallbackManager.Factory.create();
        swipeLoginAndRegister(true);
    }

    public void swipeLoginAndRegister(boolean isLogin) {
        if (isLogin) {
            loginFragment = new LoginFragment();
            loginFragment.show(getSupportFragmentManager(), loginFragment.getTag());
            if (registerFragment != null) {
                registerFragment.dismiss();
                registerFragment = null;
            }
        } else {
            registerFragment = new RegisterFragment();
            registerFragment.show(getSupportFragmentManager(), registerFragment.getTag());
            if (loginFragment != null) {
                loginFragment.dismiss();
                loginFragment = null;
            }
        }
    }

    /**
     * this method call a webservice for get country list
     */
    private void getCountries() {
        Call<CountriesResponse> countriesCall = ApiClient.getClient().create(ApiInterface.class).getCountries();
        countriesCall.enqueue(new retrofit2.Callback<CountriesResponse>() {
            @Override
            public void onResponse(@NonNull Call<CountriesResponse> call, @NonNull Response<CountriesResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        countryList = response.body().getCountries();
                        locationHelper.getLastLocation(loc -> {
                            if (loc != null) {
                                new GetCountryCityAsyncTask(loc.getLatitude(), loc.getLongitude()).execute();
                            } else {
                                if (countryList != null && !countryList.isEmpty()) {
                                    setLocatedCountry(countryList.get(0));
                                }
                            }
                        });
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), response.body().getStatusPhrase(),LoginActivity.this);
                    }
                } else {
                    Utils.showHttpErrorToast(response.code(), LoginActivity.this);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesResponse> call, @NonNull Throwable t) {
                AppLog.handleThrowable(LoginActivity.class.getName(),t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        locationHelper.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        locationHelper.onStop();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void initViewById() {
        //nothing here
    }

    @Override
    protected void setViewListener() {
        //nothing here
    }

    @Override
    protected void onBackNavigation() {
        //nothing here
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_SIGN_IN) {
            getGoogleSignInResult(data);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
        twitterLoginButton.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        //nothing here
    }

    @Override
    public void onLocationChanged(Location location) {
       // nothing here
    }

    @Override
    public void onBackPressed() {
        try {
            ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> taskList = activityManager.getRunningTasks(10);
            if (taskList.get(0).numActivities == 2 && taskList.get(0).baseActivity.getClassName().equals(HomeActivity.class.getName())) {
                super.onBackPressed();
            } else {
                if (isFromCheckOut || Boolean.TRUE.equals(preferenceHelper.getIsFromQRCode())) {
                    super.onBackPressed();
                } else {
                    goToHomeActivity();
                }
            }
        } catch (Exception e) {
            AppLog.handleException(LoginActivity.class.getName(),e);
            super.onBackPressed();
        }
    }

    private void initTwitter() {
        TwitterConfig config = new TwitterConfig.Builder(this).logger(new DefaultLogger(Log.DEBUG)).twitterAuthConfig(new TwitterAuthConfig(getResources().getString(R.string.TWITTER_CONSUMER_KEY), getResources().getString(R.string.TWITTER_CONSUMER_SECRET))).debug(BuildConfig.DEBUG).build();
        Twitter.initialize(config);
    }

    private void getFacebookSignInResult(AccessToken accessToken, final Profile profile) {
        GraphRequest dataRequest = GraphRequest.newMeRequest(accessToken, (jsonObject, response) -> {
            try {
                if (registerFragment != null) {
                    registerFragment.updateUiForSocialLogin(jsonObject.getString(Const.Facebook.EMAIL), profile.getId(), profile.getFirstName(), profile.getLastName(), ImageRequest.getProfilePictureUri(profile.getId(), 250, 250));
                    LoginManager.getInstance().logOut();
                }
            } catch (Exception e) {
                AppLog.handleException(Const.Tag.REGISTER_FRAGMENT, e);
            }
        });
        Bundle permissionParam = new Bundle();
        permissionParam.putString("fields", "id,name,email,picture");
        dataRequest.setParameters(permissionParam);
        dataRequest.executeAsync();
    }

    private void getGoogleSignInResult(Intent data) {
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        if (task.isSuccessful()) {
            GoogleSignInAccount acct = task.getResult();
            if (loginFragment != null) {
                if(preferenceHelper.isUseCaptcha()) {
                    checkSafetyNet(token -> loginFragment.login(acct.getId(), token));
                } else {
                    loginFragment.login(acct.getId(),"");

                }
            } else {
                if (acct != null) {
                    String firstName;
                    String lastName = "";
                    if (acct.getDisplayName().contains(" ")) {
                        String[] strings = acct.getDisplayName().split(" ");
                        firstName = strings[0];
                        lastName = strings[1];
                    } else {
                        firstName = acct.getDisplayName();
                    }
                    if (registerFragment != null) {
                        registerFragment.updateUiForSocialLogin(acct.getEmail(), acct.getId(), firstName, lastName, acct.getPhotoUrl());
                    }
                }
            }
        }
    }

    public void initTwitterLogin(View view) {
        twitterLoginButton = view.findViewById(R.id.btnTwitterLogin);
        twitterLoginButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.size_app_text_regular));
        twitterLoginButton.setCompoundDrawablesRelativeWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.iconmonstr_twitter_1), null, null, null);
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(final Result<TwitterSession> result) {
                Utils.showCustomProgressDialog(LoginActivity.this, false);
                TwitterAuthClient twitterAuthClient = new TwitterAuthClient();
                twitterAuthClient.requestEmail(result.data, new Callback<String>() {
                    @Override
                    public void success(Result<String> result2) {
                        Utils.hideCustomProgressDialog();
                        if (loginFragment != null) {
                            if(preferenceHelper.isUseCaptcha()) {
                                checkSafetyNet(token -> loginFragment.login(String.valueOf(result.data.getUserId()), token));
                            } else {
                                loginFragment.login(String.valueOf(result.data.getUserId()),"");
                            }
                        }
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        Utils.hideCustomProgressDialog();
                        AppLog.handleException("TWITTER_LOGIN", exception);
                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {
                // nothing here
            }
        });
    }

    public void initFBLogin(View view) {
        callbackManager = CallbackManager.Factory.create();
        LoginButton faceBookLogin = view.findViewById(R.id.btnFbLogin);
        faceBookLogin.setPermissions(Arrays.asList(Const.Facebook.PUBLIC_PROFILE, Const.Facebook.EMAIL));
        faceBookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult != null) {
                    if (loginFragment != null) {
                        if(preferenceHelper.isUseCaptcha()) {
                            checkSafetyNet(token -> loginFragment.login(loginResult.getAccessToken().getUserId(), token));
                        } else {
                            loginFragment.login(loginResult.getAccessToken().getUserId(),"");
                        }
                    }
                    LoginManager.getInstance().logOut();
                } else {
                    profileTracker.stopTracking();
                }
            }

            @Override
            public void onCancel() {
                Utils.showToast("Facebook login cancel", LoginActivity.this);

            }

            @Override
            public void onError(@NonNull FacebookException error) {
                Utils.showToast("Facebook login error", LoginActivity.this);
            }
        });
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                if (AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired()) {
                    getFacebookSignInResult(AccessToken.getCurrentAccessToken(), currentProfile);
                }
            }
        };
    }

    public void initGoogleLogin(View view) {
        SignInButton btnGoogleSingIn;
        btnGoogleSingIn = view.findViewById(R.id.btnGoogleLogin);
        btnGoogleSingIn.setSize(SignInButton.SIZE_WIDE);
        btnGoogleSingIn.setOnClickListener(v -> googleSignInClient.signOut().addOnCompleteListener(task -> {
            Intent intent = googleSignInClient.getSignInIntent();
            startActivityForResult(intent, GOOGLE_SIGN_IN);
        }));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (profileTracker != null) {
            profileTracker.stopTracking();
        }
    }

    private void checkCountryCode(String country) {
        int countryListSize = countryList.size();
        for (int i = 0; i < countryListSize; i++) {
            if (countryList.get(i).getName().toUpperCase().startsWith(country.toUpperCase())) {
                setLocatedCountry(countryList.get(i));
                return;
            }
        }
        if (!countryList.isEmpty()) {
            setLocatedCountry(countryList.get(0));
        }
    }

    public RegisterFragment getRegisterFragment() {
        return registerFragment;
    }

    public void setLocatedCountry(Country country) {
        if (country == null) {
            return;
        }
        this.locatedCountry = country;
        if (loginFragment != null) {
            loginFragment.setCountry(locatedCountry);
        }
        if (registerFragment != null) {
            registerFragment.setCountry(locatedCountry);
        }
    }

    public LoginFragment getLoginFragment() {
        return loginFragment;
    }

    public boolean isFromCheckOut() {
        return isFromCheckOut;
    }

    /**
     * this class will help to get current cityName or county according current location
     */
    @SuppressLint("StaticFieldLeak")
    private class GetCountryCityAsyncTask extends AsyncTask<String, Void, Address> {

        double lat;
        double lng;

        public GetCountryCityAsyncTask(double lat, double lng) {
            this.lat = lat;
            this.lng = lng;
        }

        @Override
        protected Address doInBackground(String... params) {
            Geocoder geocoder = new Geocoder(LoginActivity.this, new Locale("en_US"));
            try {
                List<Address> addressList = geocoder.getFromLocation(lat, lng, 1);
                if (addressList != null && !addressList.isEmpty()) {
                    return addressList.get(0);
                }
            } catch (IOException e) {
                AppLog.handleException(RegisterFragment.class.getName(), e);
                publishProgress();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            if (countryList != null && !countryList.isEmpty()) {
                setLocatedCountry(countryList.get(0));
            }
        }

        @Override
        protected void onPostExecute(Address address) {
            String countryName;
            if (address != null) {
                countryName = address.getCountryName();
                checkCountryCode(countryName);
                if (address.getLocality() != null) {
                    address.getLocality();
                } else if (address.getSubAdminArea() != null) {
                    address.getSubAdminArea();
                } else {
                    address.getAdminArea();
                }
            }
        }
    }

    public List<Country> getCountryList() {
        return countryList;
    }

    public Country getLocatedCountry() {
        return locatedCountry;
    }
}