package com.edelivery;

import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.appcompat.widget.SwitchCompat;

import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.utils.AppColor;

public class SettingActivity extends AbstractBaseAppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private SwitchCompat switchPushNotificationSound;
    private SwitchCompat switchStoreImage;
    private SwitchCompat switchProductImage;
    private Spinner spinnerLanguage;
    private Spinner spinnerTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_settings));
        initViewById();
        setViewListener();
        initLanguageSpinner();
        initThemeSpinner();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void initViewById() {
        switchPushNotificationSound = findViewById(R.id.switchPushNotificationSound);
        switchStoreImage = findViewById(R.id.switchStoreImage);
        switchProductImage = findViewById(R.id.switchProductImage);
        switchPushNotificationSound.setChecked(preferenceHelper.getIsPushNotificationSoundOn());
        switchStoreImage.setChecked(preferenceHelper.getIsLoadStoreImage());
        switchProductImage.setChecked(preferenceHelper.getIsLoadProductImage());
        LinearLayout llNotification = findViewById(R.id.llNotification);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            llNotification.setVisibility(View.GONE);
        } else {
            llNotification.setVisibility(View.VISIBLE);
        }
        spinnerLanguage = findViewById(R.id.spinnerLanguage);
        spinnerTheme = findViewById(R.id.spinnerTheme);
    }

    @Override
    protected void setViewListener() {
        switchPushNotificationSound.setOnCheckedChangeListener(this);
        switchStoreImage.setOnCheckedChangeListener(this);
        switchProductImage.setOnCheckedChangeListener(this);
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        //nothing here
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        if (id == R.id.switchPushNotificationSound) {
            preferenceHelper.putIsPushNotificationSoundOn(isChecked);
        } else if (id == R.id.switchStoreImage) {
            preferenceHelper.putIsLoadStoreImage(isChecked);
        } else if (id == R.id.switchProductImage) {
            preferenceHelper.putIsLoadProductImage(isChecked);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    
    private void initLanguageSpinner() {
        TypedArray array = this.getResources().obtainTypedArray(R.array.language_code);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.language_name, R.layout.spiner_view_small);
        adapter.setDropDownViewResource(R.layout.item_spiner_view_small);
        spinnerLanguage.setAdapter(adapter);

        spinnerLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String languageCode = array.getString(i);
                if (!TextUtils.equals(preferenceHelper.getLanguageCode(), languageCode)) {
                    preferenceHelper.putLanguageCode(languageCode);
                    preferenceHelper.putLanguageIndex(getLangIndxex(languageCode, currentBooking.getLangs(), false));
                    CurrentBooking.getInstance().setLanguageChanged(true);
                    finishAffinity();
                    restartApp();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //nothing here

            }
        });

        int size = array.length();
        for (int i = 0; i < size; i++) {
            if (TextUtils.equals(this.preferenceHelper.getLanguageCode(), array.getString(i))) {
                spinnerLanguage.setSelection(i);
                break;
            }
        }
    }

    private void initThemeSpinner() {
        TypedArray array = this.getResources().obtainTypedArray(R.array.theme_code);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.theme, R.layout.spiner_view_small);
        adapter.setDropDownViewResource(R.layout.item_spiner_view_small);
        spinnerTheme.setAdapter(adapter);

        spinnerTheme.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int themeCode = Integer.parseInt(array.getString(i));
                if (preferenceHelper.getTheme() != themeCode) {
                    preferenceHelper.putTheme(themeCode);
                    AppColor.onActivityCreateSetTheme(SettingActivity.this);
                    finishAffinity();
                    restartApp();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //nothing here

            }
        });

        int size = array.length();
        for (int i = 0; i < size; i++) {
            if (this.preferenceHelper.getTheme() ==  Integer.parseInt(array.getString(i))) {
                spinnerTheme.setSelection(i);
                break;
            }
        }
    }
}